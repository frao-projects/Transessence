#include "Manager\Control.hpp"

//! \file
//!
//! \brief Implementation of retreival of global file manager
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

namespace frao
{
inline namespace FileIO
{
// unnamed namepsace for scoped global file manager
//(ie: to restrict usage, and delay creation of
// object until required, whilst also allowing global
// usage and access characteristics)

namespace
{
static std::unique_ptr<FileManager> g_FileManager = nullptr;
}

//===============================
//-----------Manager-----------
//===============================
// namespace for functions to
// control instantiation and
// access to global g_FileManager
//===============================
void Manager::createNewFileManager(bool readDefaultFile)
{
	if (g_FileManager == nullptr)
		g_FileManager =
			std::make_unique<FileManager>(true, readDefaultFile);
}
FileManager& Manager::getFileManager()
{
	if (g_FileManager == nullptr) createNewFileManager(true);

	return *g_FileManager;
}

}  // namespace FileIO
}  // namespace frao