// File_IO module is purely for file i/o and parsing
#include "Manager\Organisation.hpp"
#include <Nucleus_inc\Environment.hpp>
#include <Nucleus_inc\Error.hpp>

//! \file
//!
//! \brief Implementation of management of file IO
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

namespace frao
{
inline namespace FileIO
{
using namespace Strings::Operators;

// FileManager functions
// private
bool FileManager::checkReadFile(iterator& file)
{
	if (file == m_FilesToRead.end()) return false;

	if ((*file)->readComplete())
	{
		FileBuffer buf = (*file)->getData();

		// put data in list of read files, and remove files
		// from list of unread ones
		m_ReadFiles.insert(
			std::make_pair((*file)->fullPath(), std::move(buf)));
		file = m_FilesToRead.erase(file);
		return true;
	}

	return false;
}
bool FileManager::checkWrittenFile(iterator& file)
{
	if (file == m_FilesToWrite.end()) return false;

	if ((*file)->writeComplete())
	{
		// put data in list of written files, and remove
		// files from list of unwritten ones
		m_WrittenFiles.emplace_back((*file)->fullPath());
		file = m_FilesToWrite.erase(file);
		return true;
	}

	return false;
}
// public
FileManager::FileManager(bool asyncio, bool usedefaultfile)
	: FileManager(Path(Directory::getExePath()), asyncio,
				  usedefaultfile)
{}
FileManager::FileManager(Strings::widestring usepath, bool asyncio,
						 bool usedefaultfile)
	: FileManager(Path(usepath), asyncio, usedefaultfile)
{}
FileManager::FileManager(Path usepath, bool asyncio,
						 bool usedefaultfile)
	: m_BaseDirectory(nullptr),
	  m_ActiveDirectory(),
	  m_FilesToRead(),
	  m_FilesToWrite(),
	  m_ReadFiles(),
	  m_WrittenFiles(),
	  m_FileAssocs()
{
	// add 'default file', and '.txt' parser, automatically
	m_FileAssocs.emplace(fraocfgParse::defExtension(),
						 &FileParse::make<fraocfgParse>);
	m_FileAssocs.emplace(txtParse::defExtension(),
						 &FileParse::make<txtParse>);
	m_FileAssocs.emplace(bitmapParse::defExtension(),
						 &FileParse::make<bitmapParse>);

	// create directory system
	m_BaseDirectory =
		std::make_unique<Directory>(usepath, nullptr, asyncio, false);
	setActiveDirectory(usepath);

	if (usedefaultfile)
	{
		readDefaultFile(true);
	}
}
//
Path FileManager::getActiveDirectory() const
{
	return m_BaseDirectory->activeDirPath();
}
bool FileManager::isActiveDirAsync() const
{
	return m_BaseDirectory->getSubDirectory(getActiveDirectory())
		->asyncByDef();
}
void FileManager::setActiveDirectory(Strings::widestring dirpath)
{
	setActiveDirectory(Path(dirpath));
}
void FileManager::setActiveDirectory(Path dirpath)
{
	if (dirpath.isRelative())
	{
		dirpath.makeAbsolute(m_BaseDirectory->activeDirPath());
	}

	m_ActiveDirectory = m_BaseDirectory->setActiveDir(dirpath);
}
//
void FileManager::setFileAssoc(FileParse::makeptr make,
							   bool				  overwrite)
{
	auto temp = make();  // create temporary to get file extension
	Strings::widestring extension = temp->getExtension();

	// make sure we don't already have something for that
	// extension
	auto loc = m_FileAssocs.find(extension);
	if (loc != m_FileAssocs.end())
	{
		if (overwrite)
			m_FileAssocs.erase(loc);
		else
			return;
	}

	m_FileAssocs.insert(std::make_pair(extension, make));
}
//
void FileManager::readDefaultFile(bool implementcontents)
{
	Path defaultfileName(L"defaultfile.fraocfg"_ustring);

	readDefaultFile(defaultfileName, implementcontents);
}
void FileManager::readDefaultFile(Path filepath,
								  bool implmentcontents)
{
	if (filepath.isRelative())
		filepath.makeAbsolute(m_BaseDirectory->activeDirPath());

	readSpecified(filepath);  // can throw FileCreation

	// loop until file has been read
	while (!fileHasBeenRead(filepath))
		Sleep(5);

	// implement contents of file
	if (implmentcontents)
	{
		// get the parsed file contents!
		auto fileparse = getReadFileData(filepath);
		runtime_assert(fileparse->getExtension()
					   == fraocfgParse::defExtension());

		std::unique_ptr<fraocfgParse> defaultparsed{
			static_cast<fraocfgParse*>(fileparse.release())};

		Path activedir = defaultparsed->getDirectory();
		activedir.makeAbsolute(Directory::getExePath());

		setActiveDirectory(activedir);

		std::vector<Path> files = defaultparsed->getFiles();
		readSpecified(files);
	}

	closeReadFile(filepath, true);
}
//
void FileManager::addFileToQueue(Path filepath, bool writeover)
{
	auto	   activedir = m_ActiveDirectory.lock();
	const bool async	 = activedir ? activedir->asyncByDef()
								 : m_BaseDirectory->asyncByDef();
	addFileToQueue(filepath, writeover, async);
}
void FileManager::addFileToQueue(Path filepath, bool writeover,
								 bool async)
{
	if (filepath.isRelative())
	{
		filepath.makeAbsolute(m_BaseDirectory->activeDirPath());
	}

	auto fileptr =
		m_BaseDirectory->reOpenFile(filepath, async, writeover);

	if (writeover)
		m_FilesToWrite.push_back(fileptr);
	else
		m_FilesToRead.push_back(fileptr);
}
//
void FileManager::addFilesToQueue(
	std::vector<Strings::widestring> files, bool writeover)
{
	std::vector<Path> parsed;

	for (const auto& str : files)
		parsed.push_back(Path(str));

	auto	   activedir = m_ActiveDirectory.lock();
	const bool async	 = activedir ? activedir->asyncByDef()
								 : m_BaseDirectory->asyncByDef();

	addFilesToQueue(parsed, writeover, async);
}
void FileManager::addFilesToQueue(
	std::vector<Strings::widestring> files, bool writeover,
	bool async)
{
	std::vector<Path> parsed;

	for (const auto& str : files)
		parsed.push_back(Path(str));

	addFilesToQueue(parsed, writeover, async);
}
//
void FileManager::addFilesToQueue(std::vector<Path> files,
								  bool				writeover)
{
	auto	   activedir = m_ActiveDirectory.lock();
	const bool async	 = activedir ? activedir->asyncByDef()
								 : m_BaseDirectory->asyncByDef();

	addFilesToQueue(files, writeover, async);
}
void FileManager::addFilesToQueue(std::vector<Path> files,
								  bool writeover, bool async)
{
	Path activepath = m_BaseDirectory->activeDirPath();

	for (auto file : files)
	{
		if (file.isRelative()) file.makeAbsolute(activepath);

		addFileToQueue(file, writeover, async);
	}
}
//
void FileManager::readAllQueued()
{
	for (auto fileptr : m_FilesToRead)
	{
		fileptr->read();
	}
}
void FileManager::readAllQueued(std::vector<Path> extrafiles)
{
	// read all of the things currently there
	readAllQueued();

	auto activedir = m_ActiveDirectory.lock();

	// get and read additional files to the queue
	for (auto filepath : extrafiles)
	{
		if (filepath.isRelative() && activedir)
			m_FilesToRead.push_back(
				activedir->getFile(filepath, true));
		else
			m_FilesToRead.push_back(
				m_BaseDirectory->getFile(filepath, true));
	}
}
//
void FileManager::readAsyncFiles()
{
	for (auto fileptr : m_FilesToRead)
	{
		if (fileptr->isAsync()) fileptr->read();
	}
}
void FileManager::readAsyncFiles(std::vector<Path> extrafiles)
{
	// read appropriate files that have already been added
	readAsyncFiles();

	auto activedir = m_ActiveDirectory.lock();

	// get and read additional files to the queue
	for (auto filepath : extrafiles)
	{
		if (filepath.isRelative() && activedir)
			m_FilesToRead.push_back(
				activedir->getFile(filepath, true));
		else
			m_FilesToRead.push_back(
				m_BaseDirectory->getFile(filepath, true));
	}
}
void FileManager::readSyncFiles()
{
	for (auto fileptr : m_FilesToRead)
	{
		if (!(fileptr->isAsync())) fileptr->read();
	}
}
void FileManager::readSyncFiles(std::vector<Path> extrafiles)
{
	// read appropriate files that have already been added
	readSyncFiles();

	auto activedir = m_ActiveDirectory.lock();

	// get and read additional files to the queue
	for (auto filepath : extrafiles)
	{
		if (filepath.isRelative() && activedir)
			m_FilesToRead.push_back(
				activedir->getFile(filepath, true));
		else
			m_FilesToRead.push_back(
				m_BaseDirectory->getFile(filepath, true));
	}
}
//
void FileManager::readSpecified(Path filepath)
{
	if (filepath.isRelative())
		filepath.makeAbsolute(m_BaseDirectory->activeDirPath());

	// see if the file is already in the read list
	for (auto file : m_FilesToRead)
		if (file->fullPath() == filepath)
		{
			file->read();
			return;
		}

	addFileToQueue(filepath, false);

	for (auto iter = m_FilesToRead.rbegin();
		 iter != m_FilesToRead.rend(); ++iter)
		if ((*iter)->fullPath() == filepath)
		{
			(*iter)->read();
			return;
		}
}
void FileManager::readSpecified(std::vector<Path> files)
{
	for (auto filepath : files)
	{
		readSpecified(filepath);
	}
}
//
void FileManager::writeSpecified(Path filepath, FileBuffer data)
{
	if (filepath.isRelative())
		filepath.makeAbsolute(m_BaseDirectory->activeDirPath());

	// see if the file is already in the read list
	for (auto file : m_FilesToWrite)
		if (file->fullPath() == filepath)
		{
			file->setData(std::move(data));
			file->write();
			return;
		}

	addFileToQueue(filepath, true);

	for (auto iter = m_FilesToWrite.rbegin();
		 iter != m_FilesToWrite.rend(); ++iter)
		if ((*iter)->fullPath() == filepath)
		{
			(*iter)->setData(std::move(data));
			(*iter)->write();
		}
}
void FileManager::writeSpecified(std::map<Path, FileBuffer> datamap)
{
	for (auto& pair : datamap)
	{
		writeSpecified(pair.first, std::move(pair.second));
	}
}
//
bool FileManager::fileHasBeenRead(Strings::widestring filepath)
{
	return fileHasBeenRead(Path(filepath));
}
bool FileManager::fileHasBeenRead(Path filepath)
{
	// NOTE: UNSAFE IF CALLED ON DIFFERENT FILES IN ONE LOOP
	// - ONE FILE MAY COMPLETE WHILE THE OTHER IS WAITING,
	// LEADING TO PERMANENT STALL

	// TODO: fix this function, so that it can be called
	// multiple times in the same loop

	if (filepath.isRelative())
	{
		filepath.makeAbsolute(m_BaseDirectory->activeDirPath());
	}

	for (auto iter = m_FilesToRead.begin();
		 iter != m_FilesToRead.end(); ++iter)
	{
		if ((*iter)->fullPath() == filepath)
		{
			return checkReadFile(iter);
		}
	}

	return false;
}
//
bool FileManager::fileHasBeenWrittenTo(Strings::widestring filepath)
{
	return fileHasBeenWrittenTo(Path(filepath));
}
bool FileManager::fileHasBeenWrittenTo(Path filepath)
{
	if (filepath.isRelative())
		filepath.makeAbsolute(m_BaseDirectory->activeDirPath());

	for (auto iter = m_FilesToWrite.begin();
		 iter != m_FilesToWrite.end(); ++iter)
	{
		if ((*iter)->fullPath() == filepath)
		{
			return checkWrittenFile(iter);
		}
	}

	return false;
}
//
std::vector<Path> FileManager::getActiveFiles() const
{
	return m_BaseDirectory->getAllFilePaths(true);
}
std::vector<Path> FileManager::getActiveSubDirectories() const
{
	return m_BaseDirectory->getAllDirectoryPaths(true);
}
//
std::vector<Path> FileManager::getReadFiles(bool blockuntilread)
{
	// iterate over files to read, and see if they've been
	// read. If so, add them to the read file map
	for (auto iter = m_FilesToRead.begin();
		 iter != m_FilesToRead.end();)
	{
		if (!checkReadFile(iter))
		{
			if (!blockuntilread)
				++iter;
			else
				while (!checkReadFile(iter))
					Sleep(1);
		}
	}

	std::vector<Path> result;

	for (auto pair : m_ReadFiles)
		result.push_back(pair.first);

	return result;
}
std::vector<Path> FileManager::getWrittenFiles()
{
	// iterate over files to read, and see if they've been
	// read. If so, add them to the read file map
	for (auto iter = m_FilesToWrite.begin();
		 iter != m_FilesToWrite.end();)
	{
		if (!checkReadFile(iter))
		{
			++iter;
		}
	}

	std::vector<Path> result;

	for (auto filepath : m_WrittenFiles)
		result.push_back(filepath);

	return result;
}
//
void FileManager::closeReadFile(Strings::widestring filepath,
								bool				closefully)
{
	closeReadFile(Path(filepath), closefully);
}
void FileManager::closeReadFile(Path filepath, bool closefully)
{
	if (filepath.isRelative())
		filepath.makeAbsolute(m_BaseDirectory->activeDirPath());

	auto loc = m_ReadFiles.find(filepath);

	if (loc != m_ReadFiles.end()) m_ReadFiles.erase(loc);

	if (closefully) m_BaseDirectory->closeFile(filepath);
}
//
void FileManager::closeWrittenFile(Strings::widestring filepath,
								   bool				   closefully)
{
	closeWrittenFile(Path(filepath), closefully);
}
void FileManager::closeWrittenFile(Path filepath, bool closefully)
{
	if (filepath.isRelative())
		filepath.makeAbsolute(m_BaseDirectory->activeDirPath());

	for (auto iter = m_WrittenFiles.begin();
		 iter != m_WrittenFiles.end(); ++iter)
	{
		if (filepath == (*iter))
		{
			m_WrittenFiles.erase(iter);
			break;
		}
	}

	if (closefully) m_BaseDirectory->closeFile(filepath);
}
//
bool FileManager::canParseFile(Strings::widestring filepath)
{
	return canParseFile(Path(filepath));
}
bool FileManager::canParseFile(Path filepath)
{
	auto loc = m_FileAssocs.find(filepath.backCopy().fileextension());

	return (loc != m_FileAssocs.end());
}
//
FileBuffer FileManager::getReadFileBuffer(
	Strings::widestring filepath)
{
	return getReadFileBuffer(Path(filepath));
}
FileBuffer FileManager::getReadFileBuffer(Path filepath)
{
	if (filepath.isRelative())
	{
		filepath.makeAbsolute(m_BaseDirectory->activeDirPath());
	}

	for (auto& pair : m_ReadFiles)
	{
		if (pair.first == filepath) return pair.second;
	}

	// if we get here, the buffer is not in the map, so try
	// and get it
	if (fileHasBeenRead(filepath)) return getReadFileBuffer(filepath);

	throw getErrorLog()->createException<NotFound>(
		FILELOC_UTF8, u8"the specified buffer was not found!"_ustring);
}
//
std::unique_ptr<FileParse> FileManager::getReadFileData(
	Strings::widestring filepath)
{
	return getReadFileData(Path(filepath));
}
std::unique_ptr<FileParse> FileManager::getReadFileData(Path filepath)
{
	// get the appropriate file parser
	auto loc = m_FileAssocs.find(filepath.backCopy().fileextension());
	if (loc == m_FileAssocs.end())
		throw getErrorLog()->createException<InvalidFunctionCall>(
			FILELOC_UTF8,
			u8"no appropriate file parser was available"_ustring);

	std::unique_ptr<FileParse> result = loc->second();

	// get the actual data
	FileBuffer buffer = getReadFileBuffer(filepath);
	(*result)(std::move(buffer));  // call operator() of FileParse

	return result;
}

}  // namespace FileIO
}  // namespace frao