//! \file
//!
//! \brief Implementation of interpretation of bitmaps
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "Formats\Bitmap.hpp"
#include <intrin.h>	 //needed for efficient bit stuff

// FileFormat::Bitmap namespace

namespace frao
{
inline namespace FileIO
{
namespace FileFormats
{
namespace Bitmap
{
using namespace Strings::Operators;

// Header functions
// public
Header::Header(const small_nat8* dataBufferPtr, natural64 bufferSize)
	: m_FileSize(0), m_OffsetBytes(0), m_Reserved1(0), m_Reserved2(0)
{
	if (dataBufferPtr == nullptr)
	{
		throw getErrorLog()->createException<DataAcquisition>(
			FILELOC_UTF8,
			u8"FileBuffer pointer passed was null!"_ustring);
	}

	if (bufferSize < sizeInFile())
	{
		throw getErrorLog()->createException<DataAcquisition>(
			FILELOC_UTF8,
			u8"FileBuffer passed was too small!"_ustring);
	}

	const char* data = reinterpret_cast<const char*>(dataBufferPtr);

	if ((data[0] != 'B') || (data[1] != 'M'))
	{
		throw getErrorLog()->createException<InvalidFileFormat>(
			FILELOC_UTF8,
			u8"File that was meant to be a bitmap, is not"_ustring);
	}

	m_FileSize =
		*(reinterpret_cast<const small_nat32*>(dataBufferPtr + 2));
	m_Reserved1 =
		*(reinterpret_cast<const small_nat16*>(dataBufferPtr + 6));
	m_Reserved2 =
		*(reinterpret_cast<const small_nat16*>(dataBufferPtr + 8));
	m_OffsetBytes =
		*(reinterpret_cast<const small_nat32*>(dataBufferPtr + 10));
}
Header::Header(small_nat32 filesize, small_nat32 offsetBytes) noexcept
	: Header(filesize, offsetBytes, 0, 0)
{}
Header::Header(small_nat32 fileSize, small_nat32 offsetBytes,
			   small_nat16 reserved1, small_nat16 reserved2) noexcept
	: m_FileSize(fileSize),
	  m_OffsetBytes(offsetBytes),
	  m_Reserved1(reserved1),
	  m_Reserved2(reserved2)
{}
//
small_nat32 Header::bitmapOffset() const noexcept
{
	return m_OffsetBytes;
}
//
natural64 Header::writeTo(small_nat8* dataBufferPtr,
						  natural64	  bufferSize) const
{
	if (dataBufferPtr == nullptr)
	{
		throw getErrorLog()->createException<InvalidUserInput>(
			FILELOC_UTF8,
			u8"passed null buffer - cannot write to!"_ustring);
	}

	if (bufferSize < sizeInFile())
	{
		throw getErrorLog()->createException<InvalidUserInput>(
			FILELOC_UTF8,
			u8"FileBuffer passed was too small!"_ustring);
	}

	dataBufferPtr[0]									 = 'B';
	dataBufferPtr[1]									 = 'M';
	*(reinterpret_cast<small_nat32*>(dataBufferPtr + 2)) = m_FileSize;
	*(reinterpret_cast<small_nat16*>(dataBufferPtr + 6)) =
		m_Reserved1;
	*(reinterpret_cast<small_nat16*>(dataBufferPtr + 8)) =
		m_Reserved2;
	*(reinterpret_cast<small_nat32*>(dataBufferPtr + 10)) =
		m_OffsetBytes;

	return 14;
}


Detail::FailCode heightMatchesCompression(
	small_int32 imageHeight, Compression compression) noexcept
{
	bool result = true;

	switch (compression)
	{
		case Compression::RGB:
			[[fallthrough]];
		case Compression::BITFIELD:
			break;
		default:
			if (imageHeight < 0)
			{
				result = false;
			}
			break;
	}

	return Detail::makeFailCode(result, false);
}
Detail::FailCode isValidCompression(Strings::utf8string& errStr,
									Compression			 compression,
									small_nat16 bitcount) noexcept
{
	bool result = true;

	if ((compression != Compression::JPEG)
		&& (compression != Compression::PNG))
	{
		if (bitcount == 0)
		{
			result = false;
			errStr =
				u8"Bitmap: 0 was specified for bitcount, but "
				u8"compression was not set to JPEG or PNG!"_ustring;
		} else if (compression == Compression::RGB)
		{
			result = true;
		} else
		{
			switch (bitcount)
			{
				case 16:  // Bitfield
					[[fallthrough]];
				case 32:  // Bitfield
					if (compression != Compression::BITFIELD)
					{
						result = false;
						errStr = u8"Bitmap: "_ustring + Strings::utf8string(bitcount)
						+ u8" was specified for bitcount, but compression "
						u8"was not set to RGB, Bitfield, PNG, or JPEG!"_ustring;
					} else
					{
						result = true;
					}
					break;
				case 4:	 // RLE4
					if (compression != Compression::RLE4)
					{
						result = false;
						errStr =
							u8"Bitmap: 4 was specified for bitcount, "
							u8"but compression was not set to RGB, "
							u8"RLE4, PNG, or JPEG!"_ustring;
					} else
					{
						result = true;
					}
					break;
				case 8:	 // RLE8
					if (compression != Compression::BITFIELD)
					{
						result = false;
						errStr =
							u8"Bitmap: 8 was specified for bitcount, "
							u8"but compression was not set to RGB, RLE8, PNG, or JPEG!"_ustring;
					} else
					{
						result = true;
					}
					break;

				default:  // must be invalid

					result = false;
					errStr =
						u8"Bitmap: Bitcount was invalid or mismatched the compression mode"_ustring;
					break;
			}
		}
	}

	return Detail::makeFailCode(result, false);
}
Detail::FailCode checkUsedColours(Strings::utf8string& errStr,
								  small_nat32		   coloursUsed,
								  small_nat16 bitcount) noexcept
{
	bool result = true;

	if (bitcount != 0)
	{
		small_nat64 maxcolours = 0x1ULL
								 << static_cast<natural64>(bitcount);

		if (coloursUsed > maxcolours)
		{
			errStr =
				u8"Bitmap: bitcount parameter was "_ustring
				+ Strings::utf8string(bitcount)
				+ u8", but the colours used was greater than "_ustring
				+ Strings::utf8string(maxcolours)
				+ ((maxcolours == 0)
					   ? u8", which is what colours used must be for bitcounts of 16, 24, and 32!"_ustring
					   : u8", which is not possible!"_ustring);

			result = false;
		}
	}

	return Detail::makeFailCode(result, false);
}
Detail::FailCode checkImportantColours(
	small_nat32 coloursUsed, small_nat32 coloursImportant) noexcept
{
	bool result = true;

	if ((coloursUsed != 0) && (coloursImportant != 0)
		&& (coloursUsed < coloursImportant))
	{
		result = false;
	}

	return Detail::makeFailCode(result, false);
}
Detail::FailCode checkColourMasks(small_nat32 redmask,
								  small_nat32 greenmask,
								  small_nat32 bluemask,
								  Compression compression) noexcept
{
	bool		result;
	small_nat32 orCombined = (redmask | greenmask | bluemask);

	// the second part of the test is for when all three
	// masks have the same bit set, which is not tested in
	// the first test
	if ((((redmask ^ greenmask) ^ bluemask) != orCombined)
		|| ((redmask & greenmask & bluemask) != 0))
	{
		// then we have that the masks overlap
		result = false;
	} else
	{
		// ie: non-0 masks only valid for bitfield
		result = ((compression == Compression::BITFIELD)
				  || (orCombined == 0));
	}

	return Detail::makeFailCode(result, false);
}
Detail::FailCode checkValidColourSpace(ColourSpaceType colourspace,
									   bool v5Header) noexcept
{
	bool result;

	switch (colourspace)
	{
		case ColourSpaceType::sRGB:
			[[fallthrough]];
		case ColourSpaceType::Windows_Colour:
			[[fallthrough]];
		case ColourSpaceType::RGB_Calibrated:
			result = true;
			break;
		case ColourSpaceType::Profile_Linked:
			[[fallthrough]];
		case ColourSpaceType::Profile_Embedded:
			if (v5Header)
			{
				result = true;
				break;
			}

			[[fallthrough]];
		default:
			result = false;
			break;
	}

	return Detail::makeFailCode(result, false);
}
Detail::FailCode checkValidIntent(Intent intent) noexcept
{
	bool result;

	switch (intent)
	{
		case Intent::Business:
			[[fallthrough]];
		case Intent::Colourmetric:
			[[fallthrough]];
		case Intent::Graphics:
			[[fallthrough]];
		case Intent::Image:
			result = true;
			break;
		default:
			result = false;
			break;
	}

	return Detail::makeFailCode(result, false);
}
//
Detail::FailCode checkSupportedCompression(
	Compression compression) noexcept
{
	switch (compression)
	{
		case Compression::RLE4:
			[[fallthrough]];
		case Compression::RLE8:
			[[fallthrough]];
		case Compression::RGB:
			[[fallthrough]];
		case Compression::BITFIELD:
			return Detail::makeFailCode(true, true);
		default:
			return Detail::makeFailCode(false, false);
	}
}
Detail::FailCode checkSupportedColourSpace(
	ColourSpaceType colourspace) noexcept
{
	// basically, we only support this
	return Detail::makeFailCode(colourspace == ColourSpaceType::sRGB,
								true);
}


// ColourMasks functions
// public
ColourMasks::ColourMasks() noexcept
	: ColourMasks(0xFF0000, 0x00FF00, 0x0000FF)
{}
ColourMasks::ColourMasks(small_nat32 redMask, small_nat32 greenMask,
						 small_nat32 blueMask) noexcept
	: ColourMasks(redMask, greenMask, blueMask, 0)
{}
ColourMasks::ColourMasks(small_nat32 redMask, small_nat32 greenMask,
						 small_nat32 blueMask,
						 small_nat32 alphaMask) noexcept
	: m_RedMask(redMask),
	  m_GreenMask(greenMask),
	  m_BlueMask(blueMask),
	  m_AlphaMask(alphaMask)
{
	small_nat32 orCombined = (m_RedMask | m_GreenMask | m_BlueMask
							  | m_AlphaMask),
				xorCombined =
					(((m_RedMask ^ m_GreenMask) ^ m_BlueMask)
					 ^ m_AlphaMask),
				andCombined = (m_RedMask & m_GreenMask & m_BlueMask
							   & m_AlphaMask);
	if ((xorCombined != orCombined) || (andCombined != 0))
	{
		getErrorLog()->addError<InvalidUserInput>(
			FILELOC_UTF8,
			u8"RGB masks were provided which had overlapping bits. "
			u8"This may lead to incorrect or strange results"_ustring);
	}

	if (m_RedMask == 0)
	{
		m_RedMask = 0xFF0000;

		getErrorLog()->addError<InvalidUserInput>(
			FILELOC_UTF8,
			u8"Red Mask was zero! Setting to default mask..."_ustring);
	}
	if (m_GreenMask == 0)
	{
		m_BlueMask = 0x00FF00;

		getErrorLog()->addError<InvalidUserInput>(
			FILELOC_UTF8,
			u8"Green Mask was zero! Setting to default mask..."_ustring);
	}
	if (m_BlueMask == 0)
	{
		m_BlueMask = 0x0000FF;

		getErrorLog()->addError<InvalidUserInput>(
			FILELOC_UTF8,
			u8"Blue Mask was zero! Setting to default mask..."_ustring);
	}

	m_RedStep	= 1.f / getMaxRedComponent();
	m_GreenStep = 1.f / getMaxGreenComponent();
	m_BlueStep	= 1.f / getMaxBlueComponent();
	m_AlphaStep = 1.f / getMaxAlphaComponent();
}
//
small_nat32 ColourMasks::getMaxRedComponent() const noexcept
{
	// the intrinsic expects unsigned long explicitly, so
	// don't use typical 'small_nat32'
	unsigned long index;

	// find the position of the first non-0 bit: this is
	// equivalent to the number of leading zeros
	auto res = _BitScanForward(&index, m_RedMask);
	runtime_assert(res != 0);

	return m_RedMask >> index;
}
small_nat32 ColourMasks::getMaxGreenComponent() const noexcept
{
	// the intrinsic expects unsigned long explicitly, so
	// don't use typical 'small_nat32'
	unsigned long index;

	// find the position of the first non-0 bit: this is
	// equivalent to the number of leading zeros
	auto res = _BitScanForward(&index, m_GreenMask);
	runtime_assert(res != 0);

	return m_GreenMask >> index;
}
small_nat32 ColourMasks::getMaxBlueComponent() const noexcept
{
	// the intrinsic expects unsigned long explicitly, so
	// don't use typical 'small_nat32'
	unsigned long index;

	// find the position of the first non-0 bit: this is
	// equivalent to the number of leading zeros
	auto res = _BitScanForward(&index, m_BlueMask);
	runtime_assert(res != 0);

	return m_BlueMask >> index;
}
small_nat32 ColourMasks::getMaxAlphaComponent() const noexcept
{
	// the intrinsic expects unsigned long explicitly, so
	// don't use typical 'small_nat32'
	unsigned long index;

	// find the position of the first non-0 bit: this is
	// equivalent to the number of leading zeros
	auto res = _BitScanForward(&index, m_AlphaMask);

	if (res != 0)
	{
		return m_AlphaMask >> index;
	} else
	{
		return 0;
	}
}
//
small_nat8 ColourMasks::getRedComponent(
	small_nat32 rawData) const noexcept
{
	return static_cast<small_nat8>(getRedComponentFloat(rawData)
								   * 255.0f);
}
small_nat8 ColourMasks::getGreenComponent(
	small_nat32 rawData) const noexcept
{
	return static_cast<small_nat8>(getGreenComponentFloat(rawData)
								   * 255.0f);
}
small_nat8 ColourMasks::getBlueComponent(
	small_nat32 rawData) const noexcept
{
	return static_cast<small_nat8>(getBlueComponentFloat(rawData)
								   * 255.0f);
}
small_nat8 ColourMasks::getAlphaComponent(
	small_nat32 rawData) const noexcept
{
	return static_cast<small_nat8>(getAlphaComponentFloat(rawData)
								   * 255.0f);
}
//
float ColourMasks::getRedComponentFloat(
	small_nat32 rawData) const noexcept
{
	// the intrinsic expects unsigned long explicitly, so
	// don't use typical 'small_nat32'
	unsigned long index;

	// find the position of the first non-0 bit: this is
	// equivalent to the number of leading zeros
	auto res = _BitScanForward(&index, m_RedMask);
	runtime_assert(res != 0);

	// get raw data
	small_nat32 component = (rawData & m_RedMask) >> index;

	// scale to size of raw data
	return static_cast<float>(component) * m_RedStep;
}
float ColourMasks::getGreenComponentFloat(
	small_nat32 rawData) const noexcept
{
	// the intrinsic expects unsigned long explicitly, so
	// don't use typical 'small_nat32'
	unsigned long index;

	// find the position of the first non-0 bit: this is
	// equivalent to the number of leading zeros
	auto res = _BitScanForward(&index, m_GreenMask);
	runtime_assert(res != 0);

	// get raw data
	small_nat32 component = (rawData & m_GreenMask) >> index;

	// scale to size of raw data
	return static_cast<float>(component) * m_GreenStep;
}
float ColourMasks::getBlueComponentFloat(
	small_nat32 rawData) const noexcept
{
	// the intrinsic expects unsigned long explicitly, so
	// don't use typical 'small_nat32'
	unsigned long index;

	// find the position of the first non-0 bit: this is
	// equivalent to the number of leading zeros
	auto res = _BitScanForward(&index, m_BlueMask);
	runtime_assert(res != 0);

	// get raw data
	small_nat32 component = (rawData & m_BlueMask) >> index;

	// scale to size of raw data
	return static_cast<float>(component) * m_BlueStep;
}
float ColourMasks::getAlphaComponentFloat(
	small_nat32 rawData) const noexcept
{
	if (m_AlphaMask == 0)
	{
		return 1.f;
	}

	// the intrinsic expects unsigned long explicitly, so
	// don't use typical 'small_nat32'
	unsigned long index;

	// find the position of the first non-0 bit: this is
	// equivalent to the number of leading zeros
	auto res = _BitScanForward(&index, m_AlphaMask);
	runtime_assert(res != 0);

	// get raw data
	small_nat32 component = (rawData & m_AlphaMask) >> index;

	// scale to size of raw data
	return static_cast<float>(component) * m_AlphaStep;
}


// Colour functions
Colour::Colour(small_nat32 rawData) noexcept
	: Colour(ColourMasks(), rawData)
{}
Colour::Colour(const ColourMasks& masks, small_nat32 rawData) noexcept
	: m_RedChannel(masks.getRedComponent(rawData)),
	  m_GreenChannel(masks.getGreenComponent(rawData)),
	  m_BlueChannel(masks.getBlueComponent(rawData)),
	  m_AlphaChannel(masks.getAlphaComponent(rawData))
{}
//
small_nat32 Colour::getB8G8R8A8() const noexcept
{
	return ((m_AlphaChannel << 24) | (m_RedChannel << 16)
			| (m_GreenChannel << 8) | (m_BlueChannel));
}
//
small_nat8 Colour::getRed() const noexcept
{
	return m_RedChannel;
}
small_nat8 Colour::getGreen() const noexcept
{
	return m_GreenChannel;
}
small_nat8 Colour::getBlue() const noexcept
{
	return m_BlueChannel;
}
small_nat8 Colour::getAlpha() const noexcept
{
	return m_AlphaChannel;
}
float Colour::getRedFloat() const noexcept
{
	return static_cast<float>(m_RedChannel) / 255.0f;
}
float Colour::getGreenFloat() const noexcept
{
	return static_cast<float>(m_GreenChannel) / 255.0f;
}
float Colour::getBlueFloat() const noexcept
{
	return static_cast<float>(m_BlueChannel) / 255.0f;
}
float Colour::getAlphaFloat() const noexcept
{
	return static_cast<float>(m_AlphaChannel) / 255.0f;
}


Palette::Palette(const small_nat8* dataBuffer, natural64 bufferSize,
				 const InfoHeader& header)
	: m_Palette(), m_Masks()
{
	if (header.hasColourPalette())
	{
		// then we have a palette

		const natural64 headersSize =
			Header::sizeInFile() + header.currentSizeInFile();
		const natural64 maxOffset =
			headersSize + (header.colourPaletteSize() * 4);

		if (bufferSize < maxOffset)
		{
			throw getErrorLog()->createException<DataAcquisition>(
				FILELOC_UTF8,
				u8"Bitmap: data buffer provided was not large enough to hold palette specified in info header!"_ustring);
		}

		const small_nat8* paletteStart = dataBuffer + headersSize;

		for (natural64 index = 0; index < header.colourPaletteSize();
			 ++index)
		{
			m_Palette.emplace_back(
				*reinterpret_cast<const small_nat32*>(paletteStart
													  + (index * 4)));
		}

		return;
	} else if (header.hasColourMasks())
	{
		// then we have colour masks - get them
		m_Masks = header.colourMasks();

		// if we have valid masks, use them
		if (m_Masks) return;
	}

	// then we have nothing :'(

	// set the colour masks to the default ones, since
	// they're what'll be used for the bitmap, in this case
	switch (header.getBitcount())
	{
		case 16:
			m_Masks =
				ColourMasks(0b111110000000000, 0b000001111100000,
							0b000000000011111);
			break;
		case 24:
			[[fallthrough]];
		case 32:
			// default colourmasks do what we need here
			break;
		default:
			// should not happen

			throw getErrorLog()->createException<InvalidUserInput>(
				FILELOC_UTF8,
				u8"Somehow, the combination of parameters "
				u8"was incorrect - either the bitcount was "
				u8"non-standard, or it conflicted with the compression mode"_ustring);
	}
}
//
Colour Palette::mask(small_nat32 rawData) const noexcept
{
	return Colour(m_Masks, rawData);
}
//
natural64 Palette::paletteSize() const noexcept
{
	return m_Palette.size();
}
//
Colour& Palette::operator[](natural64 index) noexcept
{
	runtime_assert(index < m_Palette.size());

	return m_Palette[index];
}
const Colour& Palette::operator[](natural64 index) const noexcept
{
	runtime_assert(index < m_Palette.size());

	return m_Palette[index];
}


// BitmapFileData functions
////private
// void BitmapFileData::createClear(small_nat32
// width, small_nat32 height, Colour clearColour) noexcept
//{
//	m_Bitmap.clear();
//	m_Bitmap.reserve(height);
//
//	for(small_nat32 heightIndex = 0; heightIndex < height;
//++heightIndex)
//	{
//		m_Bitmap.emplace_back();
//		auto& row = m_Bitmap.back();
//
//		for(small_nat32 widthIndex = 0; widthIndex < width;
//++widthIndex)
//		{
//			row.emplace_back(clearColour);
//		}
//
//		runtime_assert(row.size() == width);
//	}
//
//	runtime_assert(m_Bitmap.size() == height);
//}
//
void BitmapFileData::normalParse(
	const InfoHeader& infoHeader, const Palette& palette,
	const small_nat8* bitmapBuffer, natural64 remainingBuffer,
	small_nat32 realWidth, small_nat32 realHeight, bool bottomUp)
{
	small_nat32 minRowBits = realWidth * infoHeader.getBitcount(),
				offByteBoundary = (minRowBits % 8) ? 1 : 0,
				minByteBound = ((minRowBits / 8) + offByteBoundary),
				off4ByteBoundary = (minByteBound % 4) ? 1 : 0,
				min4ByteBound =
					(off4ByteBoundary == 1)
						? ((minByteBound / 4) + 1) * 4
						: minByteBound;	 // ensures we're on
										 // a 4 byte boundary

	if ((min4ByteBound * realHeight) > remainingBuffer)
	{
		throw getErrorLog()->createException<DataAcquisition>(
			FILELOC_UTF8,
			u8"Bitmap: data buffer provided was not large "
			u8"enough for the specifed bitmap size (when "
			u8"taking into account row 4-byte alignment)!"_ustring);
	}

	for (small_nat32 rowIndex = 0; rowIndex < realHeight; ++rowIndex)
	{
		// TODO: move this check somewhere more sensible,
		// now that we don't need to check this independently
		// for each row
		runtime_assert(m_Bitmap.getDimensionSize(1) == realWidth);

		small_nat32 realRowIndex =
			bottomUp ? rowIndex : (realHeight - rowIndex - 1);
		const small_nat8* rowStart =
			bitmapBuffer + (realRowIndex * min4ByteBound);

		for (small_nat32 pixelIndex = 0; pixelIndex < realWidth;
			 ++pixelIndex)
		{
			small_nat32 bitcount		= infoHeader.getBitcount(),
						byteIndex		= (pixelIndex * bitcount) / 8;
			const small_nat8* byteStart = (rowStart + byteIndex);

			switch (bitcount)
			{
				case 1: {
					small_nat32 offset = 7 - (pixelIndex % 8);
					small_nat8	byte   = ((*byteStart) >> offset)
									  & 0x1;  // extract actual index

					if (byte < palette.paletteSize())
					{
						m_Bitmap[{rowIndex, pixelIndex}] =
							palette[byte];
						// m_Bitmap[rowIndex][pixelIndex] =
						// palette[byte];
					}
				}

				break;
				case 4: {
					small_nat32 offset = 4 - ((pixelIndex % 2) * 4);
					small_nat8	byte   = ((*byteStart) >> offset)
									  & 0xF;  // extract actual index

					if (byte < palette.paletteSize())
					{
						m_Bitmap[{rowIndex, pixelIndex}] =
							palette[byte];
						// m_Bitmap[rowIndex][pixelIndex] =
						// palette[byte];
					}
				}

				break;
				case 8: {
					small_nat8 byte = *byteStart;

					if (byte < palette.paletteSize())
					{
						m_Bitmap[{rowIndex, pixelIndex}] =
							palette[byte];
						// m_Bitmap[rowIndex][pixelIndex] =
						// palette[byte];
					}
				}

				break;
				case 16: {
					small_nat32 byte4Data =
						*reinterpret_cast<const small_nat32*>(
							byteStart);
					if (!(pixelIndex % 2))
					{
						// then we are on an even number of
						// pixels
						byte4Data = (byte4Data >> 16);
					}

					m_Bitmap[{rowIndex, pixelIndex}] =
						palette.mask(byte4Data & 0xFFFF);
					// m_Bitmap[rowIndex][pixelIndex] =
					// palette.mask(byte4Data & 0xFFFF);
				}

				break;
				default:

					m_Bitmap[{rowIndex, pixelIndex}] = palette.mask(
						*reinterpret_cast<const small_nat32*>(
							byteStart));
					// m_Bitmap[rowIndex][pixelIndex] =
					// palette.mask(*reinterpret_cast<const
					// small_nat32*>(byteStart));
					break;
			}
		}
	}
}
void BitmapFileData::rle4Parse(const Palette&	 palette,
							   const small_nat8* bitmapBuffer,
							   natural64 remainingBuffer) noexcept
{
	small_nat32 widthIndex = 0, heightIndex = 0;
	for (natural64 dataOffset = 0; dataOffset < remainingBuffer;)
	{
		small_nat8 firstByte  = bitmapBuffer[dataOffset],
				   secondByte = bitmapBuffer[dataOffset + 1];

		if (firstByte == 0)
		{
			// then we have special 'absolute' sequence
			switch (secondByte)
			{
				case 0:
					// end of line
					++heightIndex;
					widthIndex = 0;

					dataOffset += 2;
					break;
				case 1:
					// end of bitmap
					return;
				case 2:
					// 2-D offset to width, height indices
					widthIndex += bitmapBuffer[dataOffset + 2];
					heightIndex += bitmapBuffer[dataOffset + 3];

					dataOffset += 4;
					break;
				default:
					// run of individual pixels - will
					// always end on a word boundary!
					for (small_nat8 pixelNum = 0;
						 pixelNum < secondByte;
						 ++pixelNum, ++widthIndex)
					{
						small_nat8 dataByte =
							bitmapBuffer[dataOffset + 2
										 + (pixelNum / 2)];

						if ((pixelNum % 2) == 0)
						{
							dataByte = dataByte >> 4;
						}

						dataByte = dataByte & 0xF;
						runtime_assert(dataByte
									   < palette.paletteSize());

						if (widthIndex
							>= m_Bitmap.getDimensionSize(1))
						{
							// move to new row
							++heightIndex;
							widthIndex = 0;
						}

						runtime_assert(
							heightIndex
							< m_Bitmap.getDimensionSize(0));
						m_Bitmap[{heightIndex, widthIndex}] =
							palette[dataByte];
						// m_Bitmap[heightIndex][widthIndex]
						// = palette[dataByte];
					}

					// ensure that we ignore things we read
					// (padding rest of byte), and extra
					// padding byte, if it's there
					dataOffset +=
						2 + (secondByte / 2)
						+ (secondByte % 2) /* + (((secondByte % 4) !=
											  0) ? 1: 0)*/
						;
					dataOffset += (dataOffset % 2);
			}
		} else
		{
			// then we have a standard 'encoded' sequence
			small_nat8 high = (secondByte >> 4) & 0xF,
					   low	= secondByte & 0xF;
			runtime_assert(high < palette.paletteSize());
			runtime_assert(low < palette.paletteSize());

			for (small_nat8 pixelIndex = 0; pixelIndex < firstByte;
				 ++pixelIndex, ++widthIndex)
			{
				if (widthIndex >= m_Bitmap.getDimensionSize(1))
				{
					// move to new row
					++heightIndex;
					widthIndex = 0;
				}

				runtime_assert(heightIndex
							   < m_Bitmap.getDimensionSize(0));
				m_Bitmap[{heightIndex, widthIndex}] =
					palette[(((pixelIndex % 2) != 0) ? low : high)];
				// m_Bitmap[heightIndex][widthIndex] =
				// palette[(((pixelIndex % 2) != 0) ? low :
				// high)];
			}

			dataOffset += 2;
		}
	}
}
void BitmapFileData::rle8Parse(const Palette&	 palette,
							   const small_nat8* bitmapBuffer,
							   natural64 remainingBuffer) noexcept
{
	small_nat32 widthIndex = 0, heightIndex = 0;
	for (natural64 dataOffset = 0; dataOffset < remainingBuffer;)
	{
		small_nat8 firstByte  = bitmapBuffer[dataOffset],
				   secondByte = bitmapBuffer[dataOffset + 1];

		if (firstByte == 0)
		{
			// then we have a special 'absolute' sequence
			switch (secondByte)
			{
				case 0:
					// end of line
					++heightIndex;
					widthIndex = 0;

					dataOffset += 2;
					break;
				case 1:
					// end of bitmap
					return;
				case 2:
					// 2-D offset to width, height indices
					widthIndex += bitmapBuffer[dataOffset + 2];
					heightIndex += bitmapBuffer[dataOffset + 3];

					dataOffset += 4;
					break;
				default:
					// run of individual pixels - will
					// always end on a word boundary!
					for (small_nat8 pixelNum = 0;
						 pixelNum < secondByte;
						 ++pixelNum, ++widthIndex)
					{
						small_nat8 colourIndex =
							bitmapBuffer[dataOffset + 2 + pixelNum];
						runtime_assert(colourIndex
									   < palette.paletteSize());

						if (widthIndex
							>= m_Bitmap.getDimensionSize(1))
						{
							// move to new row
							++heightIndex;
							widthIndex = 0;
						}

						runtime_assert(
							heightIndex
							< m_Bitmap.getDimensionSize(0));
						m_Bitmap[{heightIndex, widthIndex}] =
							palette[colourIndex];
						// m_Bitmap[heightIndex][widthIndex]
						// = palette[colourIndex];
					}

					// ensure that we ignore things we read,
					// and padding byte, if it's there
					dataOffset += 2 + secondByte + (secondByte % 2);
			}
		} else
		{
			// then we have a standard 'encoded' sequence
			runtime_assert(secondByte < palette.paletteSize());

			while (firstByte != 0)
			{
				if (widthIndex >= m_Bitmap.getDimensionSize(1))
				{
					// move to new row
					++heightIndex;
					widthIndex = 0;
				}

				runtime_assert(heightIndex
							   < m_Bitmap.getDimensionSize(0));
				m_Bitmap[{heightIndex, widthIndex}] =
					palette[secondByte];
				// m_Bitmap[heightIndex][widthIndex] =
				// palette[secondByte];

				++widthIndex;
				--firstByte;
			}

			dataOffset += 2;
		}
	}
}
// public
BitmapFileData::BitmapFileData(const Header&	 header,
							   const InfoHeader& infoHeader,
							   const Palette&	 palette,
							   const small_nat8* dataBuffer,
							   natural64		 bufferSize)
	: m_Bitmap()
{
	Colour clearColour(0);

	if (palette.paletteSize() > 0)
	{
		clearColour = palette[0];
	}

	small_int32 width  = infoHeader.getWidth(),
				height = infoHeader.getHeight();
	bool bottomUp	   = true;
	runtime_assert(width > 0);

	if (height < 0)
	{
		bottomUp = false;
		height	 = -height;
	}
	small_nat32 realWidth  = static_cast<small_nat32>(width),
				realHeight = static_cast<small_nat32>(height);

	// clear to standard colour! (palette[0] or black, if no
	// palette)
	m_Bitmap = ContiguousArray<Colour, 2>({realHeight, realWidth},
										  clearColour);
	// createClear(realWidth, realHeight, clearColour);

	natural64 dataOffset	  = header.bitmapOffset(),
			  remainingBuffer = (bufferSize > dataOffset)
									? (bufferSize - dataOffset)
									: 0;
	const small_nat8* dataStart = dataBuffer + dataOffset;

	switch (infoHeader.getCompression())
	{
		case Compression::RLE4:
			rle4Parse(palette, dataStart, remainingBuffer);
			break;
		case Compression::RLE8:
			rle8Parse(palette, dataStart, remainingBuffer);
			break;
		default:
			normalParse(infoHeader, palette, dataStart,
						remainingBuffer, realWidth, realHeight,
						bottomUp);
			break;
	}
}
//
// auto BitmapFileData::getColourRow(natural64
// heightIndex) const noexcept -> const
// std::vector<Colour>&
//{
//	runtime_assert(heightIndex <
// m_Bitmap.getDimensionSize(0));
//
//	return m_Bitmap[heightIndex];
//}
auto BitmapFileData::getB8G8R8A8ArrayCopy(bool reverseRows) const
	-> ContiguousArray<small_nat32, 2>
{
	// this will initialise an array of the right size, with default
	// initialisation of all members
	frao::natural64 numRows	   = m_Bitmap.getDimensionSize(0),
					numColumns = m_Bitmap.getDimensionSize(1);
	ContiguousArray<small_nat32, 2> result({numRows, numColumns});

	runtime_assert(
		result.getDimensionSize(0) == m_Bitmap.getDimensionSize(0),
		"Copied array does not have the same size");
	runtime_assert(
		result.getDimensionSize(1) == m_Bitmap.getDimensionSize(1),
		"Copied array does not have the same size");

	auto dstIter = result.begin();

	for (natural64 rowIndex = 0; rowIndex < numRows; ++rowIndex)
	{
		natural64 srcRowIndex =
			reverseRows ? (numRows - 1) - rowIndex : rowIndex;
		auto* srcPtr = m_Bitmap.data() + (srcRowIndex * numColumns);

		for (natural64 columnIndex = 0; columnIndex < numColumns;
			 ++columnIndex, ++srcPtr, ++dstIter)
		{
			frao::runtime_assert(
				(srcPtr - m_Bitmap.data()) < m_Bitmap.size(),
				"Arrays were initialised to the same "
				"size, so this should be impossible");
			frao::runtime_assert(
				dstIter != result.end(),
				"Arrays were initialised to the same "
				"size, so this should be impossible");

			*dstIter = srcPtr->getB8G8R8A8();
		}
	}

	return result;
}
auto BitmapFileData::getR32G32B32A32_floatArrayCopy(
	bool reverseRows) const -> ContiguousArray<float, 2>
{
	// this will initialise an array of the right size, with default
	// initialisation of all members
	frao::natural64 numRows	   = m_Bitmap.getDimensionSize(0),
					numColumns = m_Bitmap.getDimensionSize(1);
	ContiguousArray<float, 2> result({numRows, numColumns * 4});

	runtime_assert(
		result.getDimensionSize(0) == m_Bitmap.getDimensionSize(0),
		"Copied array does not have the same size");
	runtime_assert(result.getDimensionSize(1)
					   == (m_Bitmap.getDimensionSize(1) * 4),
				   "Copied array does not have the same size");

	auto dstIter = result.begin();
	for (natural64 rowIndex = 0; rowIndex < numRows; ++rowIndex)
	{
		natural64 srcRowIndex =
			reverseRows ? (numRows - 1) - rowIndex : rowIndex;
		auto* srcPtr = m_Bitmap.data() + (srcRowIndex * numColumns);

		for (natural64 columnIndex = 0; columnIndex < numColumns;
			 ++columnIndex, ++srcPtr)
		{
			frao::runtime_assert(
				(srcPtr - m_Bitmap.data()) < m_Bitmap.size(),
				"Arrays were initialised to the same "
				"size, so this should be impossible");

			frao::runtime_assert(
				dstIter != result.end(),
				"Arrays were initialised to the correct (dst = "
				"4*src) size, so this should be impossible");
			*dstIter = srcPtr->getRedFloat();
			++dstIter;

			frao::runtime_assert(
				dstIter != result.end(),
				"Arrays were initialised to the correct (dst = "
				"4*src) size, so this should be impossible");
			*dstIter = srcPtr->getGreenFloat();
			++dstIter;

			frao::runtime_assert(
				dstIter != result.end(),
				"Arrays were initialised to the correct (dst = "
				"4*src) size, so this should be impossible");
			*dstIter = srcPtr->getBlueFloat();
			++dstIter;

			frao::runtime_assert(
				dstIter != result.end(),
				"Arrays were initialised to the correct (dst = "
				"4*src) size, so this should be impossible");
			*dstIter = srcPtr->getAlphaFloat();
			++dstIter;
		}
	}

	return result;
}
Colour BitmapFileData::getColourAt(
	natural64 heightIndex, natural64 widthIndex) const noexcept
{
	runtime_assert(heightIndex < m_Bitmap.getDimensionSize(0));
	runtime_assert(widthIndex < m_Bitmap.getDimensionSize(1));

	return m_Bitmap[{heightIndex, widthIndex}];
}
//
small_nat32 BitmapFileData::getBitmapHeight() const noexcept
{
	return static_cast<small_nat32>(m_Bitmap.getDimensionSize(0));
}
small_nat32 BitmapFileData::getBitmapWidth() const noexcept
{
	return static_cast<small_nat32>(m_Bitmap.getDimensionSize(1));
	// return (m_Bitmap.size() != 0) ?
	// static_cast<small_nat32>(m_Bitmap[0].size()) : 0;
}

}  // namespace Bitmap
}  // namespace FileFormats
}  // namespace FileIO
}  // namespace frao