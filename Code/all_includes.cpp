#include "Basic\Classes.hpp"
#include "Basic\IOTypes.hpp"
#include "Basic\Parsing.hpp"
#include "Formats\Bitmap.hpp"
#include "Manager\Organisation.hpp"
#include "Manager\Control.hpp"
#include "Version\VersionIncl.hpp"

// this file exists to ensure that every header has been compiled at
// least once