//! \file
//!
//! \brief Implementation of translation of raw file data into
//! intelligible information
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "Basic\Parsing.hpp"
#include <Nucleus_inc\Error.hpp>

namespace frao
{
inline namespace FileIO
{
using namespace Strings::Operators;

//==============================================
//---------------FileParse class----------------
//==============================================
// Base class for functors that implement parsing
// of a given file type to parse from
//==============================================

FileParse::FileParse(Strings::widestring extension)
	: m_FileExtension(extension)
{}
//
Strings::widestring FileParse::getExtension() const
{
	return m_FileExtension;
}
//
FileParse& FileParse::operator=(const FileParse& rhs)
{
	// this shouldn't happen unless we slice, but no harm in
	// putting it anyway...
	runtime_assert(m_FileExtension == rhs.getExtension());

	return *this;
}

//==============================================
//---------------txtParse class-----------------
//==============================================
// cderived class for parsing a txt file from a
// FileBuffer
//==============================================


txtParse::txtParse() : FileParse(defExtension()), m_Data()
{}
//
Strings::utf8string txtParse::getData() const
{
	return m_Data;
}
//
void txtParse::operator()(FileBuffer buffer)
{
	m_Data = Strings::utf8string{
		reinterpret_cast<charU8*>(buffer.unsafeBufferRef()),
		buffer.size()};
}
//
Strings::widestring txtParse::defExtension()
{
	return L".txt"_ustring;
}
FileBuffer txtParse::getBuffer(Strings::utf8string data)
{
	FileBuffer result(data.byteSize());

	// we need to iterate through string, cluster,
	// codepoint, etc, to get all the bytes!
	natural64 resultByteIndex = 0;
	for (natural64 clusterindex = 0; clusterindex < data.size();
		 ++clusterindex)
	{
		const auto& cluster = data[clusterindex];
		for (natural64 codepointindex = 0;
			 codepointindex < cluster.size(); ++codepointindex)
		{
			const auto& codepoint = cluster[codepointindex];
			for (natural64 byteindex = 0;
				 byteindex < codepoint.size(); ++byteindex)
			{
				result[resultByteIndex] = codepoint[byteindex];
				++resultByteIndex;
			}
		}
	}

	return result;
}

//==============================================
//---------------csoParse class-----------------
//==============================================
// derived class for parsing a cso file (a cso
// file is a shader bytes file) from a FileBuffer
//==============================================

csoParse::csoParse()
	: FileParse(defExtension()), m_Data(nullptr), m_Size(0)
{}
csoParse::~csoParse() noexcept
{
	SafeArrayDelete(&m_Data);
}
//
const natural8* csoParse::getBuffer() const
{
	return m_Data;
}
natural64 csoParse::bufferSize() const
{
	return m_Size;
}
//
void csoParse::operator()(FileBuffer buffer)
{
	m_Size = buffer.size();

	SafeArrayDelete(&m_Data);

	try
	{
		m_Data = new natural8[m_Size];
	} catch (std::bad_alloc&)
	{
		throw getErrorLog()->createException<MemAlloc>(FILELOC_UTF8,
													   u8"");
	}

	for (natural64 i = 0; i < m_Size; ++i)
		m_Data[i] = buffer[i];
}
//
Strings::widestring csoParse::defExtension()
{
	return L".cso"_ustring;
}

//==============================================
//-------------fraocfgParse class-------------
//==============================================
// derived class for parsing a fraocfg file
// from a FileBuffer
//==============================================

void fraocfgParse::parseDirectory(Strings::widestring buffer)
{
	runtime_assert(buffer.size() != 0);

	if (buffer[buffer.size() - 1] != L"\\"_ucluster)
		buffer << L"\\"_ucluster;

	m_Directory = Path(std::move(buffer));
}
void fraocfgParse::parseFiles(Strings::widestring buffer)
{
	m_FilePaths.clear();
	runtime_assert(buffer.size() != 0);

	natural64 loc = buffer.findNext(L"|"_ustring, 0), pastloc = 0;

	while (loc != decltype(buffer)::npos)
	{
		m_FilePaths.emplace_back(
			buffer.subStr(pastloc, loc - pastloc));

		pastloc = loc + 1;
		loc		= buffer.findNext(L"|"_ustring, pastloc);
	}
}
//
fraocfgParse::fraocfgParse()
	: FileParse(defExtension()), m_Directory(), m_FilePaths()
{}
//
Path fraocfgParse::getDirectory() const
{
	return m_Directory;
}
std::vector<Path> fraocfgParse::getFiles() const
{
	return m_FilePaths;
}
//
void fraocfgParse::operator()(FileBuffer buffer)
{
	Strings::utf8string data{
		reinterpret_cast<charU8*>(buffer.unsafeBufferRef()),
		buffer.size()};
	runtime_assert(data.size() != 0);

	natural64 dirstart = data.findNext(u8"<"_ustring, 0)
						 + 1,  //+1 to get index of first
							   // cluster AFTER "<"
		dirend			 = data.findNext(u8">"_ustring, dirstart),
			  filesStart = data.findNext(u8"<"_ustring, dirend)
						   + 1,  //+1 to get index of first cluster
								 // AFTER "<"
		filesend = data.findNext(u8">"_ustring, filesStart);

	// checkl to see all is formatted well, etc.
	if ((dirstart >= data.size()) || (dirend >= data.size())
		|| (filesStart >= data.size()) || (filesend >= data.size()))
	{
		throw getErrorLog()->createException<InvalidFileFormat>(FILELOC_UTF8, static_cast<Strings::utf8string>(defExtension())
			+ u8" file not correctly formatted: incorrect number of <> braces!"_ustring);
	} else if (dirend <= dirstart)
	{
		throw getErrorLog()->createException<InvalidFileFormat>(FILELOC_UTF8, static_cast<Strings::utf8string>(defExtension())
			+ u8" file not correctly formatted: directory brace empty!"_ustring);
	} else if (filesend <= filesStart)
	{
		throw getErrorLog()->createException<InvalidFileFormat>(FILELOC_UTF8, static_cast<Strings::utf8string>(defExtension())
			+ u8" file not correctly formatted: files brace empty!"_ustring);
	}

	// get substrings for inside of directory braces (<>),
	// and files braces, and send to relevant parse section
	parseDirectory(static_cast<Strings::widestring>(
		data.subStr(dirstart, dirend - dirstart)));
	parseFiles(static_cast<Strings::widestring>(
		data.subStr(filesStart, filesend - filesStart)));
}
//
Strings::widestring fraocfgParse::defExtension()
{
	return L".fraocfg"_ustring;
}


namespace
{
template<typename DataType>
std::unique_ptr<FileFormats::Bitmap::InfoHeader> tryCreateInfoHeader(
	small_nat8* bufferPtr, natural64 bufferSize)
{
	std::unique_ptr<FileFormats::Bitmap::InfoHeader> result;

	try
	{
		result = std::make_unique<
			FileFormats::Bitmap::InfoHeaderIMPL<DataType>>(
			bufferPtr, bufferSize);

		return result;
	} catch (catch_type<InvalidFileFormat>&)
	{
		getErrorLog()->addError<InvalidFileFormat>(
			FILELOC_UTF8,
			u8"Bitmap: Could not parse info header of size: "_ustring
				+ Strings::utf8string(DataType::sizeInFile())
				+ u8". Header was not in expected format"_ustring);
	} catch (catch_type<DataAcquisition>&)
	{
		getErrorLog()->addError<DataAcquisition>(FILELOC_UTF8,
				u8"Bitmap: Could not parse info header of size: "_ustring + Strings::utf8string(DataType::sizeInFile())
					+ u8". Could not acquire enough data from the header"_ustring);
	}

	return result;
}

std::unique_ptr<FileFormats::Bitmap::InfoHeader> createInfoHeader(
	small_nat8* bufferPtr, natural64 bufferSize)
{
	using namespace FileFormats::Bitmap;

	std::unique_ptr<InfoHeader> result;
	small_nat32 infoHeaderSize = *reinterpret_cast<small_nat32*>(
		bufferPtr + Header::sizeInFile());

	if (infoHeaderSize >= InfoV5Data::sizeInFile())
	{
		if (infoHeaderSize > InfoV5Data::sizeInFile())
		{
			// WARN: non-standard size
			getErrorLog()->addError<InvalidFileFormat>(
				FILELOC_UTF8,
				u8"Bitmap warning: Info header was too big to be V5 header. "
				u8"Assuming it is V5"_ustring);
		}

		result =
			tryCreateInfoHeader<InfoV5Data>(bufferPtr, bufferSize);

		if (result)
		{
			return result;
		}
	}

	if (infoHeaderSize >= InfoV4Data::sizeInFile())
	{
		if (infoHeaderSize > InfoV4Data::sizeInFile())
		{
			// WARN: non-standard size
			getErrorLog()->addError<InvalidFileFormat>(
				FILELOC_UTF8,
				u8"Bitmap warning: Info header could not be a V5 header, "
				u8"but too big to be V4 header. Assuming V4"_ustring);
		}

		result =
			tryCreateInfoHeader<InfoV4Data>(bufferPtr, bufferSize);

		if (result)
		{
			return result;
		}
	}

	if (infoHeaderSize >= InfoData::sizeInFile())
	{
		if (infoHeaderSize > InfoData::sizeInFile())
		{
			// WARN: non-standard size
			getErrorLog()->addError<InvalidFileFormat>(
				FILELOC_UTF8,
				u8"Bitmap warning: Info header could not be a V4 header, "
				u8"but too big to be V3 header. Assuming V3"_ustring);
		}

		result = tryCreateInfoHeader<InfoData>(bufferPtr, bufferSize);
	}
	// else since we don't drop from V3 into V2, because
	// height and width are read differently there (ie: they
	// are 2bytes and unsigned, rather than 4 and signed),
	// which means a V3 / V4 / V5 info header with invalid
	// data CANNOT BE READ AS A V2 INFO HEADER
	else if (infoHeaderSize >= CoreInfoData::sizeInFile())
	{
		if (infoHeaderSize > CoreInfoData::sizeInFile())
		{
			// WARN: non-standard size
			getErrorLog()->addError<InvalidFileFormat>(
				FILELOC_UTF8,
				u8"Bitmap warning: Info header could not be a V3 header, "
				u8"but too big to be V2 header. Assuming V2"_ustring);
		}

		result =
			tryCreateInfoHeader<CoreInfoData>(bufferPtr, bufferSize);
	}

	if (!result)
	{
		// ERROR: info header too small! cannot read file!
		// must abort!
		throw getErrorLog()->createException<InvalidFileFormat>(
			FILELOC_UTF8,
			u8"Bitmap: info header could not be read. Aborting"_ustring);
	}

	return result;
}
}  // namespace


// bitmapParse functions
// public
bitmapParse::bitmapParse()
	: FileParse(defExtension()), m_MainHeader(), m_InfoHeader()
{}
//
void bitmapParse::operator()(FileBuffer buffer)
{
	using namespace FileFormats::Bitmap;

	small_nat8* bufferPtr  = buffer.unsafeBufferRef();
	natural64   bufferSize = buffer.size();

	try
	{
		m_MainHeader =
			std::make_unique<Header>(bufferPtr, bufferSize);
		m_InfoHeader = createInfoHeader(bufferPtr, bufferSize);
		m_Palette = std::make_unique<Palette>(bufferPtr, bufferSize,
											  *m_InfoHeader);
		runtime_assert(m_MainHeader && m_InfoHeader && m_Palette);

		m_BitmapData = std::make_unique<BitmapFileData>(
			*m_MainHeader, *m_InfoHeader, *m_Palette, bufferPtr,
			bufferSize);
	} catch (catch_type<InvalidFileFormat>&)
	{
		getErrorLog()->addError<InvalidFileFormat>(
			FILELOC_UTF8,
			u8"Could not parse bitmap: file was not in expected format"_ustring);
	} catch (catch_type<DataAcquisition>&)
	{
		getErrorLog()->addError<DataAcquisition>(
			FILELOC_UTF8,
			u8"Could not parse bitmap: could not acquire enough data from the file"_ustring);
	}
}
//
auto bitmapParse::getMainHeader() noexcept -> decltype(m_MainHeader)&
{
	return m_MainHeader;
}
auto bitmapParse::getMainHeader() const noexcept -> const
	decltype(m_MainHeader)&
{
	return m_MainHeader;
}
auto bitmapParse::getInfoHeader() noexcept -> decltype(m_InfoHeader)&
{
	return m_InfoHeader;
}
auto bitmapParse::getInfoHeader() const noexcept -> const
	decltype(m_InfoHeader)&
{
	return m_InfoHeader;
}
auto bitmapParse::getPalette() noexcept -> decltype(m_Palette)&
{
	return m_Palette;
}
auto bitmapParse::getPalette() const noexcept -> const
	decltype(m_Palette)&
{
	return m_Palette;
}
auto bitmapParse::getFileData() noexcept -> decltype(m_BitmapData)&
{
	return m_BitmapData;
}
auto bitmapParse::getFileData() const noexcept -> const
	decltype(m_BitmapData)&
{
	return m_BitmapData;
}
//
Strings::widestring bitmapParse::defExtension()
{
	return L".bmp"_ustring;
}
}  // namespace FileIO
}  // namespace frao