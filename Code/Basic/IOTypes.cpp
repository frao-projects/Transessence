//! \file
//!
//! \brief Implementation of File and directory abstractions
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "Basic\IOTypes.hpp"
#include <Windows.h>  //for fileIO stuff
#include <Nucleus_inc\Environment.hpp>
#include <Nucleus_inc\Error.hpp>

namespace frao
{
inline namespace FileIO
{
using namespace Strings::Operators;

//==============================================
//-----------------File class-------------------
//==============================================
// class for interacting with a file, in the file
// system of the computer
//==============================================

void File::openFile(bool write)
{
	if (m_FileHandle != nullptr) closeHandle();

	DWORD access, share, creation, flags;

	m_FileInfo.m_WriteAccess = write;

	if (m_FileInfo.m_WriteAccess)
	{
		access = GENERIC_WRITE | GENERIC_READ;
		share  = 0;  // TODO: should we change this to
					 // FILE_SHARE_READ, and lock on write?
		creation = CREATE_ALWAYS;  // TODO: this always resets the
								   // file to zero size! fix so that
								   // it only does that on complete
								   // re-write
	} else
	{
		access = GENERIC_READ;
		share  = FILE_SHARE_READ;  // TODO: should we add
								   // FILE_SHARE WRITE, and hook
								   // something to be notified if
								   // the file changes?
		creation = OPEN_EXISTING;
	}

	if (m_FileInfo.m_Async)
		flags = FILE_FLAG_OVERLAPPED;
	else
		flags = FILE_ATTRIBUTE_NORMAL;

	m_FileHandle = CreateFileW(
		static_cast<Strings::widestring>(fullPath()).data(), access,
		share, nullptr, creation, flags, nullptr);

	if (m_FileHandle == INVALID_HANDLE_VALUE)
	{
		m_FileHandle = nullptr;

		DWORD err = GetLastError();

		switch (err)
		{
			case ERROR_ACCESS_DENIED:
				throw getErrorLog()->createException<AccessViolation>(
					FILELOC_UTF8,
					u8"Access to the specified file is prohibited"_ustring);
			case ERROR_FILE_NOT_FOUND:
				throw getErrorLog()->createException<NotFound>(
					FILELOC_UTF8,
					u8"The Specified file does not exist"_ustring);
			default:
				throw getErrorLog()->createException<Unknown>(
					FILELOC_UTF8, u8"Could not open file"_ustring);
		}
	}
}
void File::closeHandle() noexcept
{
	if (m_FileHandle == nullptr) return;

	if ((m_WriteStatus == Status::IO_PENDING)
		|| (m_ReadStatus == Status::IO_PENDING))
	{
		m_WriteStatus = m_ReadStatus = Status::NONE;

		if (CancelIo(m_FileHandle) == TRUE)
		{
			// if we can get the IO to cancel
			DWORD bytes;

			// wait for operation to actually finish
			GetOverlappedResult(m_FileHandle, &m_AsyncInput, &bytes,
								TRUE);
		} else
		{
			// if we can't get the IO to cancel
			const natural max_iterations = 1000;  // about 5 seconds

			// loop until the io is done (up to a point)
			for (natural iteration = 0;
				 (iteration < max_iterations) && ioPending();
				 ++iteration)
			{
				Sleep(5);
			}
		}
	}

	// NOTE: THIS WILL BREAK UNDER A DEBUGGER, IF USED INCORRECTLY.
	// CloseHandle throws a SEH exception iff running under a
	// debugger, when CloseHandle is called twice, or for the wrong
	// thing
	CloseHandle(m_FileHandle);
	m_FileHandle = nullptr;
}
//
void File::checkReadStatus() const noexcept
{
	if (m_ReadStatus == Status::IO_PENDING)
	{
		// sleep the minumum amount of time, and let windows
		// call our callback routine (if appropriate)
		SleepEx(1, TRUE);
	}
}
void File::checkWriteStatus() const noexcept
{
	if (m_WriteStatus == Status::IO_PENDING)
	{
		// sleep the minumum amount of time, and let windows
		// call our callback routine (if appropriate)
		SleepEx(1, TRUE);
	}
}
//! \todo shouldn't we open the file (via openFile) in the
//! constructor, not in read / write?
File::File(PathSegment path, Directory* owner, bool async, bool write)
	: File(IORequired{path, async, write}, owner)
{}
#pragma region File::File(IORequired filedata, const Directory& owner)
File::File(IORequired filedata, Directory* owner)
	: m_FileInfo(filedata),
	  m_Buffer(0),
	  m_AsyncInput{},
	  m_Owner(owner),
	  m_FileHandle(nullptr),
	  m_ReadStatus(Status::NONE),
	  m_WriteStatus(Status::NONE)
{
	runtime_assert(filedata.m_FileName.type()
				   == PathSegmentType::FILE);

	auto attributes = getAttributes(fullPath());

	if (attributes)
	{
		// then the file existed
		if (attributes.m_Directory)
		{
			// then we actually had a directory!
			throw getErrorLog()->createException<InvalidFunctionCall>(
				FILELOC_UTF8,
				u8"because a directory was opened as if it was a file"_ustring);
		}
	} else
	{
		// then the file didn't exist
		if (!m_FileInfo.m_WriteAccess)
		{
			// then we wanted read access...
			throw getErrorLog()->createException<NotFound>(
				FILELOC_UTF8,
				u8"could not find file with name : "_ustring
					+ static_cast<Strings::widestring>(fullPath()));
		}
	}
}
#pragma endregion
//! \todo shouldn't we open the file (via openFile) in the
//! constructor, not in read / write?
#pragma region File::File(File&& file)
File::File(File&& file) noexcept
	: m_FileInfo(file.m_FileInfo),
	  m_Buffer(0),
	  m_AsyncInput{},
	  m_Owner(file.m_Owner),
	  m_FileHandle(file.m_FileHandle),
	  m_ReadStatus(Status::NONE),
	  m_WriteStatus(Status::NONE)
{
	// check if the file is in the middle IO. Block until it
	// isn't
	while (file.ioPending())
	{
		Sleep(3);
	}

	// copy/move remaining members
	m_AsyncInput  = file.m_AsyncInput;
	m_Buffer	  = std::move(file.m_Buffer);
	m_ReadStatus  = file.m_ReadStatus;
	m_WriteStatus = file.m_WriteStatus;

	// get rid of old File's data, where appropriate
	file.m_FileHandle = nullptr;
}
#pragma endregion
File::~File() noexcept
{
	closeHandle();
}
//
File& File::operator=(File&& file) noexcept
{
	// block until io is done
	while (ioPending() || file.ioPending())
		Sleep(3);

	if (m_FileHandle != nullptr) closeHandle();

	// copies that can't go wrong if the file is in the
	// middle fo IO
	m_FileInfo   = file.m_FileInfo;
	m_AsyncInput = file.m_AsyncInput;
	m_FileHandle = file.m_FileHandle;

	// copy/move members that have to happen after any IO is
	// complete
	m_Buffer	  = std::move(file.m_Buffer);
	m_ReadStatus  = file.m_ReadStatus;
	m_WriteStatus = file.m_WriteStatus;

	// get rid of old File's data, where appropriate
	file.m_FileHandle = nullptr;

	return *this;
}
//! \todo try and make noexcept, once we rework path a bit (esp.
//! operator+)
Path File::fullPath() const
{
	if (m_Owner != nullptr)
		return m_Owner->path() + m_FileInfo.m_FileName;
	else
		return Path(Directory::getWindowsWorkingDir())
			   + m_FileInfo.m_FileName;
}
bool File::isAsync() const noexcept
{
	return m_FileInfo.m_Async;
}
//! \todo Do we need to check for outstanding IO, before potentailly
//! closing and reopening file?
void File::reOpen(bool async, bool write)
{
	if (m_FileHandle != nullptr) closeHandle();

	m_FileInfo.m_Async = async;
	openFile(write);
}
//
//! \todo create a more sophisticated system around pending reads
//! /writes - this one might cause another thread to block forever, we
//! keep issuing reads, and the other one wants to write \todo We
//! should probably not consider non-overlapping reads / writes to be
//! competing?
void File::read()
{
	// make sure we're not doing something else
	// TODO: make IO Pending messages etc. smarter, so that
	// we only block this that need to be blocked
	if (writePending())
	{
		while (!writeComplete())
			Sleep(5);
	} else if (readPending())
		return;  // if there is a read pending, we don't
				 // need to read again...

	// make sure we have a handle...
	if (m_FileHandle == nullptr) openFile(false);

	runtime_assert(m_FileHandle);
	m_ReadStatus = Status::IO_PENDING;

	// get the file size, and mark the file as not finished
	// reading
	const natural64 filesize = getAttributes(fullPath()).m_FileSize;

	// create buffer of correct size, and get the ptr to the
	// buffer
	m_Buffer.resizeBuffer(filesize, false);

	// if we have decided to read asynchonously
	if (m_FileInfo.m_Async)
	{
		// the hEvent is to pass what object to work on,
		// because the completion routine cannot be a member
		// function
		m_AsyncInput.hEvent		= static_cast<void*>(this);
		m_AsyncInput.Offset		= 0;
		m_AsyncInput.OffsetHigh = 0;

		// avoid returning errors from previous calls (ie:
		// CreateFile()) this is important if we want to
		// catch errors after 'successful' reads
		SetLastError(ERROR_SUCCESS);

		if (ReadFileEx(m_FileHandle, m_Buffer.unsafeBufferRef(),
					   static_cast<DWORD>(m_Buffer.size()),
					   &m_AsyncInput, &File::readIOCompleter)
			== TRUE)
		{
			DWORD errafter = GetLastError();

			// we still need to deal with "success case"
			// errors
			switch (errafter)
			{
				case ERROR_SUCCESS:
					break;
				case ERROR_MORE_DATA:
					getErrorLog()->addError<OutsideBounds>(
						FILELOC_UTF8,
						u8"Buffer overflow in ReadFileEx!"_ustring);
					break;
				default:
					getErrorLog()->addError<Unknown>(
						FILELOC_UTF8,
						u8"an unexpected error occurred"_ustring);
					break;
			}
		} else
		{
			DWORD errafter = GetLastError();

			switch (errafter)
			{
				case ERROR_INVALID_USER_BUFFER:
				case ERROR_NOT_ENOUGH_MEMORY:
					throw getErrorLog()->createException<TooManyRequests>(
						FILELOC_UTF8,
						u8"Multiple outstanding IO requests. Cannot fulfill more!"_ustring);
				case ERROR_HANDLE_EOF:
					throw getErrorLog()
						->createException<OutsideBounds>(
							FILELOC_UTF8,
							u8"Attempted to read past EOF!"_ustring);
				default:
					throw getErrorLog()->createException<Unknown>(
						FILELOC_UTF8,
						u8"an unexpected error occurred"_ustring);
			}
		}
	} else
	{
		// this is required, for some reason
		DWORD bytesread;
		SetLastError(ERROR_SUCCESS);

		if (ReadFile(m_FileHandle, m_Buffer.unsafeBufferRef(),
					 static_cast<DWORD>(m_Buffer.size()), &bytesread,
					 nullptr)
			== TRUE)
		{
			m_ReadStatus = Status::IO_COMPLETE;
		} else
		{
			DWORD errafter = GetLastError();
			m_ReadStatus   = Status::IO_COMPLETE;

			switch (errafter)
			{
				case ERROR_INVALID_USER_BUFFER:
				case ERROR_NOT_ENOUGH_MEMORY:
				case ERROR_IO_PENDING:
					throw getErrorLog()->createException<TooManyRequests>(
						FILELOC_UTF8,
						u8"Multiple outstanding IO requests. Cannot fulfill more!"_ustring);
				default:
					throw getErrorLog()->createException<Unknown>(
						FILELOC_UTF8,
						u8"an unexpected error occurred"_ustring);
			}
		}

		closeHandle();
	}
}
//! \todo create a more sophisticated system around pending reads
//! /writes - this one might block forever, if another thread keeps
//! issuing reads, and this one wants to write. \todo We should
//! probably not consider non-overlapping reads / writes to be
//! competing?
void File::write()
{
	if (writePending())
		return;  // if there is already a write pending, we
				 // don't want to write again...
	else if (readPending())
	{
		// then wait until the read is done
		while (!readComplete())
			Sleep(5);
	}

	// make sure we have a handle...
	if (m_FileHandle == nullptr) openFile(true);

	runtime_assert(m_FileHandle);
	m_WriteStatus = Status::IO_PENDING;

	if (m_FileInfo.m_Async)
	{
		// the hEvent is to pass what object to work on,
		// because the completion routine cannot be a member
		// function
		m_AsyncInput.hEvent		= static_cast<void*>(this);
		m_AsyncInput.Offset		= 0;
		m_AsyncInput.OffsetHigh = 0;

		// avoid returning errors from previous calls (ie:
		// CreateFile()) this is important if we want to
		// catch errors after 'successful' writes
		SetLastError(ERROR_SUCCESS);

		if (WriteFileEx(m_FileHandle, m_Buffer.unsafeBufferRef(),
						static_cast<DWORD>(m_Buffer.size()),
						&m_AsyncInput, &File::writeIOCompleter)
			== TRUE)
		{
			DWORD errafter = GetLastError();

			switch (errafter)
			{
				case ERROR_SUCCESS:
					break;
				case ERROR_MORE_DATA:
					getErrorLog()->addError<OutsideBounds>(
						FILELOC_UTF8,
						u8"buffer overflow when writing with WriteFileEx!"_ustring);
					break;
				default:
					getErrorLog()->addError<Unknown>(
						FILELOC_UTF8,
						u8"an unexpected error occurred"_ustring);
					break;
			}
		} else
		{
			DWORD errafter = GetLastError();

			switch (errafter)
			{
				case ERROR_INVALID_USER_BUFFER:  // intentional
												 // fall-through
				case ERROR_NOT_ENOUGH_MEMORY:
					throw getErrorLog()->createException<TooManyRequests>(
						FILELOC_UTF8,
						u8"Multiple outstanding IO requests. Cannot fulfill more!"_ustring);
				default:
					throw getErrorLog()->createException<Unknown>(
						FILELOC_UTF8,
						u8"an unexpected error occurred"_ustring);
			}
		}
	} else
	{
		DWORD byteswritten;

		if (WriteFile(m_FileHandle, m_Buffer.unsafeBufferRef(),
					  static_cast<DWORD>(m_Buffer.size()),
					  &byteswritten, nullptr)
			== TRUE)
		{
			m_WriteStatus = Status::IO_COMPLETE;
		} else
		{
			DWORD errafter = GetLastError();
			m_WriteStatus  = Status::IO_COMPLETE;

			switch (errafter)
			{
				case ERROR_INVALID_USER_BUFFER:
				case ERROR_NOT_ENOUGH_MEMORY:
				case ERROR_IO_PENDING:
					throw getErrorLog()->createException<TooManyRequests>(
						FILELOC_UTF8,
						u8"Multiple outstanding IO requests. Cannot fulfill more!"_ustring);
				default:
					throw getErrorLog()->createException<Unknown>(
						FILELOC_UTF8,
						u8"an unexpected error occurred"_ustring);
			}
		}

		closeHandle();
	}
}
//
bool File::readPending() const noexcept
{
	checkReadStatus();

	return m_ReadStatus == Status::IO_PENDING;
}
bool File::writePending() const noexcept
{
	checkWriteStatus();

	return m_WriteStatus == Status::IO_PENDING;
}
bool File::ioPending() const noexcept
{
	return (readPending() || writePending());
}
//
bool File::readComplete() const noexcept
{
	checkReadStatus();

	return m_ReadStatus == Status::IO_COMPLETE;
}
bool File::writeComplete() const noexcept
{
	checkWriteStatus();

	return m_WriteStatus == Status::IO_COMPLETE;
}
//
//! \todo change to getDataCopy() function, and add a genuine
//! getData() function that returns a reference
FileBuffer File::getData() const
{
	return m_Buffer;
}
//! \todo add capability (in another function, probably) to
//! incrementally chnage the file buffer - no need to replace the
//! whole thing if we just need to replace one byte
void File::setData(FileBuffer data) noexcept
{
	m_Buffer = std::move(data);
}
//
void File::clearData() noexcept
{
	m_Buffer.clear();
}
//
void CALLBACK File::readIOCompleter(
	DWORD errorcode, DWORD, LPOVERLAPPED asyncinputptr) noexcept
{
	// check to see if the operation was cancelled
	if (errorcode == ERROR_OPERATION_ABORTED)
	{
		asyncinputptr->hEvent = nullptr;
		return;
	}

	// ReadFileEx doesn't touch the hEvent pointer, so it
	// had a pointer to the object of the file stored in it
	File* object = static_cast<File*>(asyncinputptr->hEvent);

	// check for detailed success (object-dependant)
	DWORD bytes;
	if (GetOverlappedResult(object->m_FileHandle, asyncinputptr,
							&bytes, FALSE)
		== FALSE)
	{
		getErrorLog()->addError<Unknown>(
			FILELOC_UTF8, u8"something strange happened"_ustring);
	}

	// we no longer need this pointer, so get rid of it (we must not
	// delete, as it's a this pointer, and we don't have ownership)
	asyncinputptr->hEvent = nullptr;

	// we're now finished with our operation
	object->m_ReadStatus = Status::IO_COMPLETE;
	object->closeHandle();

	getErrorLog()->addError<Success>(
		FILELOC_UTF8,
		u8"Finished asynchronously reading: "_ustring
			+ static_cast<Strings::widestring>(object->fullPath()),
		false, ErrorStatus::Warning, ErrorMatchingMode::Strict);
}
void CALLBACK File::writeIOCompleter(
	DWORD errorcode, DWORD, LPOVERLAPPED asyncinputptr) noexcept
{
	// check to see if the operation was cancelled
	if (errorcode == ERROR_OPERATION_ABORTED)
	{
		asyncinputptr->hEvent = nullptr;
		return;
	}

	// WriteFileEx doesn't touch the hEvent pointer, so it
	// had a pointer to the object of the file stored in it
	File* object = static_cast<File*>(asyncinputptr->hEvent);

	// check for detailed success (object-dependant)
	DWORD bytes;
	if (GetOverlappedResult(object->m_FileHandle, asyncinputptr,
							&bytes, FALSE)
		== FALSE)
	{
		getErrorLog()->addError<Unknown>(
			FILELOC_UTF8, u8"something strange happened"_ustring,
			false);
	}

	// we no longer need this pointer, so get rid of it (we must not
	// delete, as it's a this pointer, and we don't have ownership)
	asyncinputptr->hEvent = nullptr;

	// we're now finished with our operation
	object->m_WriteStatus = Status::IO_COMPLETE;
	object->closeHandle();

	getErrorLog()->addError<Success>(
		FILELOC_UTF8,
		u8"Finished asynchronously writing: "_ustring
			+ static_cast<Strings::widestring>(object->fullPath()));
}
//
FileAttributes File::getAttributes(Path filename)
{
	return getAttributes(static_cast<Strings::widestring>(filename));
}
FileAttributes File::getAttributes(Strings::widestring filename)
{
	// create object for data, and 0-initialise
	WIN32_FILE_ATTRIBUTE_DATA attributes{};

	if (GetFileAttributesExW(filename.data(), GetFileExInfoStandard,
							 &attributes)
		== FALSE)
	{
		DWORD err = GetLastError();

		if (err == ERROR_FILE_NOT_FOUND)
		{
			// return a file attributes object, that indicates that
			// the object did not exist
			return FileAttributes();
		} else
			throw getErrorLog()->createException<Unknown>(
				FILELOC_UTF8,
				u8"failed to get Attributes from file with name: "_ustring
					+ filename);
	}

	// get attributes that we care about
	const natural64 size =
		SetBits(static_cast<char32_t>(attributes.nFileSizeLow),
				static_cast<char32_t>(attributes.nFileSizeHigh),
				0xFFFFFFFF, 32);
	const bool isDirectory =
		(attributes.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;

	return FileAttributes{size, isDirectory};
}

//==============================================
//---------------Directory class----------------
//==============================================
// class for organising files inside, that are
// inside a directory in the file system. Can
// also contain sub-directories, and will provide
// access to them
//==============================================

Directory::Directory(Strings::widestring path, Directory* owner,
					 bool asyncbydef, bool writebydef)
	: Directory(Path(path), owner, asyncbydef, writebydef)
{}
Directory::Directory(Path path, Directory* owner, bool asyncbydef,
					 bool writebydef)
	: m_OwnedDirectories(),
	  m_OwnedFiles(),
	  m_Path(L"C:\\", PathSegmentType::ABSOLUTE_DRIVE),
	  m_Owner(owner),
	  m_ActiveSubDir(),
	  m_DefWrite(writebydef),
	  m_DefAsync(asyncbydef)
{
	if ((owner == nullptr) && (path.isRelative()))
	{
		Path abspath = getExePath();
		runtime_assert(abspath.listsize() != 0);
		runtime_assert(abspath.isAbsolute());

		if (abspath.backCopy().type() == PathSegmentType::FILE)
			abspath.claimBack();

		path.makeAbsolute(abspath);
	}

	runtime_assert(path.listsize() != 0);
	m_Path = path.claimTopLevel();

	if (path.isNotEmpty()
		&& (path.backCopy().type() != PathSegmentType::DIRECTORY))
		path.claimBack();

	// durng initialisation, above, we reduced the path to a
	// relative path of subdirectories
	if (path.isNotEmpty())
	{
		// check again, in case
		auto pathfrontcopy = path.frontCopy();
		auto pair		   = m_OwnedDirectories.emplace(
			 std::move(pathfrontcopy),
			 std::make_unique<Directory>(std::move(path), this,
										 m_DefAsync, m_DefWrite));
		m_ActiveSubDir = pair.first->second;  // make this dir the
											  // active directory
	}
}
//
bool Directory::asyncByDef() const noexcept
{
	return m_DefAsync;
}
bool Directory::writeByDef() const noexcept
{
	return m_DefWrite;
}
//
Path Directory::path() const
{
	if (isTopLevelDir())
		return m_Path;
	else
		return m_Owner->path() + m_Path;
}
bool Directory::isTopLevelDir() const noexcept
{
	return (m_Owner == nullptr);
}
//! \todo what do we do with non-active directores? does each
//! directory HAVE TO have an active sub-directory? Need to think more
//! about active directories and their consequences
Path Directory::activeDirPath() const
{
	auto activedir = m_ActiveSubDir.lock();
	return activedir ? activedir->activeDirPath() : path();
}
bool Directory::isActiveDir() const
{
	return !m_ActiveSubDir.expired();
}
//! \todo These functions don't seem to consider that users might pass
//! the path to an unrelated directory? Should maybe call parent, if
//! directory is not a sub-directory? We need to distinguish between
//! Incorrect sub-dirs and unrelated dirs in that case, though
std::weak_ptr<Directory> Directory::setActiveDir(
	Strings::widestring dirpath)
{
	return setActiveDir(Path(dirpath));
}
std::weak_ptr<Directory> Directory::setActiveDir(Path dirpath)
{
	if (dirpath.isAbsolute())
	{
		dirpath = dirpath - m_Path;

		if (dirpath.isNotEmpty()
			&& (dirpath.frontCopy().type()
				!= PathSegmentType::DIRECTORY))
			dirpath.claimFront();
	}

	if (dirpath.isEmpty())
	{
		auto activedir = m_ActiveSubDir.lock();
		if (activedir)  // to make sure that if we already
						// have a (lower down) active
						// directory, then the active
						// directory is correctly set, in
						// those subdirs
			activedir->setActiveDir(
				dirpath);  // we want to pass an empty path

		return m_ActiveSubDir =
				   std::weak_ptr<Directory>();  // invalidate
												// weak_ptr, without
												// destroying
												// shared_ptr
	} else
	{
		// we don't need any appended file
		if (dirpath.backCopy().type() == PathSegmentType::FILE)
			dirpath.claimBack();

		PathSegment seg = dirpath.claimFront();

		switch (seg.type())
		{
			case PathSegmentType::DIRECTORY:
			{
				auto loc = m_OwnedDirectories.find(seg);

				if (loc == m_OwnedDirectories.end())
				{
					// then the directory is not opened

					auto iterpair =
						m_OwnedDirectories.emplace(std::make_pair(
							seg,
							std::make_shared<Directory>(
								seg, this, m_DefAsync, m_DefWrite)));

					if (!iterpair.second)
						throw getErrorLog()
							->createException<CreationFailure>(
								FILELOC_UTF8,
								u8"failed to create directory"_ustring);

					loc = iterpair.first;
				}

				m_ActiveSubDir = loc->second;

				// if the returned ptr was invalid, then the
				// next dir down is the active dir
				auto temp = loc->second->setActiveDir(dirpath);
				if (temp.expired()) temp = m_ActiveSubDir;

				return temp;
			}
			case PathSegmentType::FILE:
			{
				// ignore the file, and call again
				return setActiveDir(dirpath);
			}
			default:
			{
				// is this even possible?
				throw getErrorLog()->createException<Unknown>(
					FILELOC_UTF8, u8"the path was invalid"_ustring);
			}
		}
	}
}
//
std::vector<PathSegment> Directory::getAllFileNames(
	bool useactivedir) const
{
	if (useactivedir)
	{
		auto activedir = m_ActiveSubDir.lock();

		if (activedir) return activedir->getAllFileNames(true);
	}

	// shouldn't we pass false, instead of useactivedir? It should be
	// always false, at this point in the function...
	std::vector<Path>		 tempres = getAllFilePaths(useactivedir);
	std::vector<PathSegment> result;
	const Path				 thispath = path();

	for (auto iter = tempres.begin(); iter != tempres.end(); ++iter)
	{
		Path relativepath = (*iter) - thispath;

		if (relativepath.listsize() == 1)
			result.push_back(
				relativepath.claimFront());  // get a path relative
											 // to this dir
	}

	return result;
}
//! \todo Do the actual search in getAllFileNames(), and then use the
//! results here. Could maybe also use a (non-threaded) coroutine, to
//! make it a bit more readable? ie: generate a name at a time, and
//! turn it into a path, and add to the array. As opposed to creating
//! the whole segment array and then using here. Further, we could
//! allow users to pass a function to call on names/ paths
//! per-element?
std::vector<Path> Directory::getAllFilePaths(bool useactivedir) const
{
	if (useactivedir)
	{
		auto activedir = m_ActiveSubDir.lock();

		if (activedir) return activedir->getAllFilePaths(true);
	}

	std::vector<Path>   result;
	Strings::widestring searchpath =
		static_cast<Strings::widestring>(path());
	searchpath << L"*"_upoint;  // add the wildcard to the end

	WIN32_FIND_DATAW filedata;
	HANDLE findhandle = FindFirstFileW(searchpath.data(), &filedata);

	if (findhandle == INVALID_HANDLE_VALUE) return result;

	do
	{
		Strings::widestring filepath =
			searchpath.subStr(0, searchpath.size() - 1)
			+ filedata.cFileName;

		auto fileAttr = File::getAttributes(filepath);

		if ((!fileAttr) || (fileAttr.m_Directory))
			continue;
		else
			result.push_back(filepath);
	} while (FindNextFileW(findhandle, &filedata) == TRUE);

	FindClose(findhandle);
	return result;
}
std::vector<PathSegment> Directory::getAllDirectoryNames(
	bool useactivedir) const
{
	if (useactivedir)
	{
		auto activedir = m_ActiveSubDir.lock();

		if (activedir) return activedir->getAllDirectoryNames(true);
	}

	std::vector<Path> tempres = getAllDirectoryPaths(useactivedir);
	std::vector<PathSegment> result;
	const Path				 thispath = path();

	for (auto iter = tempres.begin(); iter != tempres.end(); ++iter)
	{
		Path relativepath;

		if (iter->listsize() >= thispath.listsize())
			relativepath = (*iter) - thispath;

		if (relativepath.listsize() == 1)
			result.push_back(
				relativepath.claimFront());  // get a path relative
											 // to this dir
	}

	return result;
}
std::vector<Path> Directory::getAllDirectoryPaths(
	bool useactivedir) const
{
	if (useactivedir)
	{
		auto activedir = m_ActiveSubDir.lock();

		if (activedir) return activedir->getAllFilePaths(true);
	}

	std::vector<Path>   result;
	Strings::widestring searchpath =
		static_cast<Strings::widestring>(path());
	searchpath << L"*"_upoint;  // add the wildcard to the end

	WIN32_FIND_DATAW filedata;
	filedata.dwFileAttributes = FILE_ATTRIBUTE_DIRECTORY;
	HANDLE findhandle = FindFirstFileW(searchpath.data(), &filedata);

	if (findhandle == INVALID_HANDLE_VALUE) return result;

	do
	{
		// we should only get directories from FindNextFile,
		// etc. (because of the flag above)
		Strings::widestring filepath =
			searchpath.subStr(0, searchpath.size() - 1)
			+ filedata.cFileName + L"\\"_ustring;

		if ((filedata.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			!= 0)
			result.push_back(filepath);
	} while (FindNextFileW(findhandle, &filedata) == TRUE);

	FindClose(findhandle);
	return result;
}
//
std::vector<PathSegment> Directory::getOpenFileNames(
	bool useactivedir) const
{
	if (useactivedir)
	{
		auto activedir = m_ActiveSubDir.lock();

		if (activedir) return activedir->getOpenFileNames(true);
	}

	std::vector<PathSegment> result;

	for (const auto& pair : m_OwnedFiles)
		result.emplace_back(pair.first);

	return result;
}
std::vector<Path> Directory::getOpenFilePaths(
	bool useactivedir, bool includesubdirs) const
{
	if (useactivedir)
	{
		auto activedir = m_ActiveSubDir.lock();

		if (activedir)
			return activedir->getOpenFilePaths(true, includesubdirs);
	}

	std::vector<Path> result;

	// add the paths of each file to the result vector,
	// depending on whether we want
	for (const auto& pair : m_OwnedFiles)
		result.emplace_back(Path(pair.second->fullPath()));

	// if we want all files in subdirs, as well
	if (includesubdirs)
	{
		// get a list of files from all subdirectories
		for (const auto& pair : m_OwnedDirectories)
		{
			// get this directory's owned files
			std::vector<Path> subdirfiles =
				pair.second->getOpenFilePaths(useactivedir, true);

			// add the files to the end of the result vector
			result.insert(result.end(), subdirfiles.begin(),
						  subdirfiles.end());
		}
	}

	return result;
}
std::vector<PathSegment> Directory::getOpenDirectoryNames(
	bool useactivedir) const
{
	if (useactivedir)
	{
		auto activedir = m_ActiveSubDir.lock();

		if (activedir) return activedir->getOpenDirectoryNames(true);
	}

	std::vector<PathSegment> result;

	for (const auto& pair : m_OwnedDirectories)
		result.emplace_back(pair.first);

	return result;
}
std::vector<Path> Directory::getOpenDirectoryPaths(
	bool useactivedir, bool includesubdirs) const
{
	if (useactivedir)
	{
		auto activedir = m_ActiveSubDir.lock();

		if (activedir)
			return activedir->getOpenDirectoryPaths(true,
													includesubdirs);
	}

	std::vector<Path> result;

	// add the paths of each file to the result vector,
	// depending on whether we want
	for (const auto& pair : m_OwnedDirectories)
		result.emplace_back(Path(pair.second->path()));

	// if we want all files in subdirs, as well
	if (includesubdirs)
	{
		// get a list of files from all subdirectories
		for (const auto& pair : m_OwnedDirectories)
		{
			// get this directory's owned files
			std::vector<Path> subdirfiles =
				pair.second->getOpenDirectoryPaths(useactivedir,
												   true);

			// add the files to the end of the result vector
			result.insert(result.end(), subdirfiles.begin(),
						  subdirfiles.end());
		}
	}

	return result;
}
//! /todo these functions need to handle errors better (for example,
//! paths unrelated to this directory), and also need to be much
//! conceptually simpler. Currently these are overwritten and fairly
//! opaque
std::shared_ptr<Directory> Directory::getSubDirectory(
	Strings::widestring dirpath)
{
	return getSubDirectory(Path(dirpath));
}
std::shared_ptr<Directory> Directory::getSubDirectory(Path dirpath)
{
	// This is a REAL strange way of testing for path validity. What
	// happens if we have a path unrelated to this directory?
	if (dirpath == path())
	{
		if (!isTopLevelDir())
			return m_Owner->getSubDirectory(dirpath);
		else
			throw getErrorLog()->createException<InvalidFunctionCall>(
				FILELOC_UTF8,
				u8"attempted to get inaccessible directory"_ustring);
	}

	runtime_assert(dirpath.backCopy().type()
				   != PathSegmentType::FILE);

	if (dirpath.isRelative())
	{
		// we assume that the path is relative to this
		// directory
		PathSegment		seg  = dirpath.claimFront();
		PathSegmentType type = seg.type();

		if (type == PathSegmentType::DIRECTORY)
		{
			if (dirpath.isEmpty())
				return getSubDirectory(std::move(seg));
			else
			{
				// at least one more level of dir to go down

				auto dirloc = m_OwnedDirectories.find(seg);

				// if dir exists matching path
				if (dirloc != m_OwnedDirectories.end())
					return dirloc->second->getSubDirectory(
						std::move(dirpath));
				else
				{
					// open dir
					getSubDirectory(seg);

					// now that we've opened the directory,
					// get it
					dirloc = m_OwnedDirectories.find(seg);
					if (dirloc != m_OwnedDirectories.end())
					{
						return dirloc->second->getSubDirectory(
							std::move(dirpath));
					} else
					{
						// this should be impossible
						throw getErrorLog()->createException<Unknown>(
							FILELOC_UTF8,
							u8"something impossible happened"_ustring);
					}
				}
			}
		} else if (type == PathSegmentType::RELATIVE_PARENT)
		{
			if (dirpath.isEmpty())
				throw getErrorLog()
					->createException<InvalidUserInput>(
						FILELOC_UTF8,
						u8"the directory path was invalid"_ustring);

			if (m_Owner != nullptr)
			{
				// then we have an owner

				// pass remaining path to parent
				return m_Owner->getSubDirectory(std::move(dirpath));
			} else
			{
				// TODO: add abilty to create parent
				// directories if required, if we have none

				throw getErrorLog()
					->createException<InvalidUserInput>(
						FILELOC_UTF8,
						u8"the directory path was invalid"_ustring);
			}
		} else if ((type == PathSegmentType::RELATIVE_CURRENT)
				   || (type == PathSegmentType::RELATIVE_DRIVE))
		{
			if (dirpath.isEmpty())
				throw getErrorLog()
					->createException<InvalidUserInput>(
						FILELOC_UTF8,
						u8"the directory path was invalid"_ustring);

			// pass rest of path for checking again
			return getSubDirectory(std::move(dirpath));
		} else
		{
			// something has gone wrong
			throw getErrorLog()->createException<InvalidUserInput>(
				FILELOC_UTF8,
				u8"the directory path was invalid"_ustring);
		}
	} else
	{
		// we have to check what dir we are, in comparison
		// to the path

		// subtract our path from the path, to a path
		// relative to this dir
		Path differencepath = dirpath - path();

		// if it's the same, then the subtraction didn't
		// work
		if (differencepath == dirpath)
			throw getErrorLog()->createException<InvalidUserInput>(
				FILELOC_UTF8,
				u8"the directory path was invalid for the given directory"_ustring);

		// check with path relative to this one
		return getSubDirectory(std::move(differencepath));
	}
}
std::shared_ptr<Directory> Directory::getSubDirectory(
	PathSegment name)
{
	runtime_assert(name.type() == PathSegmentType::DIRECTORY);

	auto dirloc = m_OwnedDirectories.find(name);

	if (dirloc == m_OwnedDirectories.end())
	{
		auto iterpair = m_OwnedDirectories.emplace(std::make_pair(
			name, std::make_shared<Directory>(name, this, m_DefAsync,
											  m_DefWrite)));

		if (!iterpair.second)
			throw getErrorLog()->createException<CreationFailure>(
				FILELOC_UTF8,
				u8"failed to create wanted directory"_ustring);

		dirloc = iterpair.first;
	}

	return dirloc->second;
}
//
void Directory::closeSubDirectory(Strings::widestring dirpath)
{
	closeSubDirectory(Path(dirpath));
}
void Directory::closeSubDirectory(Path dirpath)
{
	runtime_assert(dirpath.backCopy().type()
				   == PathSegmentType::DIRECTORY);

	if (dirpath.isRelative())
	{
		PathSegment seg = dirpath.claimFront();  // get first element,
												 // and remove as well
		PathSegmentType type = seg.type();

		if (type == PathSegmentType::DIRECTORY)
		{
			if (dirpath.isEmpty())
			{
				closeSubDirectory(std::move(seg));
				return;
			} else
			{
				auto dirloc = m_OwnedDirectories.find(seg);

				if (dirloc != m_OwnedDirectories.end())
				{
					// we have got the diretory
					dirloc->second->closeSubDirectory(
						std::move(dirpath));
					return;
				} else
				{
					// we don't have the directory open, so
					// return false;
					getErrorLog()->addError<NotFound>(
						FILELOC_UTF8,
						u8"failed to close directory, because it is not open!"_ustring);
					return;
				}
			}
		} else if (type == PathSegmentType::RELATIVE_PARENT)
		{
			if (dirpath.isEmpty())
			{
				getErrorLog()->addError<InvalidUserInput>(
					FILELOC_UTF8,
					u8"the directory path was invalid"_ustring);
				return;
			} else
			{
				if (m_Owner != nullptr)
				{
					// then we have an owner

					// pass remaining path to parent
					m_Owner->closeSubDirectory(std::move(dirpath));
					return;
				} else
				{
					// TODO: add abilty to create parent
					// directories if required, if we have
					// none

					getErrorLog()->addError<InvalidUserInput>(
						FILELOC_UTF8,
						u8"the directory path was invalid"_ustring);
					return;
				}
			}
		} else if ((type == PathSegmentType::RELATIVE_CURRENT)
				   || (type == PathSegmentType::RELATIVE_DRIVE))
		{
			if (dirpath.isEmpty())
			{
				getErrorLog()->addError<InvalidUserInput>(
					FILELOC_UTF8,
					u8"the directory path was invalid"_ustring);
				return;
			}

			// pass rest of path for checking again
			closeSubDirectory(std::move(dirpath));
			return;
		}
	} else
	{
		// we have to check what dir we are, in comparison
		// to the path

		// subtract our path from the path, to a path
		// relative to this dir
		Path differencepath = dirpath - path();

		// if it's the same, then the subtraction didn't
		// work
		if (differencepath == dirpath)
		{
			getErrorLog()->addError<InvalidUserInput>(
				FILELOC_UTF8,
				u8"the directory path was invalid"_ustring);
			return;
		}

		// check with path relative to this one
		closeSubDirectory(std::move(differencepath));
		return;
	}
}
void Directory::closeSubDirectory(PathSegment name)
{
	runtime_assert(name.type() == PathSegmentType::DIRECTORY);

	// get the specified dir, throw if it doesn't exist
	auto iter = m_OwnedDirectories.find(name);
	if (iter == m_OwnedDirectories.end())
	{
		getErrorLog()->addError<NotFound>(
			FILELOC_UTF8,
			u8"failed to close directory, because it is not open!"_ustring);
		return;
	}

	// if the subdir is the active dir...
	auto subdir = m_ActiveSubDir.lock();
	if (subdir) subdir.reset();

	m_OwnedDirectories.erase(iter);
}
//
std::shared_ptr<File> Directory::getFile(Strings::widestring filepath,
										 bool startread)
{
	// parse the std::wstring into a format we recognise,
	// and pass it to the proper function
	return getFile(Path(filepath), startread);
}
std::shared_ptr<File> Directory::getFile(Path filepath,
										 bool startread)
{
	runtime_assert(filepath.backCopy().type()
				   == PathSegmentType::FILE);

	if (filepath.isRelative())
	{
		// we assume that the path is relative to this
		// directory
		PathSegment		seg  = filepath.claimFront();
		PathSegmentType type = seg.type();

		if (type == PathSegmentType::FILE)
		{
			// get file (in segment version of this
			// function)
			return getFile(std::move(seg), startread);
			;
		} else if (type == PathSegmentType::DIRECTORY)
		{
			if (filepath.isEmpty())
				throw getErrorLog()
					->createException<InvalidUserInput>(
						FILELOC_UTF8,
						u8"the file path was invalid"_ustring);

			auto dirloc = m_OwnedDirectories.find(seg);

			// if dir exists matching path
			if (dirloc != m_OwnedDirectories.end())
			{
				return dirloc->second->getFile(std::move(filepath),
											   startread);
			} else
			{
				// no dir exists matching name

				getSubDirectory(seg);

				// now that we've opened the directory, get
				// it
				dirloc = m_OwnedDirectories.find(seg);
				if (dirloc != m_OwnedDirectories.end())
				{
					return dirloc->second->getFile(
						std::move(filepath), startread);
				} else
				{
					// this should be impossible
					throw getErrorLog()->createException<Unknown>(
						FILELOC_UTF8,
						u8"something impossible happened"_ustring);
				}
			}
		} else if (type == PathSegmentType::RELATIVE_PARENT)
		{
			if (filepath.isEmpty())
				throw getErrorLog()
					->createException<InvalidUserInput>(
						FILELOC_UTF8,
						u8"the file path was invalid"_ustring);

			if (m_Owner != nullptr)
			{
				// pass remaining path to parent
				return m_Owner->getFile(std::move(filepath),
										startread);
			} else
			{
				// TODO: add abilty to create parent
				// directories if required, if we have none

				throw getErrorLog()
					->createException<InvalidUserInput>(
						FILELOC_UTF8,
						u8"the file path was invalid"_ustring);
			}
		} else if ((type == PathSegmentType::RELATIVE_CURRENT)
				   || (type == PathSegmentType::RELATIVE_DRIVE))
		{
			if (filepath.isEmpty())
				throw getErrorLog()
					->createException<InvalidUserInput>(
						FILELOC_UTF8,
						u8"the file path was invalid"_ustring);

			// pass rest of path for checking again
			return getFile(std::move(filepath), startread);
		} else
		{
			// something has gone wrong
			throw getErrorLog()->createException<InvalidUserInput>(
				FILELOC_UTF8, u8"the file path was invalid"_ustring);
		}
	} else
	{
		// we have to check what dir we are, in comparison
		// to the path

		// subtract our path from the path, to a path
		// relative to this dir
		Path differencepath = filepath - path();

		// if it's the same, then the subtraction didn't
		// work
		if (differencepath == filepath)
			throw getErrorLog()->createException<InvalidUserInput>(
				FILELOC_UTF8,
				u8"the file path was invalid for the given directory"_ustring);

		// check with path relative to this one
		return getFile(std::move(differencepath), startread);
	}
}
std::shared_ptr<File> Directory::getFile(PathSegment name,
										 bool		 startread)
{
	runtime_assert(name.type() == PathSegmentType::FILE);

	auto fileloc = m_OwnedFiles.find(name);

	if (fileloc == m_OwnedFiles.end())
	{
		// then the file didn't already exist
		auto iterpair = m_OwnedFiles.emplace(std::make_pair(
			name,
			std::make_unique<File>(
				IORequired{name, m_DefAsync, m_DefWrite}, this)));

		if (!iterpair.second)
			throw getErrorLog()->createException<CreationFailure>(
				FILELOC_UTF8,
				u8"failed to create wanted file"_ustring);

		fileloc = iterpair.first;
	}

	try
	{
		if (startread) fileloc->second->read();
	} catch (const catch_type<TooManyRequests>& thrown)
	{
		// then it was already reading (or something)!
		getErrorLog()->addError<TooManyRequests>(thrown.getWhere(),
												 thrown.getMessage());
	}

	// return the file
	return fileloc->second;
}
//
void Directory::closeFile(Strings::widestring filepath)
{
	closeFile(Path(filepath));
}
void Directory::closeFile(Path filepath)
{
	runtime_assert(filepath.backCopy().type()
				   == PathSegmentType::FILE);

	if (filepath.isRelative())
	{
		PathSegment		seg  = filepath.claimFront();
		PathSegmentType type = seg.type();

		if (type == PathSegmentType::FILE)
		{
			closeFile(std::move(seg));
			return;
		} else if (type == PathSegmentType::DIRECTORY)
		{
			if (filepath.isEmpty())
			{
				getErrorLog()->addError<InvalidUserInput>(
					FILELOC_UTF8,
					u8"the file path was invalid"_ustring);
				return;
			} else
			{
				// at least one more level of dir to go down

				auto dirloc = m_OwnedDirectories.find(seg);

				// if dir exists matching path
				if (dirloc != m_OwnedDirectories.end())
				{
					dirloc->second->closeFile(std::move(filepath));
					return;
				} else
				{
					// open dir
					getSubDirectory(seg);

					// now that we've opened the directory,
					// get it
					dirloc = m_OwnedDirectories.find(seg);
					if (dirloc != m_OwnedDirectories.end())
					{
						dirloc->second->closeFile(
							std::move(filepath));
						return;
					} else
					{
						// this should be impossible
						getErrorLog()->addError<Unknown>(
							FILELOC_UTF8,
							u8"something impossible happened"_ustring);
						return;
					}
				}
			}
		} else if (type == PathSegmentType::RELATIVE_PARENT)
		{
			if (filepath.isEmpty())
			{
				getErrorLog()->addError<InvalidUserInput>(
					FILELOC_UTF8,
					u8"the file path was invalid"_ustring);
				return;
			} else
			{
				if (m_Owner != nullptr)
				{
					// then we have an owner

					// pass remaining path to parent
					m_Owner->closeFile(std::move(filepath));
					return;
				} else
				{
					// TODO: add abilty to create parent
					// directories if required, if we have
					// none

					getErrorLog()->addError<InvalidUserInput>(
						FILELOC_UTF8,
						u8"the file path was invalid"_ustring);
					return;
				}
			}

		} else if ((type == PathSegmentType::RELATIVE_CURRENT)
				   || (type == PathSegmentType::RELATIVE_DRIVE))
		{
			if (filepath.isEmpty())
			{
				getErrorLog()->addError<InvalidUserInput>(
					FILELOC_UTF8,
					u8"the file path was invalid"_ustring);
				return;
			}

			closeFile(std::move(filepath));
			return;
		} else
		{
			// something has gone wrong
			getErrorLog()->addError<InvalidUserInput>(
				FILELOC_UTF8, u8"the file path was invalid"_ustring);
			return;
		}
	} else
	{
		// we have to check what dir we are, in comparison
		// to the path

		// subtract our path from the path, to a path
		// relative to this dir
		Path differencepath = filepath - path();

		// if it's the same, then the subtraction didn't
		// work
		if (differencepath == filepath)
		{
			getErrorLog()->addError<InvalidUserInput>(
				FILELOC_UTF8,
				u8"the file path was invalid for the given directory"_ustring);
			return;
		}

		// check with path relative to this one
		closeFile(std::move(differencepath));
		return;
	}
}
void Directory::closeFile(PathSegment name)
{
	runtime_assert(name.type() == PathSegmentType::FILE);

	// erase returns how many elements were destroyed
	if (m_OwnedFiles.erase(std::move(name)) == 0)
	{
		getErrorLog()->addError<NotFound>(
			FILELOC_UTF8, u8"failed to delete as requested"_ustring);
		return;
	}
}
//! \todo What is the point of this, if getFile() creates files if
//! they don't exist?
void Directory::createFile(Strings::widestring filepath,
						   FileBuffer contents, bool async,
						   bool replaceexisting)
{
	createFile(Path(filepath), std::move(contents), async,
			   replaceexisting);
}
//! \todo What is the point of this, if getFile() creates files if
//! they don't exist?
void Directory::createFile(Path filepath, FileBuffer contents,
						   bool async, bool replaceexisting)
{
	runtime_assert(filepath.backCopy().type()
				   == PathSegmentType::FILE);

	if (filepath.isRelative())
	{
		PathSegment		seg  = filepath.claimFront();
		PathSegmentType type = seg.type();

		if (type == PathSegmentType::FILE)
		{
			createFile(std::move(seg), std::move(contents), async,
					   replaceexisting);
			return;
		} else if (type == PathSegmentType::DIRECTORY)
		{
			if (filepath.isEmpty())
				throw getErrorLog()
					->createException<InvalidUserInput>(
						FILELOC_UTF8,
						u8"the directory path was invalid"_ustring);
			else
			{
				// at least one more level of dir to go down

				auto dirloc = m_OwnedDirectories.find(seg);

				// if dir exists matching path
				if (dirloc != m_OwnedDirectories.end())
				{
					dirloc->second->createFile(
						std::move(filepath), std::move(contents),
						async, replaceexisting);
					return;
				} else
				{
					// open dir
					getSubDirectory(seg);

					// now that we've opened the directory,
					// get it
					dirloc = m_OwnedDirectories.find(seg);
					if (dirloc != m_OwnedDirectories.end())
					{
						dirloc->second->createFile(
							std::move(filepath), std::move(contents),
							async, replaceexisting);
						return;
					} else
					{
						// this should be impossible
						throw getErrorLog()->createException<Unknown>(
							FILELOC_UTF8,
							u8"something impossible happened"_ustring);
					}
				}
			}
		} else if (type == PathSegmentType::RELATIVE_PARENT)
		{
			if (filepath.isEmpty())
				throw getErrorLog()
					->createException<InvalidUserInput>(
						FILELOC_UTF8,
						u8"the directory path was invalid"_ustring);
			else
			{
				if (m_Owner != nullptr)
				{
					// then we have an owner

					// pass remaining path to parent
					m_Owner->createFile(std::move(filepath),
										std::move(contents), async,
										replaceexisting);
					return;
				} else
				{
					// TODO: add abilty to create parent
					// directories if required, if we have
					// none

					throw getErrorLog()
						->createException<InvalidUserInput>(
							FILELOC_UTF8,
							u8"the directory path was invalid"_ustring);
				}
			}

		} else if ((type == PathSegmentType::RELATIVE_CURRENT)
				   || (type == PathSegmentType::RELATIVE_DRIVE))
		{
			if (filepath.isEmpty())
				throw getErrorLog()
					->createException<InvalidUserInput>(
						FILELOC_UTF8,
						u8"the directory path was invalid"_ustring);

			createFile(std::move(filepath), std::move(contents),
					   async, replaceexisting);
			return;
		} else
		{
			// something has gone wrong
			throw getErrorLog()->createException<InvalidUserInput>(
				FILELOC_UTF8, u8"the file path was invalid"_ustring);
		}
	} else
	{
		// we have to check what dir we are, in comparison
		// to the path

		// subtract our path from the path, to a path
		// relative to this dir
		Path differencepath = filepath - path();

		// if it's the same, then the subtraction didn't
		// work
		if (differencepath == filepath)
			throw getErrorLog()->createException<InvalidUserInput>(
				FILELOC_UTF8,
				u8"the file path was invalid for the given directory"_ustring);

		// check with path relative to this one
		createFile(std::move(differencepath), std::move(contents),
				   async, replaceexisting);
		return;
	}
}
//! \todo What is the point of this, if getFile() creates files if
//! they don't exist?
void Directory::createFile(PathSegment name, FileBuffer contents,
						   bool async, bool replaceexisting)
{
	// create file, insert in place in map
	auto result = m_OwnedFiles.emplace(std::make_pair(
		name,
		std::make_unique<File>(IORequired{name, async, true}, this)));

	// result.second will be false iff the element was not
	// inserted
	if ((!result.second) && (!replaceexisting))
		throw getErrorLog()->createException<CreationFailure>(
			FILELOC_UTF8,
			u8"the file desired already existed"_ustring);

	// set data and write it to file
	result.first->second->setData(std::move(contents));
	result.first->second->write();
}
//! \todo What is the point of this? Why can't we just have one 'open
//! / create file' function?
std::shared_ptr<File> Directory::reOpenFile(
	Strings::widestring filepath, bool async, bool replaceexisting)
{
	return reOpenFile(Path(filepath), async, replaceexisting);
}
std::shared_ptr<File> Directory::reOpenFile(Path filepath, bool async,
											bool replaceexisting)
{
	runtime_assert(filepath.backCopy().type()
				   == PathSegmentType::FILE);

	if (filepath.isRelative())
	{
		// we assume that the path is relative to this
		// directory
		PathSegment		seg  = filepath.claimFront();
		PathSegmentType type = seg.type();

		if (type == PathSegmentType::FILE)
		{
			// get file (in segment version of this
			// function)
			return reOpenFile(std::move(seg), async, replaceexisting);
		} else if (type == PathSegmentType::DIRECTORY)
		{
			if (filepath.isEmpty())
				throw getErrorLog()
					->createException<InvalidUserInput>(
						FILELOC_UTF8,
						u8"the file path was invalid"_ustring);

			auto dirloc = m_OwnedDirectories.find(seg);

			// if dir exists matching path
			if (dirloc != m_OwnedDirectories.end())
			{
				return dirloc->second->reOpenFile(
					std::move(filepath), async, replaceexisting);
			} else
			{
				// no dir exists matching name

				getSubDirectory(seg);

				// now that we've opened the directory, get
				// it
				dirloc = m_OwnedDirectories.find(seg);
				if (dirloc != m_OwnedDirectories.end())
				{
					return dirloc->second->reOpenFile(
						std::move(filepath), async, replaceexisting);
				} else
				{
					// this should be impossible
					throw getErrorLog()->createException<Unknown>(
						FILELOC_UTF8,
						u8"something impossible happened"_ustring);
				}
			}
		} else if (type == PathSegmentType::RELATIVE_PARENT)
		{
			if (filepath.isEmpty())
				throw getErrorLog()
					->createException<InvalidUserInput>(
						FILELOC_UTF8,
						u8"the file path was invalid"_ustring);

			if (m_Owner != nullptr)
			{
				// pass remaining path to parent
				return m_Owner->reOpenFile(std::move(filepath), async,
										   replaceexisting);
			} else
			{
				// TODO: add abilty to create parent
				// directories if required, if we have none

				throw getErrorLog()
					->createException<InvalidUserInput>(
						FILELOC_UTF8,
						u8"the file path was invalid"_ustring);
			}
		} else if ((type == PathSegmentType::RELATIVE_CURRENT)
				   || (type == PathSegmentType::RELATIVE_DRIVE))
		{
			if (filepath.isEmpty())
				throw getErrorLog()
					->createException<InvalidUserInput>(
						FILELOC_UTF8,
						u8"the file path was invalid"_ustring);

			// pass rest of path for checking again
			return reOpenFile(std::move(filepath), async,
							  replaceexisting);
		} else
		{
			// something has gone wrong
			throw getErrorLog()->createException<InvalidUserInput>(
				FILELOC_UTF8, u8"the file path was invalid"_ustring);
		}
	} else
	{
		// we have to check what dir we are, in comparison
		// to the path

		// subtract our path from the path, to a path
		// relative to this dir
		Path differencepath = filepath - path();

		// if it's the same, then the subtraction didn't
		// work
		if (differencepath == filepath)
			throw getErrorLog()->createException<InvalidUserInput>(
				FILELOC_UTF8,
				u8"the file path was invalid for the given directory"_ustring);

		// check with path relative to this one
		return reOpenFile(std::move(differencepath), async,
						  replaceexisting);
	}
}
std::shared_ptr<File> Directory::reOpenFile(PathSegment name,
											bool		async,
											bool replaceexisting)
{
	runtime_assert(name.type() == PathSegmentType::FILE);

	auto fileloc = m_OwnedFiles.find(name);

	if (fileloc != m_OwnedFiles.end())
	{
		fileloc->second->reOpen(async, replaceexisting);
	} else
	{
		// then the file didn't already exist
		auto iterpair = m_OwnedFiles.emplace(std::make_pair(
			name,
			std::make_unique<File>(
				IORequired{name, async, replaceexisting}, this)));

		if (!iterpair.second)
			throw getErrorLog()->createException<CreationFailure>(
				FILELOC_UTF8,
				u8"failed to create wanted file"_ustring);

		fileloc = iterpair.first;
	}

	return fileloc->second;
}
//
void Directory::deleteFile(Strings::widestring filepath)
{
	deleteFile(Path(filepath));
}
void Directory::deleteFile(Path filepath)
{
	runtime_assert(filepath.backCopy().type()
				   == PathSegmentType::FILE);

	if (filepath.isRelative())
	{
		PathSegment		seg  = filepath.claimFront();
		PathSegmentType type = seg.type();

		if (type == PathSegmentType::FILE)
		{
			deleteFile(std::move(seg));
			return;
		} else if (type == PathSegmentType::DIRECTORY)
		{
			if (filepath.isEmpty())
			{
				getErrorLog()->addError<InvalidUserInput>(
					FILELOC_UTF8,
					u8"the directory path was invalid"_ustring);
				return;
			} else
			{
				// at least one more level of dir to go down

				auto dirloc = m_OwnedDirectories.find(seg);

				// if dir exists matching path
				if (dirloc != m_OwnedDirectories.end())
				{
					dirloc->second->deleteFile(std::move(filepath));
					return;
				} else
				{
					getErrorLog()->addError<NotFound>(
						FILELOC_UTF8,
						u8"failed to delete file requested"_ustring);
					return;
				}
			}
		} else if (type == PathSegmentType::RELATIVE_PARENT)
		{
			if (filepath.isEmpty())
			{
				getErrorLog()->addError<InvalidUserInput>(
					FILELOC_UTF8,
					u8"the directory path was invalid"_ustring);
				return;
			} else
			{
				if (m_Owner != nullptr)
				{
					// then we have an owner

					// pass remaining path to parent
					m_Owner->deleteFile(std::move(filepath));
					return;
				} else
				{
					// TODO: add abilty to create parent
					// directories if required, if we have
					// none

					getErrorLog()->addError<InvalidUserInput>(
						FILELOC_UTF8,
						u8"the directory path was invalid"_ustring);
					return;
				}
			}

		} else if ((type == PathSegmentType::RELATIVE_CURRENT)
				   || (type == PathSegmentType::RELATIVE_DRIVE))
		{
			if (filepath.isEmpty())
			{
				getErrorLog()->addError<InvalidUserInput>(
					FILELOC_UTF8,
					u8"the directory path was invalid"_ustring);
				return;
			}

			deleteFile(std::move(filepath));
			return;
		} else
		{
			// something has gone wrong
			getErrorLog()->addError<InvalidUserInput>(
				FILELOC_UTF8, u8"the file path was invalid"_ustring);
			return;
		}
	} else
	{
		// we have to check what dir we are, in comparison
		// to the path

		// subtract our path from the path, to a path
		// relative to this dir
		Path differencepath = filepath - path();

		// if it's the same, then the subtraction didn't
		// work
		if (differencepath == filepath)
		{
			getErrorLog()->addError<InvalidUserInput>(
				FILELOC_UTF8,
				u8"the file path was invalid for the given directory"_ustring);
			return;
		}

		// check with path relative to this one
		deleteFile(std::move(differencepath));
		return;
	}
}
void Directory::deleteFile(PathSegment name)
{
	// first close the file
	closeFile(name);

	Path filepath = path();
	filepath.addBack(name);

	if (DeleteFileW(static_cast<Strings::widestring>(filepath).data())
		== FALSE)
		getErrorLog()->addError<DestructionFailure>(
			FILELOC_UTF8, u8"failed to delete file"_ustring);
}
//
//! \todo Check that this is safe, re: further use of this directory
//! in this program
void Directory::destroyThisDirectory()
{
	// directory must be empty
	if (!m_OwnedDirectories.empty())
	{
		getErrorLog()->addError<InvalidFunctionCall>(
			FILELOC_UTF8,
			u8"directory was not empty, so could not delete"_ustring);
		return;
	}

	if (!RemoveDirectoryW(
			static_cast<Strings::widestring>(path()).data()))
	{
		getErrorLog()->addError<DestructionFailure>(
			FILELOC_UTF8, u8"could not delete directory"_ustring);
		return;
	}
}
//
Strings::widestring Directory::getWindowsWorkingDir()
{
	// this gets the required size of buffer, to hold the
	// directory name
	natural32 buffersize = GetCurrentDirectoryW(0, nullptr);

	// create buffer for name, get name
	wchar_t* buffer;
	try
	{
		buffer = new wchar_t[buffersize];
	} catch (std::bad_alloc&)
	{
		throw getErrorLog()->createException<MemAlloc>(FILELOC_UTF8,
													   u8"");
	}
	natural32 readsize = GetCurrentDirectoryW(buffersize, buffer);

	// read size does not include trailing \0, but
	// buffersize does
	if (readsize + 1 != buffersize)
		throw getErrorLog()->createException<Unknown>(
			FILELOC_UTF8,
			u8"couldn't get windows working directory"_ustring);

	Strings::widestring result(buffer);
	result << L"\\"_upoint;  // returned path does not
							 // include trialing '\\'

	SafeArrayDelete(&buffer);

	return result;
}
Strings::widestring Directory::getExePath()
{
	return getModulePath(nullptr);  // will get the path of the file
									// (ie:exe) of the current process
}
Strings::widestring Directory::getModulePath(HINSTANCE Instance)
{
	Strings::widestring result;
	wchar_t*			pathbuffer = nullptr;
	natural				buffersize = PathSegment::MAX_SHORT_PATH;
	bool				found	  = false;

	try
	{
		do
		{
			pathbuffer = new wchar_t[buffersize];

			DWORD writesize =
				GetModuleFileNameW(Instance, pathbuffer, buffersize);

			if (writesize != buffersize)
			{
				DWORD err = GetLastError();
				switch (err)
				{
					case ERROR_SUCCESS:
						result = pathbuffer;  // copy path
											  // into reuslt
						found = true;

						break;
					case ERROR_INSUFFICIENT_BUFFER:

						if (buffersize == PathSegment::MAX_LONG_PATH)
						{
							// if MAX_LONG_PATH is too short (not sure
							// if that's even possible), then we have
							// to throw, since we don't support paths
							// longer than that
							throw getErrorLog()
								->createException<OutsideBounds>(
									FILELOC_UTF8,
									u8"required buffer was too long! Truncating path"_ustring);
						}


						buffersize *= 2;

						if (buffersize > PathSegment::MAX_LONG_PATH)
							buffersize = PathSegment::MAX_LONG_PATH;

						break;
					default:
						SafeArrayDelete(&pathbuffer);
						throw getErrorLog()->createException<Unknown>(
							FILELOC_UTF8,
							u8"windows error code: "_ustring
								+ Strings::utf8string(
									  (natural32) err));
				}
			} else
			{
				result = pathbuffer;  // copy path into reuslt
				found  = true;
			}

			SafeArrayDelete(&pathbuffer);
		} while (!found);
	} catch (std::bad_alloc&)
	{
		throw getErrorLog()->createException<MemAlloc>(
			FILELOC_UTF8,
			u8"could not create buffer for path"_ustring);
	}

	return result;
}
}  // namespace FileIO
}  // namespace frao