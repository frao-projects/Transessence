//! \file
//!
//! \brief Implementation of basic file classes (paths, filebuffers,
//! etc.)
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "Basic\Classes.hpp"
#include <Nucleus_inc\Error.hpp>

namespace frao
{
inline namespace FileIO
{
using namespace Strings::Operators;

//==============================================
//--------------PathSegment class---------------
//==============================================
// class for storing individual 'segements' of a
// path. If a path is 'c:\some\dir\names\file.tx'
// then the segments are the 'c:\', 'some\',
// 'dir\', 'names\', 'file.tx', for example
//==============================================

void PathSegment::validate()
{
	// these values are not meant to be used
	runtime_assert((m_Type != PathSegmentType::NONE)
				   && (m_Type != PathSegmentType::MAX_VALUE));

	// if segment is one of the standard types, put it in
	// that form
	switch (m_Type)
	{
		case PathSegmentType::LONG_PATH:
			m_Segment = L"\\\\\?\\"_ustring;
			return;
		case PathSegmentType::RELATIVE_CURRENT:
			m_Segment = L".\\"_ustring;
			return;
		case PathSegmentType::RELATIVE_PARENT:
			m_Segment = L"..\\"_ustring;
			return;
		case PathSegmentType::ABSOLUTE_DRIVE:
		{
			Strings::widestring	ending = L":\\"_ustring;
			Strings::widecodepoint letter;

			m_Segment >> letter;
			m_Segment.clear();

			m_Segment << letter << ending;
		}
			return;
		case PathSegmentType::RELATIVE_DRIVE:
		{
			Strings::widestring	ending = L":"_ustring;
			Strings::widecodepoint letter;

			m_Segment >> letter;
			m_Segment.clear();

			m_Segment << letter << ending;
		}
			return;
		default:
			break;
	}

	// if it's another type, then check for illegal
	// characters
	for (natural it = 0; it < m_Segment.size(); ++it)
	{
		// get char in position 'it'
		Strings::widecluster ch = m_Segment[it];

		if ((ch == L"<"_ucluster) || (ch == L">"_ucluster)
			|| (ch == L":"_ucluster) || (ch == L"\""_ucluster)
			|| (ch == L"|"_ucluster) || (ch == L"\?"_ucluster)
			|| (ch == L"*"_ucluster))
		{
			getErrorLog()->addError<InvalidUserInput>(
				FILELOC_UTF8,
				u8"The path had an illegal character!"_ustring);
			continue;
		} else if (ch == L"/"_ucluster)
		{
			m_Segment[it] = L"\\"_ucluster;
			continue;
		} else
		{
			// make sure we have no special (below 32 in
			// ascii) characters in the filename, because
			// these are unavailable
			for (natural64 index = 0; index < ch.size(); ++index)
			{
				if (ch[index].size() == 1)
				{
					if (ch[index][0] < 32)
					{
						getErrorLog()->addError<InvalidUserInput>(
							FILELOC_UTF8,
							u8"The path had an illegal character!"_ustring);
					}
				}
			}
		}
	}

	// if the resulting string is empty, then the segment
	// shouldn't exist
	if (m_Segment.size() == 0)
		throw getErrorLog()->createException<InvalidUserInput>(
			FILELOC_UTF8, u8"the path was empty"_ustring);
}
//
PathSegment::PathSegment(Strings::widestring segment,
						 PathSegmentType	 type)
	: m_Segment(segment), m_Type(type)
{
	// will throw if invalid
	validate();
}
//
Strings::widestring PathSegment::get(bool removelast) const
{
	if (m_Type != PathSegmentType::DIRECTORY)
		return m_Segment;
	else if (removelast && (m_Segment.size() != 0)
			 && (m_Segment[m_Segment.size() - 1] == L"\\"_ucluster))
		return m_Segment.subStr(0, m_Segment.size() - 1);
	else
		return m_Segment;
}
PathSegmentType PathSegment::type() const
{
	return m_Type;
}
Strings::widestring PathSegment::fileextension() const
{
	if (type() != PathSegmentType::FILE)
		throw getErrorLog()->createException<InvalidFunctionCall>(
			u8"PathSegment::fileextension()"_ustring,
			u8"attempted to get the extension of a non-file segment"_ustring);

	size_t loc = m_Segment.findNext(L".", 0);

	// there was no '.'
	if (loc == decltype(m_Segment)::npos) return Strings::widestring();

	return m_Segment.subStr(loc, m_Segment.size() - loc);
}
//
natural64 PathSegment::length() const
{
	return m_Segment.size();
}
//
bool PathSegment::operator==(const PathSegment& secondseg) const
{
	// check type
	if (this->type() != secondseg.type()) return false;

	// check that they are equivalent
	return (this->m_Segment.toCaseFolded())
		   == (secondseg.m_Segment.toCaseFolded());
}
bool PathSegment::operator!=(const PathSegment& secondseg) const
{
	return !(*this == secondseg);
}
//! \todo we should add other comparison operators (>, <=, >=), and a
//! "<=>" stand-in getOrder() function, to provide an absolute and
//! consistent order
bool PathSegment::operator<(const PathSegment& secondseg) const
{
	// do we need this test? it ever *actually* going to fire?
	if (*this == secondseg) return false;
	// it more likely that the types will be equal, so that
	// case is first
	else if (m_Type == secondseg.m_Type)
		return (this->m_Segment.toCaseFolded())
			   < (secondseg.m_Segment.toCaseFolded());
	else
		return m_Type < secondseg.m_Type;
}


//==============================================
//----------------Path class--------------------
//==============================================
// Path class used to store lots of path segments
// together, so as to form a full path
//==============================================
// Valid formats:
// <> designates something to replace with
// the relevant thing
// Absolute:
//\\?\<driveletter>:\<path>
//<driveletter>:\<path>
// Relative:
//.\<relativepath>
//..\<relativepath>	<---- Use this format with
//					care, is often inappropriate
//<relativepath>
//==============================================

void Path::parsePath(Strings::widestring path) noexcept
{
	if (path.size() == 0)
	{
		getErrorLog()->addError<InvalidFunctionCall>(
			FILELOC_UTF8,
			u8"Use of path implied non-empty path, but path was empty"_ustring);
		return;
	}

	// iterate through all forward slashes (/), and replace
	// them with backslashes (\)
	natural64 loc = path.findNext(L"/"_ustring, 0);
	while (loc != decltype(path)::npos)
	{
		// replace forward slash with backwards slash
		path[loc] = L"\\"_ucluster;

		// find next forward slash
		loc = path.findNext(L"/"_ustring, loc);
	}

	// create variable for the result
	std::list<PathSegment> dividedpath;

	// see if the prefix '\\?\' is in the string
	const auto longstr = L"\\\\\?\\"_ustring;
	natural64  start   = path.findNext(longstr, 0);

	// if '\\?\' is in the string
	if (start != decltype(path)::npos)
	{
		// we start with a string in the form
		// L"whatishereidontknow\\?\something\goes\here"

		// check that the '\\?\' is at the beginning,
		// because otherwise is invalid
		if (start != 0)
		{
			const size_t slashend = start + 4;
			path = path.subStr(slashend, path.size());
			getErrorLog()->addError<InvalidFunctionCall>(
				FILELOC_UTF8,
				u8"the long path designator was in the wrong place, so path before designator was removed"_ustring);
		}

		// check string isn't too long
		if (path.size() > PathSegment::MAX_LONG_PATH)
		{
			path = path.subStr(0, PathSegment::MAX_LONG_PATH);
			getErrorLog()->addError<InvalidFunctionCall>(
				FILELOC_UTF8,
				u8"the path was too long, so was truncated"_ustring);
		}

		// remove the '\\?\' from the string
		path.erase(0, longstr.size());
	}

	if (path.size() == 0)
	{
		getErrorLog()->addError<InvalidFunctionCall>(
			FILELOC_UTF8,
			u8"Use of path implied non-empty path, but path was functionally empty"_ustring);
		return;
	}

	// make sure second character exists, first
	if ((path.size() > 1) && (path[1] == L":"_ucluster))
	{
		if ((path.size() > 2) && (path[2] == L"\\"_ucluster))
		{
			// drive, appended with absolute path
			dividedpath.push_back(PathSegment(
				path.subStr(0, 3), PathSegmentType::ABSOLUTE_DRIVE));
			path.erase(0, 3);
		} else
		{
			// drive, appended with relative path
			dividedpath.push_back(PathSegment(
				path.subStr(0, 2), PathSegmentType::RELATIVE_DRIVE));
			path.erase(0, 2);
		}
	}

	// we now should have a roughly valid path form, (except
	// possible invalid characters)

	// get first slash location
	const auto slash	= L"\\"_ustring;
	natural64  slashloc = path.findNext(slash, 0);

	// iterate over all directory parts of the path, by
	// looking up to the next slash
	while (slashloc != decltype(path)::npos)
	{
		Strings::widestring nextparse = path.subStr(0, slashloc + 1);
		PathSegmentType		type	  = PathSegmentType::DIRECTORY;

		// check for '.\' and '..\'
		if (nextparse[0] == L"."_ucluster)
		{
			// we know there is at least another '\\' in the
			// string, so there must be another character -
			// no need to check
			Strings::widecluster second = nextparse[1];

			if ((second == L"."_ucluster) && (nextparse.size() == 3))
				type = PathSegmentType::RELATIVE_PARENT;
			else if (second == L"\\"_ucluster)
				type = PathSegmentType::RELATIVE_CURRENT;
		}

		// get up to and including the slash, and add it the
		// result
		dividedpath.push_back(
			PathSegment(std::move(nextparse), type));
		path.erase(0, slashloc + 1);

		slashloc = path.findNext(slash, 0);
	}

	// the last bit should be a file
	if (path.size() != 0)
	{
		dividedpath.push_back(
			PathSegment(path, PathSegmentType::FILE));
	}

	m_Path = std::move(dividedpath);

	rationalise();
	// check if we need a prepended '\\?\'
	correctLength();
}
void Path::rationalise()
{
	// make sure the first element isn't one of the types we
	// want to remove - this is to make sure we don't go past
	// the beginning of the list in the lower loop
	PathSegmentType fronttype = m_Path.front().type();
	while ((fronttype == PathSegmentType::RELATIVE_CURRENT)
		   || (fronttype == PathSegmentType::RELATIVE_PARENT))
	{
		m_Path.pop_front();
		fronttype = m_Path.front().type();
	}

	for (auto iter = m_Path.begin(); iter != m_Path.end(); ++iter)
	{
		if (iter->type() == PathSegmentType::RELATIVE_CURRENT)
		{
			// erase element, and make iter point to the
			// element before the deleted element
			iter = m_Path.erase(iter);
			--iter;  // loop will increment again, so make
					 // sure list in on elment before this
					 // one
		} else if (iter->type() == PathSegmentType::RELATIVE_PARENT)
		{
			auto parentiter = iter;
			++parentiter;  // one past the 'RELATIVE_PARENT'
						   // element

			// get and check the previous element (which
			// will always exist, becasue of the loop above
			// this one)
			--iter;
			if (iter->type() == PathSegmentType::DIRECTORY)
			{
				iter = m_Path.erase(
					iter,
					parentiter);  // erase both the
								  // 'RELATIVE_PARENT'
								  // element and directory
								  // previous to it
				--iter;
			} else
			{
				iter =
					m_Path.erase(++iter);  // go back to the
										   // 'RELATIVE_PARENT'
										   // element, and erase that.
										   // get the element before
										   // the erase, afterwards
				--iter;
			}
		}
	}
}
//
Path::Path() : m_Path()
{}
Path::Path(Strings::widestring fullpath) : m_Path()
{
	parsePath(fullpath);
}
Path::Path(PathSegment segment) : m_Path(1, segment)
{}
//! \todo validation / rationalisation of segment sequence?
Path::Path(std::list<PathSegment> fullpath)
	: m_Path(std::move(fullpath))
{}
//
std::list<PathSegment> Path::getListCopy() const
{
	return std::list<PathSegment>(m_Path);
}
PathSegment Path::frontCopy() const
{
	if (isEmpty())
		throw getErrorLog()->createException<NotFound>(
			FILELOC_UTF8,
			u8"attempted to get back path segment when there was no back"_ustring);

	return m_Path.front();
}
PathSegment Path::backCopy() const
{
	if (isEmpty())
		throw getErrorLog()->createException<NotFound>(
			FILELOC_UTF8,
			u8"attempted to get back path segment when there was no back"_ustring);

	return m_Path.back();
}
//
PathSegment Path::claimFront()
{
	if (isEmpty())
		throw getErrorLog()->createException<NotFound>(
			FILELOC_UTF8,
			u8"attempted to get front path segment when there was no back"_ustring);

	PathSegment result = m_Path.front();
	m_Path.pop_front();

	correctLength();

	return result;
}
PathSegment Path::claimBack()
{
	if (isEmpty())
		throw getErrorLog()->createException<NotFound>(
			FILELOC_UTF8,
			u8"attempted to get back path segment when there was no back"_ustring);

	PathSegment result = m_Path.back();
	m_Path.pop_back();

	correctLength();

	return result;
}
//! \todo add 'file' to the list of 'meaningful' segment types to
//! return?
PathSegment Path::claimTopLevel()
{
	PathSegmentType typegot;
	Path			resultholder;

	do
	{
		if (isEmpty())
			throw getErrorLog()->createException<NotFound>(
				FILELOC_UTF8,
				u8"no such top level of the path existed"_ustring);

		resultholder.makeEmpty();
		resultholder += claimFront();
		typegot = resultholder.m_Path.front().type();
	} while ((typegot != PathSegmentType::DIRECTORY)
			 && (typegot != PathSegmentType::ABSOLUTE_DRIVE));

	return resultholder.claimFront();
}
//! \todo Should we throw an exception when there's no such subpath?
//! We can have null paths (unlike segments), so doesn't it make more
//! sense to return a null path? If we should throw, should we be more
//! explicit with the exception thrown? Not to mention, what about
//! \verbatim ".\file.ext" paths? \endverbatim
Path Path::claimSubDirectoryList()
{
	PathSegmentType typegot;
	Path			result, oldpath;

	// loop until we have a first directory
	do
	{
		oldpath += claimFront();
		typegot = oldpath.m_Path.back().type();
	} while ((typegot != PathSegmentType::DIRECTORY)
			 && (typegot != PathSegmentType::ABSOLUTE_DRIVE));

	// get the rest of the path in result
	result.m_Path = std::move(m_Path);

	*this = std::move(oldpath);
	rationalise();
	correctLength();

	return result;
}
//
void Path::addFront(PathSegment segment)
{
	PathSegmentType type = segment.type();

	runtime_assert((type != PathSegmentType::NONE)
				   && (type != PathSegmentType::MAX_VALUE));

	if ((type == PathSegmentType::RELATIVE_CURRENT)
		|| (type == PathSegmentType::RELATIVE_PARENT)
		|| (type == PathSegmentType::LONG_PATH))
		return;

	m_Path.push_front(segment);
	correctLength();
}
void Path::addBack(PathSegment segment)
{
	PathSegmentType type = segment.type();

	runtime_assert((type != PathSegmentType::NONE)
				   && (type != PathSegmentType::MAX_VALUE));

	if ((type == PathSegmentType::RELATIVE_CURRENT)
		|| (type == PathSegmentType::LONG_PATH))
		return;
	else if (type == PathSegmentType::RELATIVE_PARENT)
	{
		if (m_Path.back().type() == PathSegmentType::DIRECTORY)
			m_Path.pop_back();

		return;
	}

	m_Path.push_back(segment);
	correctLength();
}
//
bool Path::isRelative() const
{
	// there are more possible first segments for realtive
	// paths, so just ask for the the negation of
	// isAbsolute()
	return !isAbsolute();
}
bool Path::isAbsolute() const
{
	// in case the list is empty, because then calling
	// front() will cause an exception
	if (isEmpty()) return false;

	// absolute paths must start with either a drive or a
	// long path specifier
	if (m_Path.front().type() == PathSegmentType::ABSOLUTE_DRIVE)
		return true;
	if (m_Path.front().type() == PathSegmentType::LONG_PATH)
		return true;

	return false;
}
//
bool Path::isNotEmpty() const
{
	return m_Path.size() != 0;
}
bool Path::isEmpty() const
{
	return m_Path.empty();
}
natural64 Path::listsize() const
{
	return m_Path.size();
}
//
void Path::makeEmpty()
{
	m_Path.clear();
}
//
void Path::correctLength()
{
	if (isEmpty()) return;

	// if we already have a length designator, remove it
	if (m_Path.front().type() == PathSegmentType::LONG_PATH)
		m_Path.pop_front();

	// this is the only first segment an absolute path that
	// isn't already long can have
	if (m_Path.front().type() == PathSegmentType::ABSOLUTE_DRIVE)
	{
		if (pathlength() > PathSegment::MAX_SHORT_PATH)
			m_Path.push_front(PathSegment(
				L"\\\\\?\\"_ustring, PathSegmentType::LONG_PATH));
	}
}
natural64 Path::pathlength() const
{
	natural64 result = 0;

	for (const auto& seg : m_Path)
	{
		result += seg.length();
	}

	return result;
}
//
//! \todo throw, rather than ignore, if both parameter and this
//! specify mismatching drives?
Path& Path::makeAbsolute(Path abspath)
{
	// if we're already an absolute path, no need to do
	// anything
	if (isAbsolute()) return *this;

	// if we're going to make this path absolute, we need an
	// absolute path to use!
	if (abspath.isRelative())
		throw getErrorLog()->createException<InvalidUserInput>(
			FILELOC_UTF8,
			u8"the argument was, incorrectly, relative"_ustring);

	// if the passed path ends in a file, get rid of that
	// segment
	if (abspath.m_Path.back().type() == PathSegmentType::FILE)
		abspath.m_Path.pop_back();

	// if the path is prepended with a relative path
	// specifier
	if (isNotEmpty())
	{
		PathSegmentType begintype = m_Path.front().type();
		if ((begintype == PathSegmentType::RELATIVE_CURRENT)
			|| (begintype == PathSegmentType::RELATIVE_PARENT)
			|| (begintype == PathSegmentType::RELATIVE_DRIVE))
			m_Path.pop_front();
	}

	// prepend this->m_Path with abspath
	while (abspath.m_Path.size())
	{
		m_Path.push_front(abspath.m_Path.back());
		abspath.m_Path.pop_back();
	}

	return *this;
}
Path& Path::removeFileSegment()
{
	if (isNotEmpty()
		&& (m_Path.back().type() == PathSegmentType::FILE))
		m_Path.pop_back();

	return *this;
}
//
Path Path::operator+(const PathSegment& segment) const
{
	// get the existing path
	std::list<PathSegment> list = getListCopy();

	// check if we're adding anything
	if (segment.length() == 0) return Path(std::move(list));

	PathSegmentType type = segment.type();

	if ((type == PathSegmentType::RELATIVE_CURRENT)
		|| (type == PathSegmentType::RELATIVE_PARENT))
		return Path(std::move(list));

	if (isEmpty()) return Path(segment);

	// files should only be at the end of the path, so if we
	// try to add another segment, then we're using the
	// operator wrongly
	runtime_assert(list.back().type() != PathSegmentType::FILE);

	list.push_back(segment);

	// if the first part of the path is an absolute drive,
	// then we know that it's absolute, and it isn't already
	// long, so check if it should be
	if (isAbsolute()
		&& (list.front().type() == PathSegmentType::ABSOLUTE_DRIVE))
	{
		// iterate over all the parts, and add the sizes
		// together
		natural64 size = 0;
		for (const auto& iter : list)
			size += iter.length();

		if (size > PathSegment::MAX_SHORT_PATH)
			list.push_front(PathSegment(L"\\\\\?\\"_ustring,
										PathSegmentType::LONG_PATH));
	}

	Path result = std::move(list);

	// see if the new path needs a length designator
	if (result.isAbsolute()) result.correctLength();

	return result;
}
Path Path::operator+(const Path& secondpath) const
{
	// check if the result is trivial
	if (this->isEmpty())
		return Path(secondpath);
	else if (secondpath.isEmpty())
		return Path(*this);

	// are we past the drive on the first path?
	Path result;

	if (secondpath.isRelative())
	{
		result = *this;

		// if the last segment of the first path is a file,
		// remove it
		if (result.backCopy().type() == PathSegmentType::FILE)
			result.claimBack();

		// add the relevant section of the second path to
		// the first
		for (const auto& seg : secondpath.m_Path)
		{
			PathSegmentType type = seg.type();

			if (type == PathSegmentType::RELATIVE_DRIVE)
			{
				// check the first argument, to see if we
				// already have a drive designator
				if ((result.isAbsolute())
					|| (result.frontCopy().type()
						== PathSegmentType::RELATIVE_DRIVE))
					continue;

				// then we don't have a drive designator, so
				// add one
				result.addFront(seg);

				continue;
			} else if (type == PathSegmentType::RELATIVE_CURRENT)
			{
				// we have nothing to do with this, so don't
				// add it
				continue;
			} else if (type == PathSegmentType::RELATIVE_PARENT)
			{
				// if we can go up a dir, do so
				if (result.backCopy().type()
					== PathSegmentType::DIRECTORY)
					result.claimBack();

				continue;
			} else
			{
				result.addBack(seg);
			}
		}
	} else
	{
		// the second argument is absolute

		if (isRelative())
		{
			// this is asking to add an absolute path to a
			// relative one. this makes no sense, so assume
			// the caller got the argument order wrong. Call
			// this function with the order reversed
			result = secondpath + *this;
		} else
		{
			// both are absolute. Add only the directory
			// components
			result = *this;

			// if the last segment of the first path is a
			// file, remove it
			if (result.backCopy().type() == PathSegmentType::FILE)
				PathSegment firstback = result.claimBack();

			// loop variables that can't (or shouldn't) be
			// intialised in the loop
			bool writeremaining = false;  // flag to write the rest of
										  // secondpath to result
			auto		existingiter = result.m_Path.begin();
			PathSegment existingseg  = *existingiter;

			for (auto iter = secondpath.m_Path.cbegin();
				 iter != secondpath.m_Path.cend(); ++iter)
			{
				PathSegment		seg  = *iter;
				PathSegmentType type = seg.type();

				// if the result hasn't already got to
				// end(), or if the paths have diverged
				if ((writeremaining) || (seg != *existingiter))
				{
					writeremaining = true;

					// write if data is type we need to
					// write
					if ((type == PathSegmentType::DIRECTORY)
						|| (type == PathSegmentType::FILE))
						result.addBack(std::move(seg));
					else if (type == PathSegmentType::RELATIVE_PARENT)
					{
						// if the last element is a dir, get
						// up a dir (get rid of last dir)
						if (result.backCopy().type()
							== PathSegmentType::DIRECTORY)
							result.claimBack();
					}
				}

				// need to do this explicitly, to avoid
				// overrun if result.size() >
				// secondpath.size()
				if (existingiter != result.m_Path.cend())
				{
					++existingiter;

					// check again, to see if it has now
					// overrun
					if (existingiter == result.m_Path.cend())
						writeremaining = true;
				}
			}
		}
	}

	return result;
}
//! \todo fixit fixit fixit fixit fixit fixit... Seriously, this
//! function is horrific. operator+ should call this one, to do the
//! work, for starters
Path& Path::operator+=(const PathSegment& segment)
{
	return (*this = *this + segment);
}
//! \todo fixit fixit fixit fixit fixit fixit... Seriously, this
//! function is horrific. operator+ should call this one, to do the
//! work, for starters
Path& Path::operator+=(const Path& secondpath)
{
	return (*this = *this + secondpath);
}
//
//! \todo add '-=' to implement actual functionality, and then make
//! '-' use? (in any case add '-=')
Path Path::operator-(const Path& secondpath) const
{
	if ((secondpath.isEmpty()) || (this->isEmpty())) return *this;

	Path result;

	// loop variables
	natural iteration = 0;

	// iterate over possible starting points for the match
	// checking. Will add segments that don't match
	for (auto iter = this->m_Path.cbegin();
		 iter != this->m_Path.cend(); ++iter, ++iteration)
	{
		if (*iter == secondpath.frontCopy())
		{
			// variables we need to check if the path we
			// even, later
			auto seconditer  = secondpath.m_Path.cbegin(),
				 reserveiter = iter;  // to restore to previous state,
									  // if match didn't happen

			// iterate over the secondpath and *this, until
			// they no longer match
			for (; (seconditer != secondpath.m_Path.cend())
				   && (iter != this->m_Path.cend())
				   && (*iter == *seconditer);
				 ++seconditer, ++iter)
			{
				continue;
			}

			// then the paths matched
			if (seconditer == secondpath.m_Path.cend())
			{
				//(make sure that we didn't already reach
				// the end of the loop, in the nested loop)
				if (iter == this->m_Path.cend()) break;

				// to counteract impending increment
				--iter;

				continue;
			} else
			{
				iter = reserveiter;
				result.addBack(
					*iter);  // add segment we started from,
							 // if it didn't match secondpath
			}
		} else
		{
			result.addBack(*iter);  // add segment that didn't match
		}
	}

	return result;
}
bool Path::operator==(const Path& secondpath) const
{
	// make sure the lists for each path are the same size
	if (this->listsize() != secondpath.listsize()) return false;

	// iterate over the lists, which we know are the same
	// length
	for (auto iterleft  = m_Path.begin(),
			  iterright = secondpath.m_Path.begin();
		 iterleft != m_Path.end(); ++iterleft, ++iterright)
	{
		// check the actual segments are the same
		if ((*iterleft) != (*iterright)) return false;
	}

	// all of the segments were the same
	return true;
}
bool Path::operator!=(const Path& secondpath) const
{
	return !(*this == secondpath);
}

//! \todo do 'dictionary' order. That is, alphabetical order, except
//! in the situaion that two paths that compare the same, until one
//! terminates (and the other doesn't). In that case, the shorter
//! comes first
//!
//! \todo do the other order operators (>, <=, >=), and the c++20 <=>
//! operator (in the form of an getOrder() function, since we don't
//! have c++20 yet)
bool Path::operator<(const Path& rhs) const
{
	// if one path is shorter (In terms of num of segments)
	// it is less
	if (listsize() != rhs.listsize())
		return listsize() < rhs.listsize();

	// iterate over the lists, which we know are the same
	// length
	for (auto iterleft  = m_Path.begin(),
			  iterright = rhs.m_Path.begin();
		 iterleft != m_Path.end(); ++iterleft, ++iterright)
	{
		// if the segments aren't the same, then use that
		// segment to determine the difference
		if ((*iterleft) != (*iterright))
			return ((*iterleft) < (*iterright));
	}

	// then all the segments are the same
	return false;
}
//
Path::operator Strings::widestring() const
{
	Strings::widestring fullstr;

	// glue the sections together
	for (auto iter = m_Path.cbegin(); iter != m_Path.cend(); ++iter)
	{
		fullstr += iter->get(false);
	}

	return fullstr;
}

//==============================================
//------------IORequired structure------------
//==============================================
// struct for telling a function what file is
// required, and on what terms
//==============================================

IORequired::IORequired(PathSegment filename, bool async,
					   bool writeaccess)
	: m_FileName(filename), m_Async(async), m_WriteAccess(writeaccess)
{}

//==============================================
//-----------FileAttributes structure-----------
//==============================================
// struct for telling a the attributes of a file
//==============================================

FileAttributes::FileAttributes()
	: m_FileSize(0), m_Directory(false), m_Found(false)
{}
FileAttributes::FileAttributes(natural64 fileSize, bool isDirectory)
	: m_FileSize(fileSize), m_Directory(isDirectory), m_Found(true)
{}
//
FileAttributes::operator bool() const noexcept
{
	return m_Found;
}

//===============================
//----------FileBuffer-----------
//===============================
// Class that holds a pointer to
// the file buffer. ie: a
// allocated array
//===============================

#pragma region FileBuffer::FileBuffer(natural64 size)
FileBuffer::FileBuffer(natural64 size) try : m_Buffer(nullptr),
											 m_Size(size)
{
	if (m_Size) m_Buffer = new natural8[m_Size];
} catch (std::bad_alloc&)
{
	throw getErrorLog()->createException<MemAlloc>(FILELOC_UTF8,
												   u8"");
}
#pragma endregion
FileBuffer::FileBuffer(std::initializer_list<natural8> buf)
	: FileBuffer(buf.size())
{
	natural64 i = 0;
	for (auto iter = buf.begin(); iter != buf.end(); ++iter, ++i)
		m_Buffer[i] = *iter;
}
//
FileBuffer::FileBuffer(const FileBuffer& buffer)
	: FileBuffer(buffer.size())
{
	// copy data from the rhs buffer to this one
	for (natural64 i = 0; i < m_Size; ++i)
		m_Buffer[i] = buffer[i];
}
FileBuffer::FileBuffer(FileBuffer&& buffer) noexcept
	: m_Buffer(nullptr), m_Size(buffer.size())
{
	m_Buffer = buffer.releaseBuffer();
}
//
FileBuffer::~FileBuffer() noexcept
{
	SafeArrayDelete(&m_Buffer);
}
//
FileBuffer& FileBuffer::operator=(const FileBuffer& buffer)
{
	// make sure we don't create a new buffer, if we don't
	// have to
	if (m_Size != buffer.size())
	{
		SafeArrayDelete(&m_Buffer);

		try
		{
			m_Size   = buffer.size();
			m_Buffer = new natural8[m_Size];
		} catch (std::bad_alloc&)
		{
			m_Size = 0;
			throw getErrorLog()->createException<MemAlloc>(
				FILELOC_UTF8, u8"");
		}
	}

	for (natural64 i = 0; i < m_Size; ++i)
		m_Buffer[i] = buffer[i];

	return *this;
}
FileBuffer& FileBuffer::operator=(FileBuffer&& temp) noexcept
{
	SafeArrayDelete(&m_Buffer);

	m_Size   = temp.size();
	m_Buffer = temp.releaseBuffer();

	return *this;
}
//
natural8* FileBuffer::releaseBuffer() noexcept
{
	// get old buffer
	natural8* temp = m_Buffer;

	// null our version
	m_Size   = 0;
	m_Buffer = nullptr;

	// return requested buffer
	return temp;
}
void FileBuffer::claimBuffer(natural8*& buffer,
							 natural64  size) noexcept
{
	SafeArrayDelete(&m_Buffer);

	m_Size   = size;
	m_Buffer = buffer;

	buffer = nullptr;
}
//
//! \todo rename to insertData, so function is more intuitive. Maybe
//! rename index param and put first, as well (since index is the only
//! consistent param across overloads)
void FileBuffer::changeData(const natural8* add, natural64 addlength,
							natural64 startpos)
{
	natural64 firstnewmem = m_Size;

	if ((startpos + addlength) > m_Size)
	{
		resizeBuffer(startpos + addlength, true);

		// will only enter if startpos > firstnewmem
		for (natural64 i = firstnewmem; i < startpos; ++i)
			m_Buffer[i] = 0;  // zero mem between start of write, and
							  // start of new memory
	}

	// add the new data
	for (natural64 liter = startpos, riter = 0; riter < addlength;
		 ++liter, ++riter)
		m_Buffer[liter] = add[riter];
}
//! \todo rename to insertData, so function is more intuitive. Maybe
//! rename index param and put first, as well (since index is the only
//! consistent param across overloads)
void FileBuffer::changeData(std::initializer_list<natural8> add,
							natural64						startpos)
{
	natural64 firstnewmem = m_Size;

	if ((startpos + add.size()) > m_Size)
	{
		resizeBuffer(startpos + add.size(), true);

		// will only enter if startpos > firstnewmem
		for (natural64 i = firstnewmem; i < startpos; ++i)
			m_Buffer[i] = 0;  // zero mem between start of write, and
							  // start of new memory
	}

	// add the new data
	natural64 liter = startpos;
	for (auto riter = add.begin(); riter != add.end();
		 ++liter, ++riter)
		m_Buffer[liter] = *riter;
}
void FileBuffer::appendData(const natural8* add, natural64 addlength)
{
	changeData(add, addlength, m_Size);
}
void FileBuffer::appendData(std::initializer_list<natural8> add)
{
	changeData(std::move(add), m_Size);
}
//
natural8* FileBuffer::unsafeBufferRef() noexcept
{
	return m_Buffer;
}
//
void FileBuffer::clear() noexcept
{
	m_Size = 0;
	SafeArrayDelete(&m_Buffer);
}
void FileBuffer::resizeBuffer(natural64 newsize, bool keepolddata)
{
	// if so, we don't need to change the buffer
	if (m_Size == newsize)
	{
		if ((!keepolddata) && (m_Size != 0))
		{
			// ie: if we don't want to keep old data, and the buffer
			// has non-zero size, zero it
			std::memset(m_Buffer, 0, m_Size);
		}

		return;
	}
	// return if newsize is 0. we only need this check here,
	// because we know m_Size and newsize cannot both be zero
	if (newsize == 0)
	{
		m_Size = 0;
		SafeArrayDelete(&m_Buffer);
		return;
	}

	// in case we want to copy the data over
	// (keepolddata==true)
	natural8* oldData = m_Buffer;
	natural64 oldSize = m_Size;
	try
	{
		m_Size   = newsize;
		m_Buffer = new natural8[m_Size];
	} catch (std::bad_alloc&)
	{
		throw getErrorLog()->createException<MemAlloc>(FILELOC_UTF8,
													   u8"");
	}

	if (keepolddata)
	{
		// if the old buffer was bigger than the new one,
		// limit the copy to the size of the new buffer
		if (oldSize > m_Size) oldSize = m_Size;

		for (natural i = 0; i < oldSize; ++i)
			m_Buffer[i] = oldData[i];
	}

	// delete the old stuff, if it existed
	if (oldSize != 0)
	{
		oldSize = 0;
		SafeArrayDelete(&oldData);
	}
}
FileBuffer FileBuffer::copy() const
{
	FileBuffer temp(size());

	for (natural64 iter = 0; iter < size(); ++iter)
		temp[iter] = m_Buffer[iter];

	return temp;
}
//
//! \todo add a 'used' size of the buffer, and functions to return it
natural64 FileBuffer::size() const noexcept
{
	return m_Size;
}
//
//! \todo change these to unchecked bounds, and add a checked get()
//! function
natural8& FileBuffer::operator[](natural64 index) noexcept
{
	if (index < m_Size)
		return m_Buffer[index];

	else
		return m_Buffer[m_Size - 1];
}
//! \todo change these to unchecked bounds, and add a checked get()
//! function
const natural8& FileBuffer::operator[](natural64 index) const noexcept
{
	if (index < m_Size)
		return m_Buffer[index];

	else
		return m_Buffer[m_Size - 1];
}
//
FileBuffer::operator bool() const
{
	return ((m_Buffer != nullptr) && (m_Size != 0));
}

}  // namespace FileIO
}  // namespace frao
