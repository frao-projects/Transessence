#ifndef FRAO_TRANESSENCE_MANAGER_CONTROL
#define FRAO_TRANESSENCE_MANAGER_CONTROL

//! \file
//!
//! \brief Header for retreiving global file manager
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "Organisation.hpp"

namespace frao
{
inline namespace FileIO
{
// for functions relating to global access to FileManager
// global object
namespace Manager
{
void createNewFileManager(bool readDefaultFile);  // will create only
												  // if none exists
FileManager& getFileManager();
}  // namespace Manager
}  // namespace FileIO
}  // namespace frao


#endif  //! FRAO_TRANESSENCE_MANAGER_CONTROL