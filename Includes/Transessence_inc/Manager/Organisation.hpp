#ifndef FRAO_TRANESSENCE_MANAGER_ORGANISATION
#define FRAO_TRANESSENCE_MANAGER_ORGANISATION

//! \file
//!
//! \brief Header for management of file IO
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "..\Basic\Classes.hpp"
#include "..\Basic\IOTypes.hpp"
#include "..\Basic\Parsing.hpp"

//===============================
//-----------Directory-----------
//===============================
// Class used to hold a set of
// pointers to files in a
// particular directory of
// interest
//===============================
namespace frao
{
inline namespace FileIO
{
//==============================================
//-------------FileManager class----------------
//==============================================
// class for controlling the file structure.
// contains directories, which themselves contain
// files, and handle all requests to read and
// write. The aim is to make sure all file IO can
// be done out of sight, with a minumum of input
//==============================================

class FileManager final
{
	std::unique_ptr<Directory>
		m_BaseDirectory;  // base directory, since only one
						  // is needed
	std::weak_ptr<Directory>
		m_ActiveDirectory;  // directory that will be used
							// as the default starting point,
							// for relative file paths.
	std::list<std::shared_ptr<File>>
		m_FilesToRead;  // list of files to be read. Has
						// copies of the shared_ptrs in the
						// Directory objects.
	std::list<std::shared_ptr<File>>
							   m_FilesToWrite;  // list of files to be written to
	std::map<Path, FileBuffer> m_ReadFiles;  // data from files that
											 // have already been read
	std::list<Path> m_WrittenFiles;  // files that have finished with
									 // their writes
	std::map<Strings::widestring, FileParse::makeptr>
		m_FileAssocs;  // file associations

	typedef std::list<std::shared_ptr<File>>::iterator
		iterator;  // iterator for read and write file lists

	// if the file is moved from m_FilesToRead to
	// m_ReadFiles the iterator will point to the element
	// after, on return
	bool checkReadFile(iterator& file);
	bool checkWrittenFile(iterator& file);

   public:
	// The (first) bool refers to whether or not the
	// filemanager should automatically read from the default
	// file on startup. If the default file is read, the
	// contents of the file will be applied. ie: files
	// contained in the default list will be opened and read
	// etc. The second is whether the read of the default
	// file (if appropriate) should be asynchronous (ie: if
	// bool is true) or synchronous (if false)
	FileManager(bool asyncio, bool usedefaultfile);
	FileManager(Strings::widestring usepath, bool asyncio,
				bool usedefaultfile);
	FileManager(Path usepath, bool asyncio, bool usedefaultfile);

	// get/set the directory from which the manager works
	Path getActiveDirectory() const;
	bool isActiveDirAsync() const;
	void setActiveDirectory(Strings::widestring dirpath);
	void setActiveDirectory(Path dirpath);

	void setFileAssoc(FileParse::makeptr make, bool overwrite);

	// read the default files, and get the list of file
	// locations, that should be read from file. either
	// from a specified or default location
	// default location is files.flemonconfig
	void readDefaultFile(bool implementcontents);
	void readDefaultFile(Path filepath, bool implmentcontents);

	// only required if you want to queue files to read
	// later
	void addFileToQueue(Path filepath, bool writeover);
	void addFileToQueue(Path filepath, bool writeover, bool async);

	// add files to the queue of files that need to be read
	void addFilesToQueue(std::vector<Strings::widestring> files,
						 bool writeover = false);
	void addFilesToQueue(std::vector<Strings::widestring> files,
						 bool writeover, bool async);

	void addFilesToQueue(std::vector<Path> files,
						 bool			   writeover = false);
	void addFilesToQueue(std::vector<Path> files, bool writeover,
						 bool async);

	// read files that have been scheduled for reading.
	// read, and place data in the filebuffer.
	void readAllQueued();
	void readAllQueued(std::vector<Path> extrafiles);

	// the ASYNC versions will ONLY READ THE FILES OPENED
	// FOR ASYNC OPERATIONS, and the SYNC versions will ONLY
	// READ THE FILES OPENED FOR SYNC OPERATIONS
	void readAsyncFiles();  // files will still be in the process
							// of being read, on return
	void readAsyncFiles(
		std::vector<Path>
			extrafiles);   // same as above, but first adds
						   // extra files to the list.
	void readSyncFiles();  // will only return when all
						   // files have been read
	void readSyncFiles(
		std::vector<Path>
			extrafiles);  // same as above, but first adds
						  // extra files to the list.

	void readSpecified(Path filepath);
	void readSpecified(std::vector<Path> files);

	void writeSpecified(Path filepath, FileBuffer data);
	void writeSpecified(std::map<Path, FileBuffer> datamap);

	// NOTE: UNSAFE IF CALLED ON DIFFERENT FILES IN ONE LOOP
	// - ONE FILE MAY COMPLETE WHILE THE OTHER IS WAITING,
	// LEADING TO PERMANENT STALL
	bool fileHasBeenRead(Strings::widestring filepath);
	bool fileHasBeenRead(Path filepath);

	bool fileHasBeenWrittenTo(Strings::widestring filepath);
	bool fileHasBeenWrittenTo(Path filepath);

	// Get the files in the active directory
	std::vector<Path> getActiveFiles() const;
	// Get the sub-directories in the active directory
	std::vector<Path> getActiveSubDirectories() const;

	// get a vector of (names of) files that have been read.
	// is bool is true, wait until all files have been read
	std::vector<Path> getReadFiles(bool blockuntilread = false);
	std::vector<Path> getWrittenFiles();

	// closes the buffer of a file, that has been read. (ie:
	// if the data is no longer required). the bool relates
	// to whether the file object should be closed entirely.
	// true indicates that file object will be closed.
	// false indicates that only filemanger version of data
	// buffer will be closed
	void closeReadFile(Strings::widestring filepath,
					   bool				   finishedwith);
	void closeReadFile(Path filepath, bool finishedwith);

	void closeWrittenFile(Strings::widestring filepath,
						  bool				  finishedwith);
	void closeWrittenFile(Path filepath, bool finishedwith);

	bool canParseFile(Strings::widestring filepath);
	bool canParseFile(Path filepath);

	// gets the content of the file, in the for, of the
	// buffer created by the file, on reading. will remove
	// the filebuffer from the filemanager
	FileBuffer getReadFileBuffer(Strings::widestring filepath);
	FileBuffer getReadFileBuffer(Path filepath);

	// get a polymorphic object holding the data of the file
	std::unique_ptr<FileParse> getReadFileData(
		Strings::widestring filepath);
	std::unique_ptr<FileParse> getReadFileData(Path filepath);
};
}  // namespace FileIO
}  // namespace frao


#endif  //! FRAO_TRANESSENCE_MANAGER_ORGANISATION