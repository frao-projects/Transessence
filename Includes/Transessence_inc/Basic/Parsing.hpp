#ifndef FRAO_TRANESSENCE_BASIC_PARSING
#define FRAO_TRANESSENCE_BASIC_PARSING

//! \file
//!
//! \brief Header for translation of raw file data into intelligible information
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <Nucleus_inc\Environment.hpp>
#include <Nucleus_inc\Strings.hpp>
#include <memory>
#include <type_traits>
#include <vector>
#include "..\Formats\Bitmap.hpp"
#include "Classes.hpp"

// because it interferes with sensible code
#undef max

namespace frao
{
inline namespace FileIO
{
//==============================================
//---------------FileParse class----------------
//==============================================
// Base class for functors that implement parsing
// of a given file type to parse from
//==============================================

//! \class FileParse
//!
//! \brief A base class for all file parsing functors to derive
//! themselves from
//!
//! \todo Add 'ProxyFileType' class that derives from this one, and
//! forwards secondary extensions to primary. Ie: provides alias file
//! extensions
class FileParse
{
	const Strings::widestring m_FileExtension;
	// add bool for parsed?

   public:
	//! \brief Initialise FileParse base class object, with the
	//! extension of the file type
	//!
	//! \param[in] filextension A string containing the extension of
	//! this file type
	FileParse(Strings::widestring fileextension);

	//! \brief virtual destructor, so that we can safelt derive from
	//! this class
	virtual ~FileParse() noexcept = default;

	//! \brief Get the registered extension of this file type
	//!
	//! \returns string with the extension
	Strings::widestring getExtension() const;

	FileParse& operator=(const FileParse& rhs);

	virtual void operator()(FileBuffer buffer) = 0;

	template<typename DerivedType>
	static std::unique_ptr<FileParse> make()
	{
		static_assert(
			std::is_convertible<std::unique_ptr<DerivedType>,
								std::unique_ptr<FileParse>>::value,
			"DerivedType must be convertable to "
			"FileParse!");

		return std::make_unique<DerivedType>();
	}

	// function ptr to hold make function, specialised for
	// the type needed
	typedef std::unique_ptr<FileParse> (*makeptr)();
};

//==============================================
//---------------txtParse class-----------------
//==============================================
// derived class for parsing a txt file from a
// FileBuffer
//==============================================

class txtParse final : public FileParse
{
	Strings::utf8string m_Data;

   public:
	txtParse();
	~txtParse() noexcept override = default;

	Strings::utf8string getData() const;

	void operator()(FileBuffer buffer) override;

	static Strings::widestring defExtension();
	static FileBuffer		   getBuffer(Strings::utf8string data);
};

//==============================================
//---------------csoParse class-----------------
//==============================================
// derived class for parsing a cso file (a cso
// file is a shader bytes file) from a FileBuffer
//==============================================

class csoParse final : public FileParse
{
	natural8* m_Data;
	natural64 m_Size;

   public:
	csoParse();
	~csoParse() noexcept override;

	const natural8* getBuffer() const;
	natural64		bufferSize() const;

	void operator()(FileBuffer buffer) override;

	static Strings::widestring defExtension();
};

//==============================================
//-------------fraocfgParse class-------------
//==============================================
// derived class for parsing a fraocfg file
// from a FileBuffer
//==============================================

class fraocfgParse final : public FileParse
{
	Path			  m_Directory;
	std::vector<Path> m_FilePaths;

	// all these parse functions for sections of the file
	// must be able to be bound to the above function pointer
	// type
	void parseDirectory(Strings::widestring buffer);
	void parseFiles(Strings::widestring buffer);

   public:
	fraocfgParse();
	~fraocfgParse() noexcept override = default;

	Path			  getDirectory() const;
	std::vector<Path> getFiles() const;

	void operator()(FileBuffer buffer) override;

	static Strings::widestring defExtension();
};


class bitmapParse final : public FileParse
{
	std::unique_ptr<FileFormats::Bitmap::Header>		 m_MainHeader;
	std::unique_ptr<FileFormats::Bitmap::InfoHeader>	 m_InfoHeader;
	std::unique_ptr<FileFormats::Bitmap::Palette>		 m_Palette;
	std::unique_ptr<FileFormats::Bitmap::BitmapFileData> m_BitmapData;

   public:
	bitmapParse();
	~bitmapParse() override = default;

	void operator()(FileBuffer buffer) override;

	auto getMainHeader() noexcept -> decltype(m_MainHeader)&;
	auto getMainHeader() const noexcept -> const
		decltype(m_MainHeader)&;
	auto getInfoHeader() noexcept -> decltype(m_InfoHeader)&;
	auto getInfoHeader() const noexcept -> const
		decltype(m_InfoHeader)&;
	auto getPalette() noexcept -> decltype(m_Palette)&;
	auto getPalette() const noexcept -> const decltype(m_Palette)&;
	auto getFileData() noexcept -> decltype(m_BitmapData)&;
	auto getFileData() const noexcept -> const
		decltype(m_BitmapData)&;

	static Strings::widestring defExtension();
};

}  // namespace FileIO

}  // namespace frao


#endif  //! FRAO_TRANESSENCE_BASIC_PARSING