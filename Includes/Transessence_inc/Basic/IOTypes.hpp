#ifndef FRAO_TRANESSENCE_BASIC_IOTYPES
#define FRAO_TRANESSENCE_BASIC_IOTYPES

//! \file
//!
//! \brief Header of File and directory abstractions
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

// so that we don't get collisions on the name max()
#ifndef NOMINMAX
#define NOMINMAX
#endif

#include <Windows.h>
#include <map>
#include <memory>
#include "Classes.hpp"

namespace frao
{
inline namespace FileIO
{
//==============================================
//------forward declarations and typedefs-------
//==============================================
class Directory;

//! \class File
//!
//! \brief class for interacting with a file in the file system of a
//! computer
//!
//! \todo add copy constructor and copy assignment operator
//!
//! \todo add support for multiple IO operations at a time. To this
//! end, change file to fileview, and have one per IO operation?
//!
//! \todo distinguish between one-time, and persistent use of a file,
//! and close file once no longer necessary (in order to free up
//! system resources, and because windows documentation implies that
//! closing a handle might be necessary in some instances, before
//! changes will be written to disk)
//!
//! \todo disallow relative paths, as they are not safe for
//! "multithreaded applicatiions and shared libraries", since the
//! GetCurrentDirectory() functions, and the global variable that is
//! stored per-process (that windows uses for current directory) are
//! not thread-safe
class File final
{
   public:
	//! \enum Status
	//!
	//! \brief Used for description of IO operation progress
	enum class Status : natural
	{
		NONE = 0U,   //!< \brief No IO operation
		IO_PENDING,  //! \brief An IO operation has been scheduled,
					 //! and is pending completion
		IO_COMPLETE  //!< \brief An IO operation was scheduled, and
					 //!< has been completed
	};

   private:
	IORequired m_FileInfo;
	FileBuffer m_Buffer;
	OVERLAPPED m_AsyncInput;
	Directory* m_Owner;
	HANDLE	 m_FileHandle;
	Status	 m_ReadStatus;  // for async read operations on
							  // this file
	Status m_WriteStatus;	 // for async write operations on
							  // this file

	// disable copying, because if we copy a file on disk,
	// we don't exactly want to copy this object
	File(const File&) = delete;
	File& operator=(const File&) = delete;

	//! \brief get a handle to a file, and inform the OS of the type
	//! of access we want. Preparation for actual reading / writing
	//!
	//! \param[in] write Should the file be opened for writing to, or
	//! just for reading? true indicates writing is (or may be)
	//! required. false that only reading is
	//!
	//! \exception AccessViolation iff the OS denies access to the
	//! file
	//!
	//! \exception NotFound iff the specified file cannot be found
	//!
	//! \exception Unknown iff anything else goes wrong
	void openFile(bool write);

	//! \param[in] safely get rid of the file handle that we earlier
	//! received. Will attempt to cancel any ongoing IO on that file
	//! (and if cancellation fails, will stall)
	//!
	//! \note Will not throw an exception under normal operation.
	//! However, if run under a debugger, then if the windows function
	//! CloseHandle throws (it will throw a SEH exception, NOT a c++
	//! one), then this function will throw. This function is
	//! nonetheless declared noexcept, for two reasons. 1) I don't
	//! know how SEH exceptions and noexcept interact, and online
	//! documentation on the issue is opaque enough that it isn't
	//! worth figuring out 2) if CloseHandle throws, then CloseHandle
	//! has been used incorrectly, and is being debugged. And hence
	//! termination is a good option, to enforce dealing with the
	//! problem (assuming SEH exceptions in noexcept functions cause
	//! terminate to be called, as with c++ exceptions)
	void closeHandle() noexcept;

	//! \brief if there is any pending read IO, will pass control to
	//! the OS, to allow it to inform us of IO completion. If no read
	//! IO is pending, no action will be performed
	void checkReadStatus() const noexcept;

	//! \brief if there is any pending write IO, will pass control to
	//! the OS, to allow it to inform us of IO completion. If no write
	//! IO is pending, no action will be performed
	void checkWriteStatus() const noexcept;

   public:
	//! \brief Construct a file object from a path to the file, a
	//! directory object to reside in, and whether or not async and
	//! writing are required
	//!
	//! \param[in] path Path of the file in question. Must be a file,
	//! not a directory
	//!
	//! \param[in] owner The Directory object that will own this path
	//!
	//! \param[in] async Should the file be read / written to
	//! asynchronously, or synchonously? true for async, as the name
	//! implies
	//!
	//! \param[in] write Should be file be opened for writing, or just
	//! for reading? true foir writing, as the name implies
	//!
	//! \exception InvalidFunctionCall iff file is actually a
	//! directory
	//!
	//! \exception NotFound iff no such file (or directory) exists by
	//! that name (and we can't, therefore, get its attributes)
	//!
	//! \exception Unknown iff getting attributes from file fails for
	//! some other reason
	File(PathSegment path, Directory* owner, bool async, bool write);

	//! \brief Construct a file object from the data for the required
	//! operation, and a directory object to reside in
	//!
	//! \param[in] filedata Data of the required file operation (ie:
	//! path, write permissions etc.)
	//!
	//! \param[in] owner The Directory object that will own this path
	//!
	//! \exception InvalidFunctionCall iff file is actually a
	//! directory
	//!
	//! \exception NotFound iff no such file (or directory) exists by
	//! that name (and we can't, therefore, get its attributes)
	//!
	//! \exception Unknown iff getting attributes from file fails for
	//! some other reason
	File(IORequired filedata, Directory* owner);

	//! \brief move constructor; will block until any pending IO is
	//! complete. Will leave moved from file in a potentially invalid
	//! state
	//!
	//! \param[in] file Rvalue file that should be moved from.
	//! Ideally, will not have any outstanding IO, since that will
	//! cause the move constructor to block
	File(File&& file) noexcept;

	//! \brief Destructor. Will safely close file handle
	~File() noexcept;

	//! \brief move assignment operator; will block until any pending
	//! IO is complete ( **for this OR file parameter** ). Will leave
	//! moved from file in a potentially invalid state
	//!
	//! \returns reference to this, for chaining
	//!
	//! \param[in] file Rvalue file that should be moved from.
	//! Ideally, will not have any outstanding IO, since that will
	//! cause the move constructor to block
	File& operator=(File&& file) noexcept;

	//! \brief will get the full path of this file. If this has an
	//! owner, that will be used to determine the rest of the path.
	//! Otherwise, we will use the (OS) working directory
	//!
	//! \returns 'Full' path, insofar as we have the information to
	//! determine it
	Path fullPath() const;

	//! \brief will get whether this file has been specified as
	//! asynchronous IO or not
	//!
	//! \returns boolean with value true iff file IO has been
	//! specifies as asynchronous
	bool isAsync() const noexcept;

	//! \brief if the file currently has incorrect permissions (async,
	//! write), open the file with different ones
	//!
	//! \param[in] async Should the file be opened for asynchronous
	//! IO?
	//!
	//! \param[in] write Should the file be opened for writing?
	//!
	//! \exception AccessViolation iff the OS denies access to the
	//! file
	//!
	//! \exception NotFound iff the specified file cannot be found
	//!
	//! \exception Unknown iff anything else goes wrong
	void reOpen(bool async, bool write);

	//! \brief Will read this file (the file currently referred to, by
	//! this) from disk into this' file buffer. **Will block, if a
	//! write is pending, to this file**
	//!
	//! \exception AccessViolation iff the OS denies access to the
	//! file, when attempting to open it
	//!
	//! \exception NotFound iff the specified file cannot be found, at
	//! one of various stages of the read (opening file, getting
	//! attributes).
	//!
	//! \exception TooManyRequests iff There are multiple outstanding
	//! OS IO requests, and we should wait for (some of) them to be
	//! fulfilled, before issuing more
	//!
	//! \exception OutsideBounds if we attempt to read past EOF in the
	//! file
	//!
	//! \exception MemAlloc iff we run out of memory for the file
	//! buffer
	//!
	//! \exception Unknown iff anything else goes wrong, at one of
	//! various stages of the read (opening file, getting attributes,
	//! actual reading).
	void read();

	//! \brief will write to this file, on disk. **Will result in
	//! no-op if there is an outstanding write on this file. Will
	//! block until any reads are complete**
	//!
	//! \exception AccessViolation iff the OS denies access to the
	//! file, when attempting to open it
	//!
	//! \exception NotFound iff the specified file cannot be found, at
	//! one of the stages of the read (opening file).
	//!
	//! \exception TooManyRequests iff There are multiple outstanding
	//! OS IO requests, and we should wait for (some of) them to be
	//! fulfilled, before issuing more
	//!
	//! \exception OutsideBounds iff there is a buffer overflow, when
	//! writing to the file on disk
	//!
	//! \exception Unknown iff anything else goes wrong, at one of the
	//! stages of the read (opening file, writing).
	void write();

	//! \brief will determine whether there are any pending read
	//! operations
	//!
	//! \returns boolean with value true iff there is a pending read
	bool readPending() const noexcept;

	//! \brief will determine whether there are any pending write
	//! operations
	//!
	//! \returns boolean with value true iff there is a pending write
	bool writePending() const noexcept;

	//! \brief will determine whether there are any pending read /
	//! write operations
	//!
	//! \returns boolean with value true iff there is a pending read /
	//! write
	bool ioPending() const noexcept;

	//! \brief will determine whether there have been any completed
	//! read operations
	//!
	//! \returns boolean with value iff there was a completed read
	bool readComplete() const noexcept;

	//! \brief will determine whether there have been any completed
	//! write operations
	//!
	//! \returns boolean with value iff there was a completed write
	bool writeComplete() const noexcept;

	//! \brief returns a copy of the FileBuffer of this object
	//!
	//! \returns A copy of the FileBuffer
	//!
	//! \exception MemAlloc iff cannot allocate memory for copy
	FileBuffer getData() const;

	//! \brief replaces existing filebuffer
	//!
	//! \param[in] data New file buffer, to replace the existing one
	void setData(FileBuffer data) noexcept;

	//! \brief make the file buffer of this object empty
	void clearData() noexcept;

	//! \brief Callback for when a file read has completed - to be
	//! passed to the OS, to call. Not to be called manually
	//!
	//! \param[in] errorcode What condition the read operation
	//! finished in
	//!
	//! \param[in] UNUSED_DWORD An unused parameter
	//!
	//! \param[in] asyncinputptr Data about the operation that was
	//! performed. Often used to hold a ptr to a relevent object. We
	//! use this to hold the ptr to the file object, since this
	//! callback is (necessarily) static
	static void CALLBACK readIOCompleter(
		DWORD errorcode, DWORD, LPOVERLAPPED asyncinputptr) noexcept;

	//! \brief Callback for when a file write has completed - to be
	//! passed to the OS, to call. Not to be called manually
	//!
	//! \param[in] errorcode What condition the write operation
	//! finished in
	//!
	//! \param[in] UNUSED_DWORD An unused parameter
	//!
	//! \param[in] asyncinputptr Data about the operation that was
	//! performed. Often used to hold a ptr to a relevent object. We
	//! use this to hold the ptr to the file object, since this
	//! callback is (necessarily) static
	static void CALLBACK writeIOCompleter(
		DWORD errorcode, DWORD, LPOVERLAPPED asynciputptr) noexcept;

	//! \brief Get Basic attributes for a filesystem object. That is,
	//! a file or directory.
	//!
	//! \returns Attributes of the file system object
	//!
	//! \param[in] filename Path containing name of the file system
	//! object that we want the attributes of
	//!
	//! \exception NotFound iff no such file / file system object with
	//! that name
	//!
	//! \exception Unknown iff we fail to get attributes, for some
	//! unforseen or strange occurrence
	static FileAttributes getAttributes(Path filename);

	//! \brief Get Basic attributes for a filesystem object. That is,
	//! a file or directory.
	//!
	//! \returns Attributes of the file system object
	//!
	//! \param[in] filename String containing name of the file system
	//! object that we want the attributes of
	//!
	//! \exception NotFound iff no such file / file system object with
	//! that name
	//!
	//! \exception Unknown iff we fail to get attributes, for some
	//! unforseen or strange occurrence
	static FileAttributes getAttributes(Strings::widestring filename);
};

//! \class Directory
//!
//! \brief class for interacting with a directory in the file system
//! of a computer, as well as the contents thereof
//!
//! \todo disallow relative paths, as they are not safe for
//! "multithreaded applicatiions and shared libraries", since the
//! GetCurrentDirectory() functions, and the global variable that is
//! stored per-process (that windows uses for current directory) are
//! not thread-safe
class Directory final
{
	std::map<PathSegment, std::shared_ptr<Directory>>
		m_OwnedDirectories;  // ptr will be nullptr, if dir
							 // isn't open
	std::map<PathSegment, std::shared_ptr<File>>
		m_OwnedFiles;	// ptr will be nullptr, if file isn't
						 // open
	PathSegment m_Path;  // the path of this directory. If m_Owner
						 // is nullptr, this must be an absolute
						 // path. If m_Owner is non-nullptr, then it
						 // must be relative
	Directory* m_Owner;  // pointer to Directory that contains this
						 // one. nullptr if there isn't an owner
	std::weak_ptr<Directory>
		m_ActiveSubDir;  // pointer to directory currently
						 // active, inside this one
	bool m_DefWrite;	 // open files by write access, by
						 // default
	bool m_DefAsync;	 // open files by async access, by
					  // default

   public:
	//! \brief Create a directory object, that can be used to interact
	//! with the directory in question, in the filesystem
	//!
	//! \param[in] path An absolute or relative path to the Directory
	//! that we wish to interact with
	//!
	//! \param[in] owner If 'path' parameter is an absolute path,
	//! should be null. If 'path' parameter is a relative path, then
	//! this should refer to the directory that the relative path is
	//! relative to
	//!
	//! \param[in] asyncbydef Should files that are opened inside this
	//! directory, be opened for asynchronous IO (by default)? true
	//! for yes
	//!
	//! \param[in] writebydef Should files that are opened inside this
	//! directory, be opened for writing (by default)? true for yes
	//!
	//! \exception OutsideBounds iff the path is above 32,767
	//! characters (UTF-16 codepoints)
	//!
	//! \exception MemAlloc iff not enough space for path buffer
	//!
	//! \exception Unknown iff we get a failure that we did not
	//! anticipate
	Directory(Strings::widestring path, Directory* owner,
			  bool asyncbydef, bool writebydef);

	//! \brief Create a directory object, that can be used to interact
	//! with the directory in question, in the filesystem
	//!
	//! \param[in] path A string containing an absolute or relative
	//! path to the Directory that we wish to interact with
	//!
	//! \param[in] owner If 'path' parameter is an absolute path,
	//! should be null. If 'path' parameter is a relative path, then
	//! this should refer to the directory that the relative path is
	//! relative to
	//!
	//! \param[in] asyncbydef Should files that are opened inside this
	//! directory, be opened for asynchronous IO (by default)? true
	//! for yes
	//!
	//! \param[in] writebydef Should files that are opened inside this
	//! directory, be opened for writing (by default)? true for yes
	//!
	//! \exception OutsideBounds iff the path is above 32,767
	//! characters (UTF-16 codepoints)
	//!
	//! \exception MemAlloc iff not enough space for path buffer
	//!
	//! \exception Unknown iff we get a failure that we did not
	//! anticipate
	Directory(Path path, Directory* owner, bool asyncbydef,
			  bool writebydef);
	// Directory(IORequired dirinfo, Directory* owner);

	//! \brief Gets the default status of subfiles wrt synchonicity
	//!
	//! \returns a boolean with value true iff subfiles are opened (by
	//! default) asynchronously
	bool asyncByDef() const noexcept;
	//! \brief Gets the default status of subfiles wrt write access
	//!
	//! \returns a boolean with value true iff subfiles are opened (by
	//! default) with write access
	bool writeByDef() const noexcept;

	//! \brief Will determine the fully qualified path of this (ie:
	//! incl. all parents / owners).
	//!
	//! \returns Fully Qualified path object. ie: m_Owner.path() +
	//! m_Path
	Path path() const;

	//! \brief Returns whether this directory doesn't (or does) have
	//! an owner
	//!
	//! \returns boolean with value true iff this directory does NOT
	//! have an owner
	bool isTopLevelDir() const noexcept;

	//! \brief Recursively determines the full path of the currently
	//! active directory
	//!
	//! \returns The path of the active directory, or the path of
	//! this, if there is none
	Path activeDirPath() const;

	//! \brief returns whether or not we have an active directory
	//!
	//! \returns boolean with value true iff we have an active
	//! directory
	bool isActiveDir() const;

	//! \brief Set the sub-directory that should be considered the
	//! active directory
	//!
	//! \returns A weak_ptr to the active sub-director that has been
	//! made the active directory. May return an empty weak_ptr, if no
	//! such directory
	//!
	//! \param[in] dirpath A string that holds the path of the
	//! directory to make active
	//!
	//! \exception CreationError if we failed to open the directory in
	//! question
	//!
	//! \exception Unknown if we have a path so outside the norm that
	//! we don't know at all what to do with it. Note: not 100% sure
	//! that this is even possible for paths that an average user
	//! would recognise as such. This error would require a drive
	//! designator in the middle of the path or something
	std::weak_ptr<Directory> setActiveDir(
		Strings::widestring dirpath);

	//! \brief Set the sub-directory that should be considered the
	//! active directory
	//!
	//! \returns A weak_ptr to the active sub-director that has been
	//! made the active directory. May return an empty weak_ptr, if no
	//! such directory
	//!
	//! \param[in] dirpath The path of the directory to make active
	//!
	//! \exception CreationError if we failed to open the directory in
	//! question
	//!
	//! \exception Unknown if we have a path so outside the norm that
	//! we don't know at all what to do with it. Note: not 100% sure
	//! that this is even possible for paths that an average user
	//! would recognise as such. This error would require a drive
	//! designator in the middle of the path or something
	std::weak_ptr<Directory> setActiveDir(Path dirpath);

	//! \brief Determines the name of all files in this directory (or
	//! the active directory)
	//!
	//! \returns An array of path segments, that hold the names of
	//! each file
	//!
	//! \param[in] useactivedir Should we get the contents of the
	//! active directory (if possible), or this directory? true for
	//! active directory, false for this
	std::vector<PathSegment> getAllFileNames(bool useactivedir) const;

	//! \brief Determines the paths of all files in this directory (or
	//! the active directory)
	//!
	//! \returns An array of paths of each sub-directory
	//!
	//! \param[in] useactivedir Should we get the contents of the
	//! active directory (if possible), or this directory? true for
	//! active directory, false for this
	std::vector<Path> getAllFilePaths(bool useactivedir) const;

	//! \brief Determines the name of all sub-directories in this
	//! directory (or the active directory)
	//!
	//! \returns An array of path segments, that hold the names of
	//! each sub-directory
	//!
	//! \param[in] useactivedir Should we get the contents of the
	//! active directory (if possible), or this directory? true for
	//! active directory, false for this
	std::vector<PathSegment> getAllDirectoryNames(
		bool useactivedir) const;

	//! \brief Determines the paths of all sub-directories in this
	//! directory (or the active directory)
	//!
	//! \returns An array of paths of each sub-directory
	//!
	//! \param[in] useactivedir Should we get the contents of the
	//! active directory (if possible), or this directory? true for
	//! active directory, false for this
	std::vector<Path> getAllDirectoryPaths(bool useactivedir) const;

	//! \brief Gets the name (only) of every open file in this
	//! directory (or active directory).
	//!
	//! \returns An array of pathsegments, each containing a name of
	//! an open file
	//!
	//! \param[in] useactivedir Should we get the contents of the
	//! active directory (if possible), or this directory? true for
	//! active directory, false for this
	std::vector<PathSegment> getOpenFileNames(
		bool useactivedir) const;

	//! \brief Gets the paths of every open file in this directory (or
	//! active directory). Optionally include file paths from open
	//! subdirectories
	//!
	//! \returns An array of paths, each pertaining to an open file
	//!
	//! \param[in] useactivedir Should the active directory be used?
	//! Or should we use this directory, regardless of what the active
	//! directory is?
	//!
	//! \param[in] includesubdirs Should we include the paths of all
	//! open files, in all open sub-directories? IF true, will include
	//! files from open subdirectories, recursively, up to the leaves
	//! of the file tree
	std::vector<Path> getOpenFilePaths(bool useactivedir,
									   bool includesubdirs) const;

	//! \brief Gets the name (only) of every open sub-directory in
	//! this directory (or active directory).
	//!
	//! \returns An array of pathsegments, each containing a name of
	//! an open directory
	//!
	//! \param[in] useactivedir Should we get the contents of the
	//! active directory (if possible), or this directory? true
	//! indicates active directory, false indicates this directory
	std::vector<PathSegment> getOpenDirectoryNames(
		bool useactivedir) const;

	//! \brief Gets the paths of every open sub-directory in this
	//! directory (or active directory). Optionally include
	//! sub-sub-directory paths from open subdirectories
	//!
	//! \returns An array of paths, each pertaining to an open
	//! directory
	//!
	//! \param[in] useactivedir Should the active directory be used?
	//! Or should we use this directory, regardless of what the active
	//! directory is?
	//!
	//! \param[in] includesubdirs Should we include the paths of all
	//! open sub-sub-directories, in all open sub-directories? IF
	//! true, will include sub-sub-directories from open
	//! subdirectories, recursively, up to the leaves of the file tree
	std::vector<Path> getOpenDirectoryPaths(
		bool useactivedir, bool includesubdirs) const;

	//! \brief get, and if it doesn't already exist, create, a
	//! subdirectory
	//!
	//! \returns a ptr to the directory in question
	//!
	//! \param[in] dirpath A string, containing the path to the
	//! directory in question
	//!
	//! \exception InvalidUserInput iff the directory is invalid in
	//! one of a myriad of ways
	//!
	//! \exception InvalidFunctionCall if we have a certain type of
	//! invalid path
	//!
	//! \exception Unknown if we manage to both open and not open a
	//! directory? That's a bad description, but this function is
	//! badly written
	std::shared_ptr<Directory> getSubDirectory(
		Strings::widestring dirpath);

	//! \brief get, and if it doesn't already exist, create, a
	//! subdirectory
	//!
	//! \returns a ptr to the directory in question
	//!
	//! \param[in] dirpath The path of the directory in question
	//!
	//! \exception InvalidUserInput iff the directory is invalid in
	//! one of a myriad of ways
	//!
	//! \exception InvalidFunctionCall if we have a certain type of
	//! invalid path
	//!
	//! \exception Unknown if we manage to both open and not open a
	//! directory? That's a bad description, but this function is
	//! badly written
	std::shared_ptr<Directory> getSubDirectory(Path dirpath);

	//! \brief get, and if it doesn't already exist, create, a
	//! subdirectory
	//!
	//! \returns a ptr to the directory in question
	//!
	//! \param[in] dirpath A path segment of the directory in question
	//!
	//! \exception CreationError if we could not create the desired
	//! directory, for whatever reason
	//!
	//! \exception InvalidUserInput iff the directory is invalid in
	//! one of a myriad of ways
	//!
	//! \exception InvalidFunctionCall if we have a certain type of
	//! invalid path
	//!
	//! \exception Unknown if we manage to both open and not open a
	//! directory? That's a bad description, but this function is
	//! badly written
	std::shared_ptr<Directory> getSubDirectory(PathSegment name);

	//! \brief Will attempt to close the specified sub-directory. Will
	//! attempt to recurse into the directory, if appropriate. If no
	//! such directory exists, will simply return (and add an error to
	//! the log)
	//!
	//! \param[in] dirpath A string containing the path of the
	//! sub-directory to close
	void closeSubDirectory(Strings::widestring dirpath);

	//! \brief Will attempt to close the specified sub-directory. Will
	//! attempt to recurse into the directory, if appropriate. If no
	//! such directory exists, will simply return (and add an error to
	//! the log)
	//!
	//! \param[in] dirpath The path of the sub-directory to close
	void closeSubDirectory(Path dirpath);

	//! \brief Will attempt to close the specified sub-directory. If
	//! no such directory exists, will simply return (and add an error
	//! to the log)
	//!
	//! \param[in] dirpath A path segment, of the sub-directory to
	//! close
	void closeSubDirectory(PathSegment name);

	//! \brief Get, and if we do not have, create, a file object that
	//! represents a file on disk. Optionally start reading the file
	//! (from disk)
	//!
	//! \returns A ptr to the file requested
	//!
	//! \param[in] filepath A string that holds a path pertaining to a
	//! file in the file system
	//!
	//! \param[in] startread If we should start reading the file,
	//! after creating the file object
	//!
	//! \exception CreationError iff we cannot create the specified
	//! file object, for whatever reason
	//!
	//! \exception InvalidUserInput iff an invalid file path
	//! is specified
	//!
	//! \exception Unknown if something goes very wrong, in a way that
	//! we don't expect
	std::shared_ptr<File> getFile(Strings::widestring filepath,
								  bool				  startread);

	//! \brief Get, and if we do not have, create, a file object that
	//! represents a file on disk. Optionally start reading the file
	//! (from disk)
	//!
	//! \returns A ptr to the file requested
	//!
	//! \param[in] filepath A path pertaining to a file in the file
	//! system
	//!
	//! \param[in] startread If we should start reading the file,
	//! after creating the file object
	//!
	//! \exception CreationError iff we cannot create the specified
	//! file object, for whatever reason
	//!
	//! \exception InvalidUserInput iff an invalid file path
	//! is specified
	//!
	//! \exception Unknown if something goes very wrong, in a way that
	//! we don't expect
	std::shared_ptr<File> getFile(Path filepath, bool startread);

	//! \brief Get, and if we do not have, create, a file object that
	//! represents a file on disk. Optionally start reading the file
	//! (from disk)
	//!
	//! \returns A ptr to the file requested
	//!
	//! \param[in] filepath A pathsegment pertaining to a file in this
	//! directory
	//!
	//! \param[in] startread If we should start reading the file,
	//! after creating the file object
	//!
	//! \exception CreationError iff we cannot create the specified
	//! file object, for whatever reason
	std::shared_ptr<File> getFile(PathSegment name, bool startread);

	// the bool is whether or not the file existed

	//! \brief Close a file, if it exists.
	//!
	//! \param[in] filepath A string containing a path of a file, that
	//! should be closed if it exists
	void closeFile(Strings::widestring filepath);

	//! \brief Close a file, if it exists.
	//!
	//! \param[in] filepath A path of a file, that should be closed if
	//! it exists
	void closeFile(Path filepath);

	//! \brief Close a file, if it exists.
	//!
	//! \param[in] filepath A pathsegment of a file in this directory
	void closeFile(PathSegment name);

	//! \brief Opens a file with the specified attributes, and then
	//! write to it
	//!
	//! \param[in] filepath A string containing a path for the file
	//! object on disk, that we wish to create an object for
	//!
	//! \param[in] contents Data, to be written to the file (on disk),
	//! when it is opened
	//!
	//! \param[in] async Should the file be open for asynchronous IO,
	//! or synchronous? true for asynchronous, false for synchronous
	//!
	//! \param[in] replaceexisting If the file (with the specified
	//! path) already exists, should we overwrite / replace it?
	//!
	//! \exception CreationError if for whatever reason, the actual
	//! file object construction fails
	//!
	//! \exception InvalidUserInput if the path is, in some way,
	//! invalid
	//!
	//! \exception Unknown if something goes wrong that we didn't see
	//! coming
	void createFile(Strings::widestring filepath, FileBuffer contents,
					bool async, bool replacexisting);

	//! \brief Opens a file with the specified attributes, and then
	//! write to it
	//!
	//! \param[in] filepath A path for the file object on disk, that
	//! we wish to create an object for
	//!
	//! \param[in] contents Data, to be written to the file (on disk),
	//! when it is opened
	//!
	//! \param[in] async Should the file be open for asynchronous IO,
	//! or synchronous? true for asynchronous, false for synchronous
	//!
	//! \param[in] replaceexisting If the file (with the specified
	//! path) already exists, should we overwrite / replace it?
	//!
	//! \exception CreationError if for whatever reason, the actual
	//! file object construction fails
	//!
	//! \exception InvalidUserInput if the path is, in some way,
	//! invalid
	//!
	//! \exception Unknown if something goes wrong that we didn't see
	//! coming
	void createFile(Path filepath, FileBuffer contents, bool async,
					bool replaceexisting);

	//! \brief Opens a file with the specified attributes, and then
	//! write to it
	//!
	//! \param[in] filepath A pathsegment (name) for the file object
	//! on disk that we wish to create an object for
	//!
	//! \param[in] contents Data, to be written to the file (on disk),
	//! when it is opened
	//!
	//! \param[in] async Should the file be open for asynchronous IO,
	//! or synchronous? true for asynchronous, false for synchronous
	//!
	//! \param[in] replaceexisting If the file (with the specified
	//! path) already exists, should we overwrite / replace it?
	//!
	//! \exception CreationError if for whatever reason, the actual
	//! file object construction fails
	void createFile(PathSegment name, FileBuffer contents, bool async,
					bool replaceexisting);

	// reopen files that already exist


	//! \brief Reopen files, that already exist
	//!
	//! \param[in] filepath A string containing the path of the file
	//! to be re-opened
	//!
	//! \param[in] async A boolean indicting whether or not to open
	//! the file asynchronously. True for asynchronously, false for
	//! synchronously
	//!
	//! \param[in] replaceexisting A boolean indictating whether or
	//! not to replace the existing contents of the file. True to
	//! replace existing contents, and false to not do so
	//!
	//! \exception CreationError if for whatever reason, the actual
	//! file object opening fails
	//!
	//! \exception InvalidUserInput if the file path was, in some way,
	//! invalid
	//!
	//! \exception Unknown if something we didn't anticipate, went
	//! wrong
	std::shared_ptr<File> reOpenFile(Strings::widestring filepath,
									 bool				 async,
									 bool replaceexisting);

	//! \brief Reopen files, that already exist
	//!
	//! \param[in] filepath The path of the file to be re-opened
	//!
	//! \param[in] async A boolean indicting whether or not to open
	//! the file asynchronously. True for asynchronously, false for
	//! synchronously
	//!
	//! \param[in] replaceexisting A boolean indictating whether or
	//! not to replace the existing contents of the file. True to
	//! replace existing contents, and false to not do so
	//!
	//! \exception CreationError if for whatever reason, the actual
	//! file object opening fails
	//!
	//! \exception InvalidUserInput if the file path was, in some way,
	//! invalid
	//!
	//! \exception Unknown if something we didn't anticipate, went
	//! wrong
	std::shared_ptr<File> reOpenFile(Path filepath, bool async,
									 bool replaceexisting);

	//! \brief Reopen files, that already exist
	//!
	//! \param[in] filepath The pathsegment (name) of the file to be
	//! re-opened
	//!
	//! \param[in] async A boolean indicting whether or not to open
	//! the file asynchronously. True for asynchronously, false for
	//! synchronously
	//!
	//! \param[in] replaceexisting A boolean indictating whether or
	//! not to replace the existing contents of the file. True to
	//! replace existing contents, and false to not do so
	//!
	//! \exception CreationError if for whatever reason, the actual
	//! file object opening fails
	std::shared_ptr<File> reOpenFile(PathSegment name, bool async,
									 bool replaceexisting);

	//! \brief Will try to delete the specified file **ON DISK**, if
	//! it exists
	//!
	//! \param[in] name A string the holds the path of the file to
	//! delete
	void deleteFile(Strings::widestring filePath);

	//! \brief Will try to delete the specified file **ON DISK**, if
	//! it exists
	//!
	//! \param[in] name The path of the file to delete
	void deleteFile(Path filePath);

	//! \brief Will try to delete the specified file **ON DISK**, if
	//! it exists
	//!
	//! \param[in] name A pathsegment the holds the name of the file
	//! to delete
	void deleteFile(PathSegment name);

	// will destroy this directory from the windows file
	// system. Only works if the directory is empty (in
	// windows)

	//! \brief will try to destroy this directory **ON DISK**, if it
	//! is empty
	void destroyThisDirectory();

	//! \brief Get the (OS) working directory of this process
	//!
	//! \warning **NOT THREAD SAFE - Current directory is stored (by
	//! windows) as a per-process global**
	//!
	//! \returns a (OS) wide string, containing the path of the
	//! current process
	static Strings::widestring getWindowsWorkingDir();

	//! \brief get the path of executable file of the current process
	//!
	//! \returns A (OS) wide string, containing the path of the
	//! current process exe
	//!
	//! \exception OutsideBounds iff the path is above 32,767
	//! characters (UTF-16 codepoints)
	//!
	//! \exception MemAlloc iff not enough space for path buffer
	//!
	//! \exception Unknown iff we get a failure that we did not
	//! anticipate
	static Strings::widestring getExePath();

	//! \brief Get the fully-qualified path of (the file that
	//! contains) the specified module
	//!
	//! \returns A (OS) wide string, containing the path of the
	//! specified module
	//!
	//! \param[in] Instance A handle to the module whose path we
	//! require
	//!
	//! \exception OutsideBounds iff the path is above 32,767
	//! characters (UTF-16 codepoints)
	//!
	//! \exception MemAlloc iff not enough space for path buffer
	//!
	//! \exception Unknown iff we get a failure that we did not
	//! anticipate
	static Strings::widestring getModulePath(HINSTANCE Instance);
};
}  // namespace FileIO
}  // namespace frao

#endif  //! FRAO_TRANESSENCE_BASIC_IOTYPES