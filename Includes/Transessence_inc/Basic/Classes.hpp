#ifndef FRAO_TRANESSENCE_BASIC_CLASSES
#define FRAO_TRANESSENCE_BASIC_CLASSES

//! \file
//!
//! \brief Header of basic file classes (paths, filebuffers, etc.)
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright Freya Rhiannon Mayger. All rights reserved

#include <Nucleus_inc\Environment.hpp>
#include <Nucleus_inc\Strings.hpp>
#include <list>

namespace frao
{
inline namespace FileIO
{
//! \brief For describing logical sections of paths ie: c: or \stuff
//! etc.
// NONE MUST ALWAYS == 0
// MAX_VALUE MUST ALWAYS BE LAST
enum class PathSegmentType : natural
{
	NONE = 0U,		   //!< DO NOT USE AS TYPE. Placeholder for
					   //!< possible future failure case.
	FILE,			   //!< for segments that are just file names
					   //!< ie: 'file.file'
	DIRECTORY,		   //!< for segments that are just directory
					   //!< names		ie: 'directory\'
	LONG_PATH,		   //!< for segments that are the long path chars
					   //!< ie: '\\?\' (and only that)
	ABSOLUTE_DRIVE,	//!< for segments that describe a drive
					   //!< ie: 'C:\'
	RELATIVE_DRIVE,	//!< for segments that describe a
					   //!< relative drive		ie: 'C:'
	RELATIVE_CURRENT,  //!< for segments that are relative to
					   //!< the current directory ie: '.\'
	RELATIVE_PARENT,   //!< for segments that are relative to
					   //!< the parent of the current directory
					   //!< ie: '..\'
	MAX_VALUE		   //!< DO NOT USE AS TYPE. Exists to provide a
					   //!< consistent max value, for reference
};

//! \class PathSegment
//!
//! \brief class for storing individual 'segments' of a path. If a
//! path is 'C:\\some\\dir\\names\\file.txt', then the segments are
//! the 'C:\\', 'some\\', 'dir\\', 'names\\', 'file.txt' for example
//!
//! \note **This class does not supprt '.' character** except where
//! used for file name extensions, and relative path indicators. ie:
//! valid cases are: 'file.ext', '.\\', '..\\'
class PathSegment
{
	Strings::widestring m_Segment;
	PathSegmentType		m_Type;

	//! \brief Will check string for invalid characters. ie: '\<',
	//! '\>', '\|' etc. forward slashes '/' aren't considered invalid,
	//! because it is assumed they have been removed (/ converted to
	//! '\\')
	//!
	//! \exception InvalidUserInput if the path segment (text) is
	//! invalid for the given path type
	void validate();

   public:
	//! \brief maximum length a path can have, before '\\', '\\', '?',
	//! '\\' will be prepended. Also the maximum of any single path
	//! segment
	static constexpr const natural64 MAX_SHORT_PATH =
		244;  // = 256 - 8.3 filename = 256 - 12
	static constexpr const natural64 MAX_LONG_PATH = 32767;

	//! \brief construct a path segment from a string and an expected
	//! segment type (directory, drive, file, etc.)
	//!
	//! \param[in] segment String with the text for this section of
	//! the string
	//!
	//! \param[in] type Type of segment, that this segment is expected
	//! to have. ie: directory, drive, file etc.
	//!
	//! \exception InvalidUserInput if the the path segment (string)
	//! is invalid for the given pathsegment type (that is, throws iff
	//! validate() does)
	PathSegment(Strings::widestring segment, PathSegmentType type);

	//! \brief get the text of this path segment. optionally remove
	//! trailing
	//! '\\' from directory segments (ONLY)
	//!
	//! \returns string with the text for this segment, optionally
	//! removing any trailing '\\' from directory segments (ONLY)
	//!
	//! \param[in] removelast should we, if this segment is a
	//! directory, remove the trailing '\\'
	Strings::widestring get(bool removelast) const;

	//! \brief get the type of this segment
	//!
	//! \returns PathSegmentType indicating the type of this segment
	PathSegmentType type() const;

	//! \brief get the extension of this segment, if this segment is a
	//! filename
	//!
	//! \returns string containing string of file extension
	//!
	//! \exception InvalidFunctionCall iff segment type is not file
	Strings::widestring fileextension() const;

	//! \brief gets how long the string of this segment is
	//!
	//! \returns length of the string of this segment
	natural64 length() const;

	//! \brief determines if this segement is equivalent to / the same
	//! as another
	//!
	//! \returns boolean with value true iff the two segments are the
	//! same
	//!
	//! \param[in] secondseg const reference to the other segment,
	//! that should be compared to this one
	bool operator==(const PathSegment& secondseg) const;

	//! \brief determines if this segment is different to another
	//!
	//! \returns boolean with value true iff the two segments are not
	//! the same
	//!
	//! \param[in] secondseg const reference to the other segment,
	//! that should be compared to this one
	bool operator!=(const PathSegment& secondseg) const;

	//! \brief determine if this segment is before another
	//!
	//! \returns boolean with value true iff this segment is
	//! conceptually *before* another (either by type, or then by
	//! string)
	bool operator<(const PathSegment& secondseg) const;
};

//! \class Path
//!
//! \brief class used to store sequences of path segments, that
//! togther constitute a path
//!
//! \detailed \verbatim Valid formats:
//! Absolute:
//! "\\?\<driveletter>:\<path>"
//! "<driveletter>:\<path>"
//! Relative:
//! "<driveletter>:<relativepath>"
//! ".\<relativepath>"
//! "..\<relativepath>"
//! "<relativepath>"
//! \endverbatim
//!
//! \todo make m_Path a vector? we don't exactly do that much
//! rearranging, etc.
//!
//! \todo Add iterators? do that give us anything of value here?
class Path
{
	// paths are always absolute in this
	std::list<PathSegment> m_Path;

	//! \brief subdivide a whole path into a series of pathsegments
	//! corresponding to a file heirarchy. ie: \verbatim
	//! ".\first\second\file.ext" will be { ".\", "first\", "second\",
	//! "file.ext" } \endverbatim
	//!
	//! \param[in] path the whole path, that needs to be subdivided
	//! into parts
	void parsePath(Strings::widestring path) noexcept;

	//! \brief essentially puts the path into a 'canonical' form,
	//! without '.\\' or '..\\' in the middle. ie: removes redundant
	//! parts of the path
	void rationalise();

   public:
	//! \brief default construct path, to empty path
	Path();

	//! \brief construct and rationalise path, into segments, from
	//! string
	//!
	//! \param fullpath the path that should be segmented and
	//! rationalised
	Path(Strings::widestring fullpath);

	//! \brief construct whole path from a single segment
	//!
	//! \param[in] segment PathSegment that should be made the start
	//! (or seed or whatever) of a whole path
	Path(PathSegment segment);

	//! \brief construct a path directly from a list of segments
	//!
	//! \param[in] fullpath List of segments that should be made into
	//! a single conherent path
	Path(std::list<PathSegment> fullpath);

	//! \brief standard (defaulted) copy constructor
	Path(const Path&) = default;

	//! \brief standard (defaulted) move constructor
	Path(Path&&) = default;

	//! \brief standard (defaulted) destructor
	~Path() = default;

	//! \brief standard (defaulted) copy assignment operator
	Path& operator=(const Path&) = default;

	//! \brief standard (defaulted) move assignment operator
	Path& operator=(Path&&) = default;

	//! \brief get a copy of the segments of path, in order
	//!
	//! \returns a copy of each of the elements of this path, in order
	std::list<PathSegment> getListCopy() const;

	//! \brief get a copy of the front of the path. That is, the first
	//! segment in the path
	//!
	//! \returns a copy of the first segment of the path (for example,
	//! a drive signifier, if this is an absolute path)
	//!
	//! \exception NotFound iff path is empty (has no front)
	PathSegment frontCopy() const;

	//! \brief get a copy of the back of the path. That is, the last
	//! segment in the path
	//!
	//! \returns a copy of the last segment of the path (for example,
	//! a file, if the path as a whole specifies a file)
	//!
	//! \exception NotFound iff path is empty (has no back)
	PathSegment backCopy() const;

	//! \brief remove the first segment from the path, and return a
	//! copy to the caller
	//!
	//! \returns A copy of the pathsegment at the front of path
	//!
	//! \exception NotFound iff path is empty (has no front)
	PathSegment claimFront();

	//! \brief remove the last segment from the path, and return a
	//! copy to the caller
	//!
	//! \returns A copy of the pathsegment at the back of the path
	//!
	//! \exception NotFound iff path is empty (has no back)
	PathSegment claimBack();

	//! \brief get and remove the first meaningful segment, from the
	//! path. That is, will get the first drive / directory in the
	//! path
	//!
	//! \returns A copy of the first meaningful (directory / drive
	//! etc.)
	//!
	//! \exception NotFound iff no such meaningful segment is found
	//! (or if path is empty)
	PathSegment claimTopLevel();

	//! \brief will claim (get and remove from this) all directories
	//! except the first. ie: in \verbatim "C:\first\second\third", it
	//! will return "second\third" \endverbatim
	//!
	//! \returns The removed path, which is the path starting at the
	//! second directory
	//!
	//! \exception NotFound iff no such first directory is found
	Path claimSubDirectoryList();

	//! \brief add a segment to the front (beginning / base) of the
	//! path
	//!
	//! \param[in] segment The segment to add to the beginning (front
	//! / base) of the path
	void addFront(PathSegment segment);

	//! \brief add a segment to the back (end) of the path
	//!
	//! \param[in] segment The segment to add to the end (back) of the
	//! path
	void addBack(PathSegment segment);

	//! \brief determines whether the path is relative to another
	//! directory / drive, or is fully specified
	//!
	//! \returns boolean with value true iff the path IS NOT fully
	//! specified
	bool isRelative() const;

	//! \brief determines whether the path is fully specified, or is
	//! relative to another directory / drive
	//!
	//! \returns boolean with value true iff the path IS fully
	//! specified
	bool isAbsolute() const;

	//! \brief determines whether this is a non-null path
	//!
	//! \returns boolean with value true if this path has a non-zero
	//! number of segments
	bool isNotEmpty() const;

	//! \brief determines whether this is a null path
	//!
	//! \returns boolean with value true iff this path has zero
	//! segments
	bool isEmpty() const;

	//! \brief determines the number of segments, in this path
	//!
	//! \returns an unsigned integer with the number of segments of
	//! this path
	natural64 listsize() const;

	//! \brief turn this path into a null path (that is, remove all
	//! segments from this path)
	void makeEmpty();

	//! \brief will ensure that, if required, \verbatim \\?\
	//! \endverbatim will be included at the start of the path (ie: if
	//! 'character' length is longer than MAX_PATH). That is,
	//! literally "correct" the path
	void correctLength();

	//! \brief gets the 'character' length of the string.
	//!
	//! \note *This is not the UNICODE length* of the string. This is
	//! the (effectively) the number of bytes used by the string
	//! divided by two. Or alternatively, the number of UTF16
	//! CodePoints used, with surrogate pairs counted as two
	//! characters
	//!
	//! \returns The number of 'characters' in the string
	natural64 pathlength() const;

	//! \brief Turns a relative path into an absolute one, by the user
	//! telling this what absolute location this path is relative to
	//!
	//! \returns reference to this
	//!
	//! \param[in] abspath An **Absolute** path that will be taken as
	//! the 'base' for this, so that this can be made absolute
	//!
	//! \exception InvalidUserInput iff the passed path IS NOT
	//! absolute
	Path& makeAbsolute(Path abspath);

	//! \brief remove any file segment that is present in this path
	//!
	//! \returns reference to this
	Path& removeFileSegment();

	//! \brief copies this, adds specified segment to the end, and
	//! returns the copy
	//!
	//! \returns new Path object with value equivalent to segment
	//! appended to this
	//!
	//! \param[in] segment The segment to add to the copy of this path
	Path operator+(const PathSegment& segment) const;

	//! \brief copies this, adds specified secondpath to the end, and
	//! returns the copy
	//!
	//! \returns new Path object with value equivalent to secondpath
	//! appended to this
	//!
	//! \param[in] secondpath The path to add to the copy of this path
	Path operator+(const Path& secondpath) const;

	//! \brief adds a segment to the end of this
	//!
	//! \returns reference to this
	//!
	//! \param[in] segment Segment that should be appended to the end
	//! of this
	Path& operator+=(const PathSegment& segment);

	//! \brief adds a path to the end of this
	//!
	//! \returns reference to this
	//!
	//! \param[in] secondpath path that should be appended to the end
	//! of this
	Path& operator+=(const Path& secondpath);

	//! \brief conceptually, will copy this, will find first section
	//! of this that matches the specifed path, and remove it the rest
	//! of the path after the match. If no match is made, return copy
	//! of this
	//!
	//! \returns copy of this, up until the point of match
	//! (uninclusive)
	//!
	//! \param[in] secondpath path to match against this
	Path operator-(const Path& secondpath) const;

	//! \brief determines if the two paths are exactly the same
	//!
	//! \returns boolen with value true iff all segments compare as
	//! equal
	//!
	//! \param[in] secondpath path to compare against this
	bool operator==(const Path& secondpath) const;

	//! \brief determines if the two paths differ
	//!
	//! \returns boolean with value true iff one or more segments
	//! differ
	//!
	//! \param[in] secondpath path to compare against this
	bool operator!=(const Path& secondpath) const;

	//! \brief orders the two paths according to #1: number of
	//! segments #2: first non-matching segment ordering
	//!
	//! \returns boolean with value true iff this evaluates to before
	//! rhs
	//!
	//! \param[in] rhs Path to comapare this against. Will be the
	//! right hand operand
	bool operator<(const Path& rhs) const;

	//! \brief get the widestring (windows) of this
	//!
	//! \returns a widestring (that is, a windows wchar_t string) of
	//! this path
	explicit operator Strings::widestring() const;
};

//! \class IORequired
//!
//! \brief struct for informing a function of what io object is
//! required, and what terms
struct IORequired
{
	PathSegment m_FileName;
	bool		m_Async;
	bool		m_WriteAccess;

	IORequired(PathSegment filename, bool async, bool writeacess);
};

//! \class FileAttributes
//!
//! struct for holding information on a file in file system, and what
//! attributes it has. for example, whether it is a directory, and its
//! size

struct FileAttributes
{
	//! \brief Construct an unfound FileAttribute object
	//!
	//! \note Unfound file attributes are used when a file could not
	//! be located, as it did not exist. This is distinct from not
	//! being able to locate the file, because of some failure
	FileAttributes();
	//! \brief Construct a found FileAttribute object
	//!
	//! \param[in] fileSize The size of the file, in bytes.
	//!
	//! \param[in] isDirectory Is the file actually a directory?
	FileAttributes(natural64 fileSize, bool isDirectory);

	//! \brief Returns a value indicating whether the file exists
	//!
	//! \returns true of teh file exists
	explicit operator bool() const noexcept;

	//! \brief The size of the file, in bytes
	natural64 m_FileSize;
	//! \brief Whether or not the 'file' is actually a directory
	bool	  m_Directory;

	private:
	//! \brief true if the file exists, and was found.
	bool	  m_Found;
};

//! \class FileBuffer
//!
//! \brief class for holding the raw byte data from a given file
//!
//! \todo add iterators, and the like
//!
//! \todo distinguish between allocated size and used size
class FileBuffer final
{
	natural8* m_Buffer;
	natural64 m_Size;

   public:
	//! \brief construct a filebuffer with a given size
	//!
	//! \param[in] size Number of bytes that the filebuffer should be
	//! able to hold
	//!
	//! \exception MemAlloc iff cannot allocate memory
	FileBuffer(natural64 size = 0);

	//! \brief construct a filebuffer from a sequence of bytes
	//!
	//! \param[in] buf Sequence of bytes to create buffer from
	//!
	//! \exception MemAlloc iff cannnot allocate memory
	FileBuffer(std::initializer_list<natural8> buf);

	//! \brief copy construct a filebuffer, from another
	//!
	//! \param[in] buffer Buffer to copy from
	//!
	//! \exception MemAlloc iff cannot allocate memory for copy
	FileBuffer(const FileBuffer& buffer);

	//! \brief move construct a filebuffer, from another
	//!
	//! \param[in] buffer Buffer to move from. Will be left in a
	//! potentially invalid state
	FileBuffer(FileBuffer&& buffer) noexcept;

	//! \brief destroy and deallocate buffer
	~FileBuffer() noexcept;

	//! \brief copy assignment from another buffer. Will (obviously)
	//! delete existing buffer
	//!
	//! \param[in] buffer to be copied from
	//!
	//! \exception MemAlloc iff cannot allocate memory for copy
	FileBuffer& operator=(const FileBuffer& buffer);

	//! \brief move assignment from another buffer. Will (obviously)
	//! delete existing buffer
	//!
	//! \param temp Buffer to be moved from. Will be left in a
	//! potentially invalid state
	FileBuffer& operator=(FileBuffer&& temp) noexcept;

	//! \brief allows a caller to claim ownership of this' buffer.
	//! this object will be left empty, and reponsibility for
	//! deallocation and destruction of buffer will pass to the caller
	//!
	//! \returns ptr to (now caller owned) buffer of data
	natural8* releaseBuffer() noexcept;

	//! \brief Allows this to take ownership of a caller-owned buffer.
	//! Caller will no longer need to worry about deallocation and
	//! destruction of buffer, as well as having access to any
	//! facilities this class offers
	//!
	//! \param[in] buffer Reference to the buffer ptr. Will be claimed
	//! by this, and passed ptr will be set to nullptr
	//!
	//! \param[in] size size of passed buffer, in bytes
	void claimBuffer(natural8*& buffer, natural64 size) noexcept;

	//! \brief will insert the data, specified by the ptr and the
	//! length, starting at the index provided
	//!
	//! \param[in] add Ptr to buffer, containing the data to insert
	//!
	//! \param[in] addlength The length of the buffer, pointed to by
	//! add
	//!
	//! \param[in] startpos The index (0-indexed) at which to start
	//! inserting the data
	//!
	//! \exception MemAlloc iff we need to reallocate buffer to
	//! complete operation, and we cannot allocate enough memory
	void changeData(const natural8* add, natural64 addlength,
					natural64 startpos);

	//! \brief will insert the data, as specified by the
	//! initializer_list, starting at the index provided
	//!
	//! \param[in] add list of data to insert at the given location
	//!
	//! \param[in] startpos The index (0-indexed) at which to start
	//! inserting the data
	//!
	//! \exception MemAlloc iff we need to reallocate buffer to
	//! complete operation, and we cannot allocate enough memory
	void changeData(std::initializer_list<natural8> add,
					natural64						startpos);

	//! \brief will add data, as specified by parameters, to the end
	//! of the buffer
	//!
	//! \param[in] add Ptr to buffer, containing the data to append
	//!
	//! \param[in] addlength The length of the buffer, pointed to by
	//! add
	//!
	//! \exception MemAlloc iff we need to reallocate buffer to
	//! complete operation, and we cannot allocate enough memory
	void appendData(const natural8* add, natural64 length);

	// THROWS: MemAlloc . IF: No memory

	//! \brief will add data, as specified by the parameter, to the
	//! end of the buffer
	//!
	//! \param[in] add list of data to insert at the given location
	//!
	//! \exception MemAlloc iff we need to reallocate buffer to
	//! complete operation, and we cannot allocate enough memory
	void appendData(std::initializer_list<natural8> add);

	//! \brief returns raw ptr to internal buffer. Do not deallocate,
	//! or do anything stupid with it
	//!
	//! \returns Raw ptr to buffer. Use as own risk
	natural8* unsafeBufferRef() noexcept;

	//! \brief destroy and dealloc buffer, and make internal buffer
	//! null
	void clear() noexcept;

	//! \brief change the number of bytes the buffer can store.
	//! Optionally, keep the data in the buffer (or as much as
	//! possible).
	//!
	//! \param[in] newsize The size, in bytes, that the buffer should
	//! now be able to store
	//!
	//! \param[in] keepolddata Should the existing data in the buffer
	//! be kept, after the resize operation? **If false, data will be
	//! discarded, even if resize is a no-op** (resize can be no-op if
	//! new size is same as existing)
	//!
	//! \exception MemAlloc iff cannot allocate memory
	void resizeBuffer(natural64 newsize, bool keepolddata);

	//! \brief will make a copy of this buffer, and return it
	//!
	//! \returns perfect copy of this buffer
	//!
	//! \exception MemAlloc iff cannot memory for copy
	FileBuffer copy() const;

	//! \brief will get the size, in bytes, of the buffer
	//!
	//! \returns size in bytes, of buffer
	natural64 size() const noexcept;

	//! \brief will get the byte at the given index
	//!
	//! \returns reference to the byte in question, unless index is
	//! out of bounds (where last byte will be returned)
	natural8& operator[](natural64 index) noexcept;
	//! \brief will get the byte at the given index
	//!
	//! \returns const reference to the byte in question, unless index
	//! is out of bounds (where last byte will be returned)
	const natural8& operator[](natural64 index) const noexcept;

	//! \brief will determine is the buffer is non-null
	//!
	//! \returns true iff buffer has a non-null ptr and a non-null
	//! size
	explicit operator bool() const;
};
}  // namespace FileIO
}  // namespace frao

#endif  //! FRAO_TRANESSENCE_BASIC_CLASSES