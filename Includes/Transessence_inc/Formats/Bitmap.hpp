#ifndef FRAO_TRANESSENCE_FORMATS_BITMAP
#define FRAO_TRANESSENCE_FORMATS_BITMAP

//! \file
//!
//! \brief Header for interpretation of bitmaps
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <Nucleus_inc\Environment.hpp>
#include <Nucleus_inc\Error.hpp>
#include <Nucleus_inc\Strings.hpp>
#include <Protean_inc\Structures\ContiguousArray.hpp>

namespace frao
{
inline namespace FileIO
{
namespace FileFormats
{
namespace Bitmap
{
enum class Compression
{
	RGB		 = 0,
	RLE8	 = 1,
	RLE4	 = 2,
	BITFIELD = 3,
	JPEG	 = 4,
	PNG		 = 5
};
enum class ColourSpaceType
{
	RGB_Calibrated	 = 0,
	sRGB			 = 1,
	Windows_Colour	 = 2,
	Profile_Linked	 = 3,
	Profile_Embedded = 4
};
enum class Intent
{
	Business	 = 0b0001,	// 1
	Graphics	 = 0b0010,	// 2
	Image		 = 0b0100,	// 4
	Colourmetric = 0b1000	// 8
};


constexpr small_nat32 toNumber(Compression compression) noexcept
{
	// ie: if out of range of valid values
	if (static_cast<small_nat32>(compression) >= 6)
	{
		// modulo 6, to get number in range
		return (static_cast<small_nat32>(compression) % 6);
	}

	return static_cast<small_nat32>(compression);
}
constexpr small_nat32 toNumber(
	ColourSpaceType colourSpaceType) noexcept
{
	// ie: if out of range of valid values
	if (static_cast<small_nat32>(colourSpaceType) >= 5)
	{
		// modulo 6, to get number in range
		return (static_cast<small_nat32>(colourSpaceType) % 5);
	}

	return static_cast<small_nat32>(colourSpaceType);
}
constexpr small_nat32 toNumber(Intent intent) noexcept
{
	small_nat32 result = static_cast<small_nat32>(intent);

	// ensure not out of range
	result = result % 16;

	if (result == 0)
	{
		result = static_cast<small_nat32>(Intent::Business);
	}

	// make sure only one bit is set. If not, take lowest
	// bit
	if (!(result & (result - 1)))
	{
		// take lowest bit
		result &= ~result + 1;
	}

	return result;
}
constexpr Compression toCompressionEnum(small_nat32 number) noexcept
{
	switch (number)
	{
		case 1:
			static_assert(1 == toNumber(Compression::RLE8),
						  "Enum values must match numbers!");

			return Compression::RLE8;

		case 2:
			static_assert(2 == toNumber(Compression::RLE4),
						  "Enum values must match numbers!");

			return Compression::RLE4;
		case 3:
			static_assert(3 == toNumber(Compression::BITFIELD),
						  "Enum values must match numbers!");

			return Compression::BITFIELD;
		case 4:
			static_assert(4 == toNumber(Compression::JPEG),
						  "Enum values must match numbers!");

			return Compression::JPEG;
		case 5:
			static_assert(5 == toNumber(Compression::PNG),
						  "Enum values must match numbers!");

			return Compression::PNG;
		default:
			static_assert(0 == toNumber(Compression::RGB),
						  "Enum values must match numbers!");

			return Compression::RGB;
	}
}
constexpr ColourSpaceType toColourSpaceEnum(
	small_nat32 number) noexcept
{
	switch (number)
	{
		case 0:
			static_assert(
				0 == toNumber(ColourSpaceType::RGB_Calibrated),
				"Enum values must match numbers!");

			return ColourSpaceType::RGB_Calibrated;

		case 2:	 // LCS_WINDOWS_COLOR_SPACE
			static_assert(
				2 == toNumber(ColourSpaceType::Windows_Colour),
				"Enum values must match numbers!");

			return ColourSpaceType::Windows_Colour;
		case 3:	 // PROFILE_LINKED
			static_assert(
				3 == toNumber(ColourSpaceType::Profile_Linked),
				"Enum values must match numbers!");

			return ColourSpaceType::Profile_Linked;
		case 4:	 // PROFILE_EMBEDDED
			static_assert(
				4 == toNumber(ColourSpaceType::Profile_Embedded),
				"Enum values must match numbers!");

			return ColourSpaceType::Profile_Embedded;
		default:
			static_assert(1 == toNumber(ColourSpaceType::sRGB),
						  "Enum values must match numbers!");

			return ColourSpaceType::sRGB;
	}
}
constexpr Intent toIntentEnum(small_nat32 number) noexcept
{
	switch (number)
	{
		case 1:
			static_assert(1 == toNumber(Intent::Business),
						  "Enum values must match numbers!");

			return Intent::Business;
		case 2:
			static_assert(2 == toNumber(Intent::Graphics),
						  "Enum values must match numbers!");

			return Intent::Graphics;
		case 8:
			static_assert(8 == toNumber(Intent::Colourmetric),
						  "Enum values must match numbers!");

			return Intent::Colourmetric;

		default:
			static_assert(4 == toNumber(Intent::Image),
						  "Enum values must match numbers!");

			return Intent::Image;
	}
}

class Header
{
	// THESE MEMBERS ARE NOT IN FILE ORDER (OR THE SAME AS
	// HEADER MEMBERS IN FILE)
	small_nat32 m_FileSize;
	small_nat32 m_OffsetBytes;
	small_nat16 m_Reserved1;
	small_nat16 m_Reserved2;

   public:
	// will get header (assumes it starts at
	// dataBufferPtr[0]) will throw if header does not start
	// 0x42 0x4D ('B', 'M'), or if bufferSize < requiredSize
	Header(const small_nat8* dataBufferPtr,
		   natural64		 remainingBufferSize);
	Header(small_nat32 filesize, small_nat32 offsetBytes) noexcept;
	Header(small_nat32 fileSize, small_nat32 offsetBytes,
		   small_nat16 reserved1, small_nat16 reserved2) noexcept;

	// size of the header on file
	constexpr static natural64 sizeInFile() noexcept
	{
		return 14;
	}

	small_nat32 bitmapOffset() const noexcept;

	// will write file header to buffer, and return written
	// bytes
	natural64 writeTo(small_nat8* dataBufferPtr,
					  natural64	  bufferSize) const;
};

struct CoreInfoData
{
	small_nat32 m_InfoLength;  // length of info header
	small_nat16 m_Width;
	small_nat16 m_Height;
	small_nat16 m_Bitcount;

	constexpr static natural64 sizeInFile() noexcept
	{
		return 12;
	}
};

struct InfoData
{
	small_nat32 m_InfoLength;  // length of info header
	small_int32 m_Width;
	small_int32 m_Height;
	small_nat32 m_ImageByteSize;
	small_int32 m_HorizontalResolution;
	small_int32 m_VerticalResolution;
	small_nat32 m_ColoursUsed;
	small_nat32 m_ImportantColours;
	small_nat32 m_RedMask;
	small_nat32 m_GreenMask;
	small_nat32 m_BlueMask;
	small_nat16 m_Bitcount;
	Compression m_Compression;

	// does not include masks, because for V3 header, they
	// are technically 'after' the info header, not a part of
	// it!
	constexpr static natural64 sizeInFile() noexcept
	{
		return 40;
	}
};

struct InfoV4Data
{
	small_nat32		m_InfoLength;  // length of info header
	small_int32		m_Width;
	small_int32		m_Height;
	small_nat32		m_ImageByteSize;
	small_int32		m_HorizontalResolution;
	small_int32		m_VerticalResolution;
	small_nat32		m_ColoursUsed;
	small_nat32		m_ImportantColours;
	small_nat32		m_RedMask;
	small_nat32		m_GreenMask;
	small_nat32		m_BlueMask;
	small_nat32		m_AlphaMask;
	small_int32		m_CIERedX;
	small_int32		m_CIERedY;
	small_int32		m_CIERedZ;
	small_int32		m_CIEGreenX;
	small_int32		m_CIEGreenY;
	small_int32		m_CIEGreenZ;
	small_int32		m_CIEBlueX;
	small_int32		m_CIEBlueY;
	small_int32		m_CIEBlueZ;
	small_nat32		m_GammaRed;
	small_nat32		m_GammaGreen;
	small_nat32		m_GammaBlue;
	small_nat16		m_Bitcount;
	Compression		m_Compression;
	ColourSpaceType m_ColourSpaceType;

	constexpr static natural64 sizeInFile() noexcept
	{
		return 108;
	}
};

struct InfoV5Data
{
	small_nat32		m_InfoLength;  // length of info header
	small_int32		m_Width;
	small_int32		m_Height;
	small_nat32		m_ImageByteSize;
	small_int32		m_HorizontalResolution;
	small_int32		m_VerticalResolution;
	small_nat32		m_ColoursUsed;
	small_nat32		m_ImportantColours;
	small_nat32		m_RedMask;
	small_nat32		m_GreenMask;
	small_nat32		m_BlueMask;
	small_nat32		m_AlphaMask;
	small_int32		m_CIERedX;
	small_int32		m_CIERedY;
	small_int32		m_CIERedZ;
	small_int32		m_CIEGreenX;
	small_int32		m_CIEGreenY;
	small_int32		m_CIEGreenZ;
	small_int32		m_CIEBlueX;
	small_int32		m_CIEBlueY;
	small_int32		m_CIEBlueZ;
	small_nat32		m_GammaRed;
	small_nat32		m_GammaGreen;
	small_nat32		m_GammaBlue;
	small_nat32		m_ProfileData;
	small_nat32		m_ProfileSize;
	small_nat32		m_Reserved;
	small_nat16		m_Bitcount;
	Compression		m_Compression;
	ColourSpaceType m_ColourSpaceType;
	Intent			m_Intent;

	constexpr static natural64 sizeInFile() noexcept
	{
		return 140;
	}
};

namespace Detail
{
template<bool EnableAction>
struct ErrClass
{
	static void send(Strings::utf8string location,
					 Strings::utf8string message) noexcept
	{}
};

template<>
struct ErrClass<true>
{
	static void send(Strings::utf8string location,
					 Strings::utf8string message) noexcept
	{
		getErrorLog()->addError<InvalidFileFormat>(
			std::move(location), std::move(message));
	}
};

enum class FailCode
{
	FatalFailure	   = 0b00,
	RecoverableFailure = 0b01,
	Success			   = 0b11
};

constexpr FailCode makeFailCode(bool success,
								bool canContinue) noexcept
{
	if (success)
	{
		return FailCode::Success;
	} else if (canContinue)
	{
		return FailCode::RecoverableFailure;
	} else
	{
		return FailCode::FatalFailure;
	}
}

constexpr FailCode operator&(const FailCode& lhs,
							 const FailCode& rhs) noexcept
{
	return static_cast<FailCode>(static_cast<natural>(lhs)
								 & static_cast<natural>(rhs));
}
constexpr FailCode operator|(const FailCode& lhs,
							 const FailCode& rhs) noexcept
{
	return static_cast<FailCode>(static_cast<natural>(lhs)
								 | static_cast<natural>(rhs));
}

constexpr FailCode operator&=(FailCode&		  lhs,
							  const FailCode& rhs) noexcept
{
	lhs = (lhs & rhs);
	return lhs;
}
constexpr FailCode operator|=(FailCode&		  lhs,
							  const FailCode& rhs) noexcept
{
	lhs = (lhs | rhs);

	return lhs;
}

constexpr bool isSuccess(FailCode code) noexcept
{
	return (code == FailCode::Success);
}
constexpr bool shouldHalt(FailCode code) noexcept
{
	return (code == FailCode::FatalFailure);
}

}  // namespace Detail

template<bool v2Rules = false>
Detail::FailCode isValidHeight(small_int32 height) noexcept
{
	bool result;

	if constexpr (v2Rules)
	{
		result =
			((height > 0)
			 && (height <= std::numeric_limits<small_nat16>::max()));
	} else
	{
		result = height != 0;
	}

	return Detail::makeFailCode(result, false);
}
template<bool v2Rules = false>
Detail::FailCode isValidWidth(small_int32 width) noexcept
{
	bool result = (width > 0);

	if constexpr (v2Rules)
	{
		// note: this is bitwise and (& NOT &&)
		result &= (width <= std::numeric_limits<small_nat16>::max());
	}

	return Detail::makeFailCode(result, false);
}
template<bool v2Rules = false>
Detail::FailCode isValidBitcount(small_nat16 bitcount) noexcept
{
	bool result;

	switch (bitcount)
	{
		case 1:
			[[fallthrough]];
		case 4:
			[[fallthrough]];
		case 8:
			[[fallthrough]];
		case 24:
			result = true;
			break;
		case 16:
			[[fallthrough]];
		case 32:
			[[fallthrough]];
		case 0:
			if constexpr (!v2Rules)
			{
				result = true;
				break;
			}
			[[fallthrough]];
		default:

			result = false;
	}

	return Detail::makeFailCode(result, false);
}
Detail::FailCode heightMatchesCompression(
	small_int32 imageHeight, Compression compression) noexcept;
Detail::FailCode isValidCompression(Strings::utf8string& errStr,
									Compression			 compression,
									small_nat16 bitcount) noexcept;
Detail::FailCode checkUsedColours(Strings::utf8string& errStr,
								  small_nat32		   coloursUsed,
								  small_nat16 bitcount) noexcept;
Detail::FailCode checkImportantColours(
	small_nat32 coloursUsed, small_nat32 coloursImportant) noexcept;
Detail::FailCode checkColourMasks(small_nat32 redmask,
								  small_nat32 greenmask,
								  small_nat32 bluemask,
								  Compression compression) noexcept;
Detail::FailCode checkValidColourSpace(ColourSpaceType colourspace,
									   bool v5Header) noexcept;
Detail::FailCode checkValidIntent(Intent intent) noexcept;


Detail::FailCode checkSupportedCompression(
	Compression compression) noexcept;
Detail::FailCode checkSupportedColourSpace(
	ColourSpaceType colourspace) noexcept;


// TODO: Add IMPL only stuff to source file!


class ColourMasks
{
	small_nat32 m_RedMask;
	float		m_RedStep;
	small_nat32 m_GreenMask;
	float		m_GreenStep;
	small_nat32 m_BlueMask;
	float		m_BlueStep;
	small_nat32 m_AlphaMask;
	float		m_AlphaStep;

   public:
	ColourMasks() noexcept;
	ColourMasks(small_nat32 redMask, small_nat32 greenMask,
				small_nat32 blueMask) noexcept;
	ColourMasks(small_nat32 redMask, small_nat32 greenMask,
				small_nat32 blueMask, small_nat32 alphaMask) noexcept;

	small_nat32 getMaxRedComponent() const noexcept;
	small_nat32 getMaxGreenComponent() const noexcept;
	small_nat32 getMaxBlueComponent() const noexcept;
	small_nat32 getMaxAlphaComponent() const noexcept;

	// obviously will not work with more than 8 bits /
	// channel
	small_nat8 getRedComponent(small_nat32 rawData) const noexcept;
	small_nat8 getGreenComponent(small_nat32 rawData) const noexcept;
	small_nat8 getBlueComponent(small_nat32 rawData) const noexcept;
	small_nat8 getAlphaComponent(small_nat32 rawData) const noexcept;

	float getRedComponentFloat(small_nat32 rawData) const noexcept;
	float getGreenComponentFloat(small_nat32 rawData) const noexcept;
	float getBlueComponentFloat(small_nat32 rawData) const noexcept;
	float getAlphaComponentFloat(small_nat32 rawData) const noexcept;

	explicit operator bool() const noexcept
	{
		small_nat32 orCombined =
			(m_RedMask | m_GreenMask | m_BlueMask | m_AlphaMask);
		bool contiguous =
			((((m_RedMask ^ m_GreenMask) ^ m_BlueMask) ^ m_AlphaMask)
			 == orCombined);

		return (contiguous && (orCombined != 0));
	}
};


class InfoHeader
{
   public:
	InfoHeader() noexcept		   = default;
	virtual ~InfoHeader() noexcept = default;

	virtual small_int32 getWidth() const noexcept  = 0;
	virtual small_int32 getHeight() const noexcept = 0;

	virtual small_nat32 getBitcount() const noexcept	= 0;
	virtual Compression getCompression() const noexcept = 0;

	// do we have a palette of colours, that is actualy
	// useful? (because can have useless ones sitting in the
	// file, for some reason...)
	virtual bool hasColourPalette() const noexcept = 0;
	// if we don't have a colour palette, we might have
	// masks instead. Do we?
	virtual bool hasColourMasks() const noexcept = 0;
	// returns the number of colours in the palette
	// immediately following the info header
	virtual natural64	colourPaletteSize() const noexcept = 0;
	virtual ColourMasks colourMasks() const noexcept	   = 0;

	// will return the (file) size of the header that we
	// actually have (ie: V5, V4 etc), not just
	// CoreInfoHeader
	virtual natural64 currentSizeInFile() const noexcept = 0;

	virtual natural64		 writeTo(small_nat8* dataBufferPtr,
									 natural64	 bufferSize) const = 0;
	virtual Detail::FailCode isValid(
		bool shortcircuit	 = false,
		bool sendErrorsToLog = true) const noexcept = 0;
};

template<typename DataClass>
class InfoHeaderIMPL final : public InfoHeader
{
	DataClass m_Data;

	constexpr static const bool
		m_V2Rules = std::is_same<DataClass, CoreInfoData>::value,
		m_V3Rules = std::is_same<DataClass, InfoData>::value,
		m_V4Rules = std::is_same<DataClass, InfoV4Data>::value,
		m_V5Rules = std::is_same<DataClass, InfoV5Data>::value;

	static_assert(m_V2Rules || m_V3Rules || m_V4Rules || m_V5Rules,
				  "DataClass indicated must one of the "
				  "v2-5 bitmap file header data structs");

	Detail::FailCode isValidDataIMPL(
		bool shortcircuit, bool sendErrorsToLog) const noexcept
	{
		void (*errFunc)(Strings::utf8string, Strings::utf8string) =
			sendErrorsToLog ? Detail::ErrClass<true>::send
							: Detail::ErrClass<false>::send;

		Detail::FailCode result		= Detail::FailCode::Success,
						 lastResult = Detail::FailCode::Success;

		lastResult = isValidHeight<m_V2Rules>(
			static_cast<small_int32>(m_Data.m_Height));
		result &= lastResult;

		if (!Detail::isSuccess(lastResult))
		{
			errFunc(
				FILELOC_UTF8,
				u8"Invalid Height! "_ustring
					+ (m_V2Rules ? u8"V2 "_ustring : u8"V3+ "_ustring)
					+ u8"headers allow values in the range: "_ustring
					+ (m_V2Rules
						   ? u8"(0, 2^16)"_ustring
						   : u8"(-2^32, 2^32) except 0"_ustring));

			// no way to avoid branching each time here, as
			// far as I can tell. If we did the 'sendErrors'
			// template function trick, we'd just have to
			// branch again on lastResult, instead
			if (Detail::shouldHalt(lastResult) && shortcircuit)
			{
				return result;
			}
		}

		lastResult = isValidWidth<m_V2Rules>(
			static_cast<small_int32>(m_Data.m_Width));
		result &= lastResult;

		if (!Detail::isSuccess(lastResult))
		{
			errFunc(
				FILELOC_UTF8,
				u8"Invalid Width! "_ustring
					+ (m_V2Rules ? u8"V2 "_ustring : u8"V3+ "_ustring)
					+ u8"headers allow values in the range: "_ustring
					+ (m_V2Rules ? u8"(0, 2^16)"_ustring
								 : u8"(0, 2^32)"_ustring));

			// no way to avoid branching each time here, as
			// far as I can tell. If we did the 'sendErrors'
			// template function trick, we'd just have to
			// branch again on lastResult, instead
			if (Detail::shouldHalt(lastResult) && shortcircuit)
			{
				return result;
			}
		}

		lastResult = isValidBitcount<m_V2Rules>(m_Data.m_Bitcount);
		result &= lastResult;

		if (!Detail::isSuccess(lastResult))
		{
			errFunc(
				FILELOC_UTF8,
				u8"Invalid bitcount! "_ustring
					+ (m_V2Rules ? u8"V2 "_ustring : u8"V3+ "_ustring)
					+ u8"Headers allow values: "_ustring
					+ (m_V2Rules
						   ? u8"1, 4, 8, 24"_ustring
						   : u8"0, 1, 4, 8, 16, 24, 32"_ustring));

			// no way to avoid branching each time here, as
			// far as I can tell. If we did the 'sendErrors'
			// template function trick, we'd just have to
			// branch again on lastResult, instead
			if (Detail::shouldHalt(lastResult) && shortcircuit)
			{
				return result;
			}
		}

		// go to rules for data in next version of header
		if constexpr (!m_V2Rules)
		{
			// then check for violations of V3+ data rules
			Strings::utf8string errStr;
			lastResult = isValidCompression(
				errStr, m_Data.m_Compression, m_Data.m_Bitcount);
			result &= lastResult;

			if (!Detail::isSuccess(lastResult))
			{
				errFunc(FILELOC_UTF8, errStr);

				// no way to avoid branching each time here,
				// as far as I can tell. If we did the
				// 'sendErrors' template function trick, we'd
				// just have to branch again on lastResult,
				// instead
				if (Detail::shouldHalt(lastResult) && shortcircuit)
				{
					return result;
				}
			}

			lastResult = checkUsedColours(
				errStr, m_Data.m_ColoursUsed, m_Data.m_Bitcount);
			result &= lastResult;

			if (!Detail::isSuccess(lastResult))
			{
				errFunc(FILELOC_UTF8, errStr);

				// no way to avoid branching each time here,
				// as far as I can tell. If we did the
				// 'sendErrors' template function trick, we'd
				// just have to branch again on lastResult,
				// instead
				if (Detail::shouldHalt(lastResult) && shortcircuit)
				{
					return result;
				}
			}

			lastResult = checkImportantColours(
				m_Data.m_ColoursUsed, m_Data.m_ImportantColours);
			result &= lastResult;

			if (!Detail::isSuccess(lastResult))
			{
				errFunc(FILELOC_UTF8, errStr);

				// no way to avoid branching each time here,
				// as far as I can tell. If we did the
				// 'sendErrors' template function trick, we'd
				// just have to branch again on lastResult,
				// instead
				if (Detail::shouldHalt(lastResult) && shortcircuit)
				{
					return result;
				}
			}

			lastResult = checkColourMasks(
				m_Data.m_RedMask, m_Data.m_GreenMask,
				m_Data.m_BlueMask, m_Data.m_Compression);
			result &= lastResult;

			if (!Detail::isSuccess(lastResult))
			{
				errFunc(FILELOC_UTF8,
						u8"Bitmap: non-0 colours masks are only "
						u8"valid for Bitfield compression. Masks "
						u8"must also not overlap!"_ustring);

				// no way to avoid branching each time here,
				// as far as I can tell. If we did the
				// 'sendErrors' template function trick, we'd
				// just have to branch again on lastResult,
				// instead
				if (Detail::shouldHalt(lastResult) && shortcircuit)
				{
					return result;
				}
			}

			// go to rules for data in next version of
			// header
			if constexpr (!m_V3Rules)
			{
				// then check for violations of V4+ data
				// rules
				lastResult = checkValidColourSpace(
					m_Data.m_ColourSpaceType, m_V5Rules);
				result &= lastResult;

				if (!Detail::isSuccess(lastResult))
				{
					errFunc(
						FILELOC_UTF8,
						u8"Bitmap: colour space specified was invalid"_ustring
							+ (m_V5Rules
								   ? u8"."_ustring
								   : u8" for a v4 info header."_ustring));

					// no way to avoid branching each time
					// here, as far as I can tell. If we did
					// the 'sendErrors' template function
					// trick, we'd just have to branch again
					// on lastResult, instead
					if (Detail::shouldHalt(lastResult)
						&& shortcircuit)
					{
						return result;
					}
				}

				// go to rules for data in next version of
				// header
				if constexpr (!m_V4Rules)
				{
					// then check for violations of V5+ data
					// rules

					lastResult = checkValidIntent(m_Data.m_Intent);
					result &= lastResult;

					if (!Detail::isSuccess(lastResult))
					{
						errFunc(
							FILELOC_UTF8,
							u8"Bitmap: intent specified was invalid"_ustring);

						// no way to avoid branching each
						// time here, as far as I can tell.
						// If we did the 'sendErrors'
						// template function trick, we'd just
						// have to branch again on
						// lastResult, instead
						if (Detail::shouldHalt(lastResult)
							&& shortcircuit)
						{
							return result;
						}
					}
				}
			}
		}

		return result;
	}
	Detail::FailCode infoHeaderParametersSupportedIMPL(
		[[maybe_unused]] bool shortcircuit,
		[[maybe_unused]] bool sendErrorsToLog) const noexcept
	{
		Detail::FailCode result = Detail::FailCode::Success;
		[[maybe_unused]] Detail::FailCode lastResult =
			Detail::FailCode::Success;

		if constexpr (!m_V2Rules)
		{
			lastResult =
				checkSupportedCompression(m_Data.m_Compression);
			result &= lastResult;

			if (!Detail::isSuccess(lastResult))
			{
				if (sendErrorsToLog)
				{
					// we do this manually, NOT with lambda,
					// because we need to send a different
					// error
					getErrorLog()->addError<SpecNotSupported>(
						FILELOC_UTF8,
						u8"Bitmap Error: Compression mode specified is unsupported by this program. Aborting!"_ustring);
				}

				// no way to avoid branching each time here,
				// as far as I can tell. If we did the
				// 'sendErrors' template function trick, we'd
				// just have to branch again on lastResult,
				// instead
				if (Detail::shouldHalt(lastResult) && shortcircuit)
				{
					return result;
				}
			}

			if constexpr (!m_V3Rules)
			{
				lastResult = checkSupportedColourSpace(
					m_Data.m_ColourSpaceType);
				result = result & lastResult;

				if (!Detail::isSuccess(lastResult))
				{
					if (sendErrorsToLog)
					{
						// we do this manually, NOT with
						// lambda, because we need to send a
						// different error
						getErrorLog()->addError<SpecNotSupported>(
							FILELOC_UTF8,
							u8"Bitmap Warning: colour space was not "
							u8"specified as sRGB, but this program "
							u8"only supports that. Using sRGB instead!"_ustring);
					}

					// no way to avoid branching each time
					// here, as far as I can tell. If we did
					// the 'sendErrors' template function
					// trick, we'd just have to branch again
					// on lastResult, instead
					if (Detail::shouldHalt(lastResult)
						&& shortcircuit)
					{
						return result;
					}
				}

				if constexpr (!m_V4Rules)
				{
					lastResult = Detail::makeFailCode(false, true);
					result &= lastResult;

					getErrorLog()->addError<SpecNotSupported>(
						FILELOC_UTF8,
						u8"Bitmap: V5 header options (intent, profile) are unsupported. Ignoring"_ustring);
				}
			}
		}

		return result;
	}
	static DataClass createInfoHeaderFromFileIMPL(
		const small_nat8* dataBufferPtr, natural64 bufferSize)
	{
		DataClass result;
		runtime_assert(dataBufferPtr != nullptr);

		constexpr const natural64 fullHeaderSize =
			Header::sizeInFile() + DataClass::sizeInFile();
		if (fullHeaderSize > bufferSize)
		{
			throw getErrorLog()->createException<DataAcquisition>(
				FILELOC_UTF8,
				u8"Bitmap: the file header (initial + info header) was indicated to have a size of: "_ustring
					+ frao::Strings::utf8string(fullHeaderSize)
					+ u8"bytes, but only "_ustring
					+ frao::Strings::utf8string(bufferSize)
					+ u8"bytes of data was provided!"_ustring);
		}

		const small_nat8* infoHeaderStart =
			dataBufferPtr + Header::sizeInFile();

		result.m_InfoLength =
			*reinterpret_cast<const small_nat32*>(infoHeaderStart);

		if constexpr (m_V2Rules)
		{
			// V2 header only
			result.m_Width = *reinterpret_cast<const small_nat16*>(
				infoHeaderStart + 4ULL);
			result.m_Height = *reinterpret_cast<const small_nat16*>(
				infoHeaderStart + 6ULL);
			result.m_Bitcount = *reinterpret_cast<const small_nat16*>(
				infoHeaderStart + 10ULL);
		} else
		{
			// v3 || v4 || v5 header
			result.m_Width = *reinterpret_cast<const small_int32*>(
				infoHeaderStart + 4ULL);
			result.m_Height = *reinterpret_cast<const small_int32*>(
				infoHeaderStart + 8ULL);
			result.m_Bitcount = *reinterpret_cast<const small_nat16*>(
				infoHeaderStart + 14ULL);
			result.m_Compression = toCompressionEnum(
				*reinterpret_cast<const small_nat32*>(infoHeaderStart
													  + 16ULL));
			result.m_ImageByteSize =
				*reinterpret_cast<const small_nat32*>(infoHeaderStart
													  + 20ULL);
			result.m_HorizontalResolution =
				*reinterpret_cast<const small_int32*>(infoHeaderStart
													  + 24ULL);
			result.m_VerticalResolution =
				*reinterpret_cast<const small_int32*>(infoHeaderStart
													  + 28ULL);
			result.m_ColoursUsed =
				*reinterpret_cast<const small_nat32*>(infoHeaderStart
													  + 32ULL);
			result.m_ImportantColours =
				*reinterpret_cast<const small_nat32*>(infoHeaderStart
													  + 36ULL);

			// these are technically not part of the V3
			// header, but they are in the same place as they
			// are for V4, if compression mode is bitfields
			if (result.m_Compression == Compression::BITFIELD)
			{
				result.m_RedMask =
					*reinterpret_cast<const small_nat32*>(
						infoHeaderStart + 40ULL);
				result.m_GreenMask =
					*reinterpret_cast<const small_nat32*>(
						infoHeaderStart + 44ULL);
				result.m_BlueMask =
					*reinterpret_cast<const small_nat32*>(
						infoHeaderStart + 48ULL);
			} else
			{
				// they these aren't really valid, because
				// we're not using bitfields - set them to
				// what they are for RGB (kind of)
				result.m_RedMask   = 0;
				result.m_GreenMask = 0;
				result.m_BlueMask  = 0;
			}

			if constexpr (!m_V3Rules)
			{
				// v4 || v5 header
				result.m_RedMask =
					*reinterpret_cast<const small_nat32*>(
						infoHeaderStart + 40ULL);
				result.m_GreenMask =
					*reinterpret_cast<const small_nat32*>(
						infoHeaderStart + 44ULL);
				result.m_BlueMask =
					*reinterpret_cast<const small_nat32*>(
						infoHeaderStart + 48ULL);
				result.m_AlphaMask =
					*reinterpret_cast<const small_nat32*>(
						infoHeaderStart + 52ULL);
				result.m_ColourSpaceType = toColourSpaceEnum(
					*reinterpret_cast<const small_nat32*>(
						infoHeaderStart + 56ULL));
				result.m_CIERedX =
					*reinterpret_cast<const small_int32*>(
						infoHeaderStart + 60ULL);
				result.m_CIERedY =
					*reinterpret_cast<const small_int32*>(
						infoHeaderStart + 64ULL);
				result.m_CIERedZ =
					*reinterpret_cast<const small_int32*>(
						infoHeaderStart + 68ULL);
				result.m_CIEGreenX =
					*reinterpret_cast<const small_int32*>(
						infoHeaderStart + 72ULL);
				result.m_CIEGreenY =
					*reinterpret_cast<const small_int32*>(
						infoHeaderStart + 76ULL);
				result.m_CIEGreenZ =
					*reinterpret_cast<const small_int32*>(
						infoHeaderStart + 80ULL);
				result.m_CIEBlueX =
					*reinterpret_cast<const small_int32*>(
						infoHeaderStart + 84ULL);
				result.m_CIEBlueY =
					*reinterpret_cast<const small_int32*>(
						infoHeaderStart + 88ULL);
				result.m_CIEBlueZ =
					*reinterpret_cast<const small_int32*>(
						infoHeaderStart + 92ULL);
				result.m_GammaRed =
					*reinterpret_cast<const small_nat32*>(
						infoHeaderStart + 96ULL);
				result.m_GammaGreen =
					*reinterpret_cast<const small_nat32*>(
						infoHeaderStart + 100ULL);
				result.m_GammaBlue =
					*reinterpret_cast<const small_nat32*>(
						infoHeaderStart + 104ULL);

				if constexpr (!m_V4Rules)
				{
					// v5 header only
					result.m_Intent = toIntentEnum(
						*reinterpret_cast<const small_nat32*>(
							infoHeaderStart + 108ULL));
					result.m_ProfileData =
						*reinterpret_cast<const small_nat32*>(
							infoHeaderStart + 112ULL);
					result.m_ProfileSize =
						*reinterpret_cast<const small_nat32*>(
							infoHeaderStart + 116ULL);
					result.m_Reserved =
						*reinterpret_cast<const small_nat32*>(
							infoHeaderStart + 120ULL);
				}
			}
		}

		return result;
	}

   public:
	InfoHeaderIMPL(const small_nat8* dataBufferPtr,
				   natural64		 bufferSize)
		: InfoHeader(),
		  m_Data(
			  createInfoHeaderFromFileIMPL(dataBufferPtr, bufferSize))
	{
		auto failstate = isValid();
		if (Detail::shouldHalt(failstate))
		{
			throw getErrorLog()->createException<InvalidFileFormat>(
				FILELOC_UTF8,
				u8"Could not create a header of size: "_ustring
					+ Strings::utf8string(DataClass::sizeInFile()));
		} else if (!Detail::isSuccess(failstate))
		{
			getErrorLog()->addError<InvalidFileFormat>(FILELOC_UTF8,
				u8"A non critical error occurred when creating a header if size: "_ustring
				+ Strings::utf8string(DataClass::sizeInFile()) + u8". Attempting to continue despite error(s)"_ustring);
		}
	}
	~InfoHeaderIMPL() noexcept = default;

	DataClass& getData() noexcept
	{
		return m_Data;
	}
	const DataClass& getData() const noexcept
	{
		return m_Data;
	}

	small_int32 getWidth() const noexcept override
	{
		return m_Data.m_Width;
	}
	small_int32 getHeight() const noexcept override
	{
		return m_Data.m_Height;
	}

	small_nat32 getBitcount() const noexcept override
	{
		return m_Data.m_Bitcount;
	}
	Compression getCompression() const noexcept override
	{
		if constexpr (std::is_same_v<DataClass, CoreInfoData>)
		{
			// because although v2 doesn't specify in the
			// info header, it has defacto RGB compression
			return Compression::RGB;
		} else
		{
			return m_Data.m_Compression;
		}
	}

	bool hasColourPalette() const noexcept override
	{
		runtime_assert(m_Data.m_Bitcount
					   != 0);  // PNG and JPEG bitmaps not
							   // yet supported!

		// RETURN FALSE IFF BITCOUNT >= 16
		// this is since we don't support JPEG or PNG, and
		// RLE 4/8 cannot be this size. RGB and Bitfields
		// (the remaining options) at this size don't have
		// colour palettes
		return (m_Data.m_Bitcount < 16);
	}
	bool hasColourMasks() const noexcept override
	{
		if (hasColourPalette())
		{
			return false;
		}

		auto compression = getCompression();
		runtime_assert((compression == Compression::RGB)
					   || (compression == Compression::BITFIELD));

		return (compression == Compression::BITFIELD);
	}
	natural64 colourPaletteSize() const noexcept override
	{
		natural64 coloursUsed = 0;

		// if m_Data, has a 'colours used' param, get it
		// (unless we have a bitfield)
		if constexpr (!std::is_same<DataClass, CoreInfoData>::value)
		{
			if (m_Data.m_Compression == Compression::BITFIELD)
			{
				// return because we have no palette!
				return 0;
			} else
			{
				// then we have actual colour palette
				// following infoheader, not 3 colour masks
				coloursUsed = m_Data.m_ColoursUsed;
			}
		}

		if (coloursUsed == 0)
		{
			// then palette will have the max number of
			// colours
			coloursUsed = (1ULL << m_Data.m_Bitcount);
		}

		return coloursUsed;
	}
	ColourMasks colourMasks() const noexcept override
	{
		if constexpr (std::is_same_v<DataClass, CoreInfoData>)
		{
			return ColourMasks();
		} else if constexpr (std::is_same_v<DataClass, InfoData>)
		{
			return ColourMasks(m_Data.m_RedMask, m_Data.m_GreenMask,
							   m_Data.m_BlueMask);
		} else
		{
			return ColourMasks(m_Data.m_RedMask, m_Data.m_GreenMask,
							   m_Data.m_BlueMask, m_Data.m_AlphaMask);
		}
	}

	// will return byte size of DataClass ONLY
	constexpr static natural64 sizeInFile() noexcept
	{
		return DataClass::sizeInFile();
	}
	// returns the size of the header corresponding to this
	// structure!
	natural64 currentStructureSize() const noexcept
	{
		return sizeInFile();
	}
	// will return the (file) size of the header that we
	// actually have (ie: V4, V5 etc) this may be different
	// to (larger than) the size of the header corresponding
	// to this structure, if we could not read portions of
	// the header corresponding to V4/V5 data
	natural64 currentSizeInFile() const noexcept override
	{
		return m_Data.m_InfoLength;
	}

	// will write file info header to buffer (ASSUMES THAT
	// BUFFER STARTS AT START OF FILE), and return written
	// bytes
	natural64 writeTo(small_nat8* dataBufferPtr,
					  natural64	  bufferSize) const override
	{
		runtime_assert(dataBufferPtr != nullptr);

		constexpr const natural64 fullHeaderSize =
			Header::sizeInFile() + DataClass::sizeInFile();
		if (fullHeaderSize > bufferSize)
		{
			throw getErrorLog()->createException<DataAcquisition>(
				FILELOC_UTF8,
				u8"Bitmap: the file header (initial + info header) had a size of: "_ustring
					+ frao::Strings::utf8string(fullHeaderSize)
					+ u8"bytes, but only "_ustring
					+ frao::Strings::utf8string(bufferSize)
					+ u8"bytes of buffer was provided!"_ustring);
		}
		small_nat8* infoHeaderStart =
			dataBufferPtr + Header::sizeInFile();

		*reinterpret_cast<small_nat32*>(infoHeaderStart) =
			m_Data.m_InfoLength;

		if constexpr (m_V2Rules)
		{
			// v2 header only
			*reinterpret_cast<small_nat16*>(infoHeaderStart + 4ULL) =
				m_Data.m_Width;
			*reinterpret_cast<small_nat16*>(infoHeaderStart + 6ULL) =
				m_Data.m_Height;
			*reinterpret_cast<small_nat16*>(infoHeaderStart + 8ULL) =
				1;
			*reinterpret_cast<small_nat16*>(infoHeaderStart + 10ULL) =
				m_Data.m_Bitcount;
		} else
		{
			// v3 || v4 || v5 header
			*reinterpret_cast<small_int32*>(infoHeaderStart + 4ULL) =
				m_Data.m_Width;
			*reinterpret_cast<small_int32*>(infoHeaderStart + 8ULL) =
				m_Data.m_Height;
			*reinterpret_cast<small_nat16*>(infoHeaderStart + 12ULL) =
				1;
			*reinterpret_cast<small_nat16*>(infoHeaderStart + 14ULL) =
				m_Data.m_Bitcount;
			*reinterpret_cast<small_nat32*>(infoHeaderStart + 16ULL) =
				toNumber(m_Data.m_Compression);
			*reinterpret_cast<small_nat32*>(infoHeaderStart + 20ULL) =
				m_Data.m_ImageByteSize;
			*reinterpret_cast<small_int32*>(infoHeaderStart + 24ULL) =
				m_Data.m_HorizontalResolution;
			*reinterpret_cast<small_int32*>(infoHeaderStart + 28ULL) =
				m_Data.m_VerticalResolution;
			*reinterpret_cast<small_nat32*>(infoHeaderStart + 32ULL) =
				m_Data.m_ColoursUsed;
			*reinterpret_cast<small_nat32*>(infoHeaderStart + 36ULL) =
				m_Data.m_ImportantColours;

			if (m_Data.m_Compression == Compression::BITFIELD)
			{
				// then we have 3 masks in place of a
				// palette!
				*reinterpret_cast<small_nat32*>(
					infoHeaderStart + 40ULL) = m_Data.m_RedMask;
				*reinterpret_cast<small_nat32*>(
					infoHeaderStart + 44ULL) = m_Data.m_GreenMask;
				*reinterpret_cast<small_nat32*>(
					infoHeaderStart + 48ULL) = m_Data.m_BlueMask;
			}

			if constexpr (!m_V3Rules)
			{
				// v4 || v5 header
				*reinterpret_cast<small_nat32*>(
					infoHeaderStart + 40ULL) = m_Data.m_RedMask;
				*reinterpret_cast<small_nat32*>(
					infoHeaderStart + 44ULL) = m_Data.m_GreenMask;
				*reinterpret_cast<small_nat32*>(
					infoHeaderStart + 48ULL) = m_Data.m_BlueMask;
				*reinterpret_cast<small_nat32*>(
					infoHeaderStart + 52ULL) = m_Data.m_AlphaMask;
				*reinterpret_cast<small_nat32*>(infoHeaderStart
												+ 56ULL) =
					toNumber(m_Data.m_ColourSpaceType);
				*reinterpret_cast<small_int32*>(
					infoHeaderStart + 60ULL) = m_Data.m_CIERedX;
				*reinterpret_cast<small_int32*>(
					infoHeaderStart + 64ULL) = m_Data.m_CIERedY;
				*reinterpret_cast<small_int32*>(
					infoHeaderStart + 68ULL) = m_Data.m_CIERedZ;
				*reinterpret_cast<small_int32*>(
					infoHeaderStart + 72ULL) = m_Data.m_CIEGreenX;
				*reinterpret_cast<small_int32*>(
					infoHeaderStart + 76ULL) = m_Data.m_CIEGreenY;
				*reinterpret_cast<small_int32*>(
					infoHeaderStart + 80ULL) = m_Data.m_CIEGreenZ;
				*reinterpret_cast<small_int32*>(
					infoHeaderStart + 84ULL) = m_Data.m_CIEBlueX;
				*reinterpret_cast<small_int32*>(
					infoHeaderStart + 88ULL) = m_Data.m_CIEBlueY;
				*reinterpret_cast<small_int32*>(
					infoHeaderStart + 92ULL) = m_Data.m_CIEBlueZ;
				*reinterpret_cast<small_nat32*>(
					infoHeaderStart + 96ULL) = m_Data.m_GammaRed;
				*reinterpret_cast<small_nat32*>(
					infoHeaderStart + 100ULL) = m_Data.m_GammaGreen;
				*reinterpret_cast<small_nat32*>(
					infoHeaderStart + 104ULL) = m_Data.m_GammaBlue;

				if constexpr (!m_V4Rules)
				{
					// v5 header only
					*reinterpret_cast<small_nat32*>(infoHeaderStart
													+ 108ULL) =
						toNumber(m_Data.m_Intent);
					*reinterpret_cast<small_nat32*>(infoHeaderStart
													+ 112ULL) =
						m_Data.m_ProfileData;
					*reinterpret_cast<small_nat32*>(infoHeaderStart
													+ 116ULL) =
						m_Data.m_ProfileSize;
					*reinterpret_cast<small_nat32*>(
						infoHeaderStart + 120ULL) = m_Data.m_Reserved;
				}
			}
		}

		return DataClass::sizeInFile();
	}

	// checks if the combination of paramaters is valid.
	// shortcicuit will stop checks at the first invalid
	// check sendErrorsToLog will send non-critical messages
	// to the log detailing what the problem is
	Detail::FailCode isValid(
		bool shortcircuit	 = false,
		bool sendErrorsToLog = true) const noexcept override
	{
		Detail::FailCode result =
			isValidDataIMPL(shortcircuit, sendErrorsToLog);
		result &= infoHeaderParametersSupportedIMPL(shortcircuit,
													sendErrorsToLog);

		return result;
	}
};


class Colour final
{
	small_nat8 m_RedChannel;
	small_nat8 m_GreenChannel;
	small_nat8 m_BlueChannel;
	small_nat8 m_AlphaChannel;

   public:
	Colour(small_nat32 rawData) noexcept;
	Colour(const ColourMasks& masks, small_nat32 rawData) noexcept;

	small_nat32 getB8G8R8A8() const noexcept;

	small_nat8 getRed() const noexcept;
	small_nat8 getGreen() const noexcept;
	small_nat8 getBlue() const noexcept;
	small_nat8 getAlpha() const noexcept;

	float getRedFloat() const noexcept;
	float getGreenFloat() const noexcept;
	float getBlueFloat() const noexcept;
	float getAlphaFloat() const noexcept;
};

class Palette final
{
	std::vector<Colour> m_Palette;
	ColourMasks			m_Masks;

   public:
	Palette(const small_nat8* dataBuffer, natural64 bufferSize,
			const InfoHeader& header);

	// apply set mask to data (usually used with bitcount >=
	// 16)
	Colour mask(small_nat32 rawData) const noexcept;

	natural64 paletteSize() const noexcept;

	Colour&		  operator[](natural64 index) noexcept;
	const Colour& operator[](natural64 index) const noexcept;
};

class BitmapFileData final
{
	ContiguousArray<Colour, 2> m_Bitmap;
	// std::vector<std::vector<Colour>>	m_Bitmap;

	// void	createClear(small_nat32 width, small_nat32
	// height, Colour clearColour) noexcept;

	void normalParse(const InfoHeader& infoHeader,
					 const Palette&	   palette,
					 const small_nat8* bitmapBuffer,
					 natural64 remainingBuffer, small_nat32 realWidth,
					 small_nat32 realHeight, bool bottomUp);
	void rle4Parse(const Palette&	 palette,
				   const small_nat8* bitmapBuffer,
				   natural64		 remainingBuffer) noexcept;
	void rle8Parse(const Palette&	 palette,
				   const small_nat8* bitmapBuffer,
				   natural64		 remainingBuffer) noexcept;

   public:
	BitmapFileData(const Header& header, const InfoHeader& infoHeader,
				   const Palette&	 palette,
				   const small_nat8* dataBuffer,
				   natural64		 bufferSize);

	// auto		getColourRow(natural64 heightIndex) const
	// noexcept -> const std::vector<Colour>&;
	auto getB8G8R8A8ArrayCopy(bool reverseRows = false) const
		-> ContiguousArray<small_nat32, 2>;
	auto getR32G32B32A32_floatArrayCopy(
		bool reverseRows = false) const -> ContiguousArray<float, 2>;
	Colour getColourAt(natural64 heightIndex,
					   natural64 widthIndex) const noexcept;

	small_nat32 getBitmapHeight() const noexcept;
	small_nat32 getBitmapWidth() const noexcept;
};


}  // namespace Bitmap

}  // namespace FileFormats

}  // namespace FileIO

}  // namespace frao

#endif	// !FRAO_TRANESSENCE_FORMATS_BITMAP