#ifndef FRAO_TRANSESSENCE_TOPINCL
#define FRAO_TRANSESSENCE_TOPINCL

//! \file
//!
//! \brief Header of entire Transessence api
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "Basic/IOTypes.hpp"
#include "Basic/Classes.hpp"
#include "Basic/Parsing.hpp"
#include "Formats/Bitmap.hpp"
#include "Manager/Organisation.hpp"
#include "Manager/Control.hpp"
#include "Version/VersionIncl.hpp"

#endif